/**
 * Add ECMA262-5 Array methods if not supported natively
 */
//extend for Array filter
if (!Array.prototype.filter) {
	Array.prototype.filter = function (fun /*, thisp */ ) {
		"use strict";
		if (this == null)
			throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun != "function")
			throw new TypeError();

		var res = [];
		var thisp = arguments[1];
		for (var i = 0; i < len; i++) {
			if (i in t) {
				var val = t[i]; // in case fun mutates this
				if (fun.call(thisp, val, i, t))
					res.push(val);
			}
		}
		return res;
	};
}
// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
	Object.defineProperty(Array.prototype, 'find', {
		value: function (predicate) {
			// 1. Let O be ? ToObject(this value).
			if (this == null) {
				throw new TypeError('"this" is null or not defined');
			}

			var o = Object(this);

			// 2. Let len be ? ToLength(? Get(O, "length")).
			var len = o.length >>> 0;

			// 3. If IsCallable(predicate) is false, throw a TypeError exception.
			if (typeof predicate !== 'function') {
				throw new TypeError('predicate must be a function');
			}

			// 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
			var thisArg = arguments[1];

			// 5. Let k be 0.
			var k = 0;

			// 6. Repeat, while k < len
			while (k < len) {
				// a. Let Pk be ! ToString(k).
				// b. Let kValue be ? Get(O, Pk).
				// c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
				// d. If testResult is true, return kValue.
				var kValue = o[k];
				if (predicate.call(thisArg, kValue, k, o)) {
					return kValue;
				}
				// e. Increase k by 1.
				k++;
			}

			// 7. Return undefined.
			return undefined;
		},
		configurable: true,
		writable: true
	});
}
// extend for Array indexOf
if (!('indexOf' in Array.prototype)) {
	Array.prototype.indexOf = function (find, i /*opt*/ ) {
		if (i === undefined) i = 0;
		if (i < 0) i += this.length;
		if (i < 0) i = 0;
		for (var n = this.length; i < n; i++)
			if (i in this && this[i] === find)
				return i;
		return -1;
	};
}

var QueryString = function () {
	// This function is anonymous, is executed immediately and 
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [query_string[pair[0]], pair[1]];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	}
	return query_string;
}();


var animations_completed = false;


$(document).ready(function () {

	var ini_menu_toggle = menu_toggle();
	var ini_flexslider = initiate_flexslider();
	var ini_scrollers = initiate_scrollers();
	var ini_auto_google_anchor_events = auto_google_anchor_events();
	var ini_formSubmit = formSubmit();
	scrollWaypointInit($('.animateMe'));
	
	if ($('*[data-url]').length) {
		$.each($('*[data-url]'), function () {
			var el = $(this);
			to_url = el.data('url');
			url = el.attr('href');
			$.post('ajax/code', {
				'data': to_url
			}, function (response) {
				if (response) {
					//chekck to see if we have a payload
					if (url.indexOf('?') > 0) {
						url = url + '&pg=' + response;
					} else {
						url = url + '?pg=' + response;
					}
					el.attr('href', url).removeAttr('data-url');
				}
			});
		});
	}




	$('select').on('change', function () {
		$(this).addClass('selection-made');
	});

	if ($('.form-thankyou').length) {
		var pos = $('.form-thankyou').offset().top;

		$('html, body').animate({
			'scrollTop': pos
		}, 600, 'swing', function () {
			$('.form-thankyou').fadeOut(500, function () {
				$('.form-thankyou').fadeIn(500);
			});
		});
	}

	if ($('.form-validation-errors').length) {
		var pos = $('.form-validation-errors').offset().top - $('header').outerHeight() - 150;

		$('html, body').animate({
			'scrollTop': pos
		}, 600, 'swing', function () {
			$('.form-validation-errors').fadeOut(500, function () {
				$('.form-validation-errors').fadeIn(500);
			});
		});
	}

	function scrollWaypointInit( items, trigger ) {
	  items.each( function() {
	    var element = $(this),
	        osAnimationClass = element.data("animation"),
	        osAnimationDelay = element.attr('data-animation-delay');
	 
	    element.css({
	        '-webkit-animation-delay':  osAnimationDelay,
	        '-moz-animation-delay':     osAnimationDelay,
	        'animation-delay':          osAnimationDelay
	    });
	 
	    var trigger = ( trigger ) ? trigger : element;
	 
	    trigger.waypoint(function() {
	        element.addClass('animated').addClass(osAnimationClass);
	    },{
	        triggerOnce: true,
	        offset: '80%'
	    });
	  });
	}



	var headerHeight = $('#header').height();
	if ($(document).scrollTop() > headerHeight) {
		$("#wrap").addClass("header--scroll");
	} else {
		$("#wrap").removeClass("header--scroll");
	}
		
	var scrollTimer = null;
	var wscroll;
	initiate_animations();

	// scroll header shrink
	var headerOnScroll = debounce(function () {
		var headerHeight = $('#header').outerHeight();
		if ($(document).scrollTop() > headerHeight) {
			$("#wrap").addClass("header--scroll");
		} else {
			$("#wrap").removeClass("header--scroll");
		}
	}, 150);

	window.addEventListener('scroll', headerOnScroll);
	
	
	// Scroll to
	if($('.js-scroll-to').length) {
		$('.js-scroll-to').on('click tap touch', function(e) {
			e.preventDefault();
			var el = $(this).attr('href').slice(1); //remove # from href
			scroll_to(el);
		});
	}
	
	if ($('.js-fancybox').length) {
		$('.js-fancybox').fancybox({
			// add Artists Impression at bottom right of slide
			afterLoad: function (instance, slide, e) {
				var $currentAnchor = slide.opts.$orig,
					$currentSlide = slide.$slide; // according to API
				if ($currentAnchor.hasClass('ai')) {
					$currentSlide.addClass('fancybox-slide--add-ai');
				}
			}
		});
	}

	if ($('.js-jump-nav').length) {
		jump_nav();
	}
	
	//Notifications
	if ($('#notifications').children().length) {
		var ini_notifications = init_notifications();
	}
	
	//#region menu activator
	/**
	 * when on scroll over the section it 
	 * 1. turns the corresponding menu active
	 * 2. append hashtag into url
	 * 
	 * Add 'js-activate-section' class on each section and provide 
	 * section name in 'data-section' attribute which should be identical with 
	 * header menu items
	 */
	
	var activateSectionOnScroll = debounce(function () {
		var OFFSET = document.getElementById('header').clientHeight + 5;
		var sections = document.getElementsByClassName('js-activate-section'),
			curScrollPos = window.pageYOffset + OFFSET,
			curActiveSection = undefined;

		curActiveSection = Array.prototype.find.call(sections, function (curSection, i) {
			if (i < (sections.length - 1)) {
				var nextSection = sections[i + 1];
				if (curScrollPos >= offset(curSection).top && curScrollPos < offset(nextSection).top) {
					return true;
				}
			} else {
				// last section
				var lastSectionHeight = curSection.clientHeight;
				if (curScrollPos >= offset(curSection).top && curScrollPos < (offset(curSection).top + lastSectionHeight)) {
					return true;
				}
			}
			return false;
		});
		var menuItems = document.querySelectorAll('.header__menu-wrap li a');
		if (typeof curActiveSection !== 'undefined') {
			var curSectionName = curActiveSection.getAttribute('data-section');
			// menu to be active
			menuItems.forEach(function (menuItem) {
				menuItem.classList.remove('active');
				if (menuItem.textContent.toLowerCase() == curSectionName) {
					menuItem.classList.add('active');
				}
			});
			// url
			window.history.pushState(null, null, "#" + curSectionName);
		} else {
			[].forEach.call(menuItems, function (menuItem) {
				menuItem.classList.remove('active');
			});
		}
	}, 50);
	
	// window.addEventListener('scroll', activateSectionOnScroll);
	
	/** Helpers fn */
	function offset(el) {
		var rect = el.getBoundingClientRect(),
			scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
			scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		return {
			top: rect.top + scrollTop,
			left: rect.left + scrollLeft
		}
	}
	//#endregion

	/**
	 * submenu delay
	 */
	var HOVER_DELAY = 300; // in ms

	if ($('.js-header-menu-item').length) {
		var timer2;
		$('.js-header-menu-item').on('mouseover', function () {
			clearTimeout(timer2);
			$('.js-header-menu-item').removeClass('js-hover');
			$(this).addClass('js-hover');
		}).on('mouseleave', function () {
			var removeHover = function () {
				$(this).removeClass('js-hover');
			}.bind(this);
			timer2 = setTimeout(function () {
				removeHover();
			}, HOVER_DELAY);
		});
	}
	// END submenu delay
	initiate_share();
}); /* END DOCUMENT READY */

function jump_nav() {
	if ($('.js-jump-nav').length) {
		$('.js-jump-nav').on('change', function () {
			var select = $(this);
			console.log("$(this).val() ", $(this).val());
			window.location.href = $(this).val();
		});
	}
}


function scroll_to(el)
{	
	if($('#' + el).length) {
		$('html, body').stop();
		var scrollPos = ($('#' + el).offset().top - ($('header').outerHeight() + $('.mobile-register').outerHeight()) ) +2;

		$('html, body').animate({	
	        scrollTop: scrollPos
	    }, 500);
	}
}

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}

function menu_toggle() {
	var first = true;
	if ($('.js-menu-toggle').length) {
		$('.js-menu-toggle').on('click tap touch', function () {

			$('body').toggleClass('menu-open');
			if (first) {
				first = false;
			} else {
				$('body').toggleClass('menu-close');
			}

			return false;
		});
	}

	if ($('.js-menu-close').length) {
		$('.js-menu-close').on('click tap touch', function () {
			$('body').removeClass('menu-open');
			$('body').addClass('menu-close');

			return false;
		});
	}

	$('.header__menu-wrap').on('click tap touch', 'a', function (e) {
		$('body').addClass('menu-close');
		$('body').removeClass('menu-open');
	});

	if ($('.js-dropdown').length) {
		$('.js-dropdown').on('click tap touch', function (e) {
			e.preventDefault();
			e.stopPropagation();
			$(this).closest('.has-submenu').toggleClass('open');
		});
	}
}

function initiate_flexslider() {
	if ($(".flexslider").length) {

		$('.hero-gallery.flexslider').flexslider({
			animation: "fade",
			slideshowSpeed: 7000,
			slideshow: true,
			pauseOnHover: true,
			pauseOnAction: true,
		});
		$('.article-image .flexslider').flexslider({
			animation: "fade",
			slideshowSpeed: 7000,
			slideshow: true,
			pauseOnHover: true,
			pauseOnAction: true,
		});
		$('.gallery-section .flexslider').flexslider({
			animation: "fade",
			slideshowSpeed: 7000,
			slideshow: true,
			pauseOnHover: true,
			pauseOnAction: true,
		});

		$('.testimonial-gallery.flexslider').flexslider({
			animation: "fade",
			slideshowSpeed: 7000,
			slideshow: true,
			pauseOnHover: true,
			pauseOnAction: true,
		});

	}
}

function initiate_scrollers() {
	if ($('.js-scroll').length) {
		$('.js-scroll').on('click tap touch', function (e) {
			e.preventDefault();
			var target = $($(this).attr('href'));
			var scrollPos = target.offset().top - $('header').outerHeight();

			$('html, body').animate({
				scrollTop: scrollPos
			}, ((scrollPos / 10000) * 800) + 1000);

		});
	}
}
function initiate_animations() {
	var scrollTop = $(window).scrollTop();
	var nav = $('nav');
	var navHeight = nav.outerHeight();

	if (scrollTop > navHeight) {
		nav.addClass('on');
	} else {
		nav.removeClass('on');
	}
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		// .animated ruined up video player on mobile (Android)
		$('.animated.init').each(function () {
			var el = $(this);
			el.removeClass('animated');
		});
	} else {
		$('.animated.init').each(function () {
			var el = $(this);
			if ((scrollTop + $(window).height()) >= el.offset().top) {
				el.addClass($(this).data("ani")).removeClass('init');
			}
		});
	}
}


	$(".popup .close").on('click tap touch', function () { 
		$(this).parent().hide();
		console.log("popup hidden");
	});

function auto_google_anchor_events() {
	$('a').on('click tap touch', function () {
		var el = $(this);
		var params = {},
			event_action = '';
		params.event_category = '';
		params.event_label = '';

		if (el.data('eventcategory')) {
			params.eventCategory = el.data('eventcategory');
		} else if (el.attr('href')) {
			event_action = 'click';
			if (el.attr('href').indexOf('mailto:') >= 0) {
				params.event_category = 'email';
				params.event_label = el.attr('href');
			} else if (el.attr('href').indexOf('tel:') >= 0) {
				params.event_category = 'phone';
				params.event_label = el.attr('href');
			}
		}
		if (el.data('eventaction')) {
			event_action = el.data('eventaction');
		}

		if (el.data('eventlabel')) {
			params.event_label = el.data('eventlabel');
		} else if (typeof el.attr('title') !== 'undefined') {
			params.event_label = el.attr('title');
		}

		if (params.event_category.length && typeof gtag !== 'undefined') {
			gtag('event', event_action, params);
		}
	});
}





function debounce(func, wait, immediate) {
	var timeout;
	return function () {
		var context = this,
			args = arguments;
		var later = function () {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function init_notifications() {

	$('#notifications').children().on('click tap touch', '.notification-close', function () {
		var $currElem = $(this).closest('#info-box');
		$currElem.fadeOut(function () {
			$currElem.remove();
		});
	});

	$('#notifications').children().each(function () {
		var $currElem = $(this);
		$currElem.fadeIn();
		if ($currElem.data('timeout')) {
			var waitSecs = $currElem.data('timeout') * 1000;
			var tmp = setTimeout(function () {
				$currElem.fadeOut(500, function () {
					$currElem.remove();
				});
			}, waitSecs);
		}
	});

	$('#notifications').delay(750).fadeIn(400);

	$('#notifications .additional').delay(1200).fadeIn(400);

	$('#notifications .additional .close-additional').on('click tap touch', function () {
		$('#notifications .additional').fadeOut(600);
	});	
}	

function formSubmit() {
	$('form').on('submit', function (e) {
		var el = $(this);
		var $button = el.find('[type="submit"]');
		var loading = '<i class="fal fa-spinner fa-spin"></i>';
		$button.html(loading);
		$button.prop('disabled', true);
	});
}

function initiate_share() {
	if ($('.addthis_inline_share_toolbox').length) {
		$('.js-share-all').on('click tap touch', function (e) {
			e.preventDefault();
			$('.addthis_inline_share_toolbox a span').click();

			$('.addthis_toolbox > a').hide;
		});
	}
}