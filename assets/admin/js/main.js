//facade uploader event
Dropzone.options.multiUuploader = {
    init: function () {
    this.on("complete", function (file) {
      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        document.location.reload(true)
      }
    });
    }
};

// Disable auto discover for all elements:
Dropzone.autoDiscover = false;

$(document).ready(function() {

    $("body").delegate('.site-notifacition', 'click', function(e) {
        e.preventDefault();
        $(this).fadeOut();
    });

    $(".pop-up-content").fancybox({type: 'iframe'});

    $('.tip').tooltip();
    
    init_sort_lists();

    init_permalink_events();
    
    var promts  = init_confirm();
    
    var colour_picker = ini_colorPicker();

    var ini_menu = initiate_menu();


    // for house and land package
    if($('#stories').length) {
        var package_builder = init_package_builder();
    }

    // for house and land package
    if($('#display-home-create').length) {
        var package_builder = init_display_builder();
    }

    
    //pop ups
    if($(".pop-up").length) {
        var pops = init_pop_ups();
    }

    if($(".inline-popup").length) {
        var ipops = init_inline_pop_ups();
    }

    if($(".gallery-popup").length) {
        var gpops = init_gallery_pop_ups();
    }
    
    
    //load date pickers
    if ($('.date').length) {
        $('.date').datepicker({dateFormat:'dd/mm/yy', onSelect: function(dateText){
                //$('#date_added').val(dateText);
            init_blog_permalink(dateText);
        }});
    }

    //load date pickers normal
    if ($('.date-picker').length) {
        $('.date-picker').datepicker({dateFormat:'yy-mm-dd'});
    }

    //Profiler
    if($('#codeigniter_profiler').length) {
        var ini_profiler = init_profiler();
    }
    
    //Notifications
    if ($('#notifications').children().length){
        var ini_notifications = init_notifications(); 
    }
    
    // search a li
    if ($('.filter-search').length) {
        var search_filter = init_filter_search();
    }
    //blog permalink
    init_blog_permalink_events();
    
    // file uploader
    if ($('.multi-uploader').length) {
        var multi_uploader = init_multi_uploader();
    }

    //Maps
    if($('.toggle-map').length) {
        var maps = init_maps();
    }

    if ($('.page-heading').length)
    {
        var sticky_init = init_sticky();
    }

    if ($(".sub-pages-show").length)
    {
        var show_children_init = init_show_children();
    }

    if ($('[data-toggle="tooltip"]').length)
    {
        $('[data-toggle="tooltip"]').tooltip();
    }

    $('input[type="text"]').focusin(function() { 
        $(this).autoGrowInput();
        $(this).css({'position':'relative', 'z-index':'999'})
    }).focusout(function(){
        $(this).removeAttr('style')
    });


    blank_data_creation();

    draggable_list();

    add_blog_category();

    sort_list_with_checkbox();

    remove_single_image_video();

    var init_tabs = generic_tabs();

    $('.filter-categories').on('change', function(e){
        $('#filter-form').submit();
    });

    //load_video_player();

    // initialise data tables
    if ($('.js-datatables').length) {
        $('.js-datatables').DataTable({
            "paging":   true,
            dom: 'Blfrtip',
            buttons: [
                //'csv', 'excel', 'pdf'
                'csv', 'excel'
            ],
            order : [[ 2, 'desc' ]]
        });
    }

});
function remove_single_image_video() {
    if($('.remove_image_video').length) {
        $('.remove_image_video').each(function(index, item) {
            $(item).on('click', function(event) {
                event.preventDefault();
                if(confirm('Item Will Be Deleted. Are You Sure You Want To Continue ?'))
                {    
                    var element = $(this);
                    var hiddenField = element.attr('target-hidden');
                    var imageField = element.attr('target-img');

                    $.ajax({
                        url: element.attr('href'),
                        type: "POST",
                        success: function(result) {
                            if(result == '0')
                                alert('Failed to remove Image/Video');
                            else
                            {
                                if($(hiddenField).length) 
                                    $(hiddenField).remove();
                                if($(imageField).length) 
                                    $(imageField).remove();
                                element.remove();
                            }
                        }
                    });
                }
            });
        });
    }
}

function sort_list_with_checkbox() {
    if ($('.chcekbox-manage-list').length) {
        $('.chcekbox-manage-list').each(function(key, item) {
            $(item).sortable({
                cancel: '.title',
                handle: '.handle',
                update: function(event, ui) {
                    var ul = $(this);
                    var order = [];
                    var finalOrder = [];

                    var dataHolder = ul.attr('data-holder');
                    $(dataHolder).val('');
                    
                    ul.find('li input[type="checkbox"]').each(function(index, chkbox) {
                        if($(chkbox).is(':checked')) {
                            order.push($(chkbox).parents('li').attr('id'));
                            $(chkbox).attr('checked', true);
                        }  
                    });

                    //console.debug(order);

                    if(order.length > 0)
                    {
                        //$(dataHolder).attr('value') = order.join(",");
                        for(var i = 0; i < order.length; i++)
                        {
                            orderArray = order[i].split("_");
                            finalOrder.push(orderArray[1]);
                        }

                        finalOrder = finalOrder.join(",");
                        $(dataHolder).val(finalOrder);

                        // postParam = finalOrder + '&rel=' + ul.attr('data-rel');

                        // $.post(ul.attr('data-url'), postParam, function() {
                        //     set_message('success', 'Order updated.');
                        // });
                    }
                }
            });
        });
    }
}

/// this function will trigger an ajax to the backend to add a NEW BLOG CATEGORY if not exists ///
/// after the new blog category creation, 
///        - it will ADD that blog category details as a new <li> to the <ul> SPECIFIED ON THE BUTTON attribute "insert-to" 
function add_blog_category() {
    if($('.add_category_btn').length) {
        $('.add_category_btn').each(function(key, item) {
            $(item).click(function(event) {
                event.preventDefault();
                var new_category = $('#new_category').val();
                var parent_id = $('#new_category_parent').val();
                var insertPoint = $(this).attr('insert-to');

                if(new_category === '' || new_category === null || new_category === undefined) 
                {
                    alert('You must provide a name for the new blog category');
                    return false;
                }
                else
                {

                    var ajaxURL = $(this).data('url');
                    $.ajax({
                        url: ajaxURL,
                        type: "POST",
                        data: {'new_category' : new_category, 'parent_id' : parent_id},
                        success: function(result) {
                            if(result == '0')
                                alert('Failed to add Blog category');
                            else
                            {
                                if($(insertPoint).length) {
                                    $(insertPoint).append(result);
                                    $('#new_category').val('');
                                }
                                else
                                    alert('Failed to ADD Blog Category');
                            }
                        }
                    });
                }
            });
        });
    }
}

/// this function triggers the creation of blank service_testimonial && service_package for a service ///
/// Once created, the newly added testimonial or package is being added to the page DOM (the 'data-placeholder' specified on the button) ///
function blank_data_creation() {
    if($('.blank-creation').length) {
        $('.blank-creation').each(function(key, item) {
            $(item).click(function(e) {
                e.preventDefault();
                var url = $(item).attr('href');
                var placeholder = $(this).data('placeholder');
                if($(placeholder).length) {
                   $.getJSON(url, function(response) {
                    
                        if(response && response.html)
                            $(placeholder).append(response.html);
                        else
                            alert('cannot create!');
                    });
                }
            });
        });
    }
}

/// this function triggers any draggable events between two <ul> ///
/// currently being used by related_service ///
function draggable_list() {
    if($('ul.draggable_list').length) {
       $('ul.draggable_list').each(function(key, item) {
            //console.debug(item);
            $(item).sortable({
                connectWith: "ul",
                dropOnEmpty: true,
                receive: function(event, ui) {
                    //console.debug(ui.sender);
                    //console.debug(ui.item);

                    var toServiceId = $(ui.item).attr('service_id');
                    var ajaxUrl = base_url + $(ui.sender).data('url') + "/" + toServiceId;

                    $.ajax({
                        url: ajaxUrl,
                        type: "POST",
                        success: function(result) {
                            console.debug(result);
                        } 
                    }); 
                }        
            });
            $(item).disableSelection();
       });
    }
}

/*function init_display_builder() {
    
    $('#prop-estate_id').on('change', function(){
        var val = $(this).val();
        //console.debug(val);
        //return;
        $.getJSON(base_url + 'admin/ajax/get_lots/'+val, function(response){
            if(response) {
                var html = '<option value="">-- Please select --</option>';
                $.each(response, function(i, v){
                    html +='<option value="'+v.id+'">'+v.name+'</option>';
                });
                
            } else {
                var html = '<option value="">-- Please select another Estate --</option>';
            }
            $('#prop-lot').html(html);
        });
    });

    $('#prop-stories').on('change', function(){
        var val = $(this).val();
        var lot = $('#prop-lot').val();
        $.getJSON(base_url + 'admin/ajax/series_by_stories/'+val+'/'+lot, function(response){
            if(response) {
                var html = '<option value="">-- Please select --</option>';
                $.each(response, function(i, v){
                    html +='<option value="'+v.id+'">'+v.select_option+'</option>';
                });
                
            } else {
                var html = '<option value="">-- Please select another Stories --</option>';
            }
            $('#prop-model').html(html);
        });
    });

    $('#prop-inclusions').on('change', function(){
        var val = $(this).val();
        var model = $('#prop-model').val();
        $.getJSON(base_url + 'admin/ajax/house_inclusion/'+val+'/'+model, function(response){
            if(response) {
                var html = '<option value="">-- Please select --</option>';
                $.each(response, function(i, v){
                    html +='<option value="'+v.id+'">'+v.name+'</option>';
                    //console.debug(v);
                });
            } else {
                var html = '<option value="">-- No Facades available --</option>';
            }
            $('#prop-facade_id').html(html);
        });
    });


}*/

function init_display_builder() {
    
    $('#prop-estate_id').on('change', function(){
        var val = $(this).val();
        //console.debug(val);
        //return;
        $.getJSON(base_url + 'admin/ajax/get_lots/'+val, function(response){
            if(response) {
                var html = '<option value="">-- Please select --</option>';
                $.each(response, function(i, v){
                    html +='<option value="'+v.id+'">'+v.name+'</option>';
                });
                
            } else {
                var html = '<option value="">-- Please select another Estate --</option>';
            }
            $('#prop-lot').html(html);
        });
    });

    $('#prop-lot').on('change', function(){
        var val = 0;
        var lot = $('#prop-lot').val();
        $.getJSON(base_url + 'admin/ajax/series_by_lots/'+lot, function(response){
            if(response) {
                var html = '<option value="">-- Please select --</option>';
                $.each(response, function(i, v){
                    html +='<option value="'+v.id+'">'+v.select_option+'</option>';
                });
                
            } else {
                var html = '<option value="">-- Please select another Stories --</option>';
            }
            $('#prop-model').html(html);
        });
    });

    $('#prop-model').on('change', function(){
        var model = $('#prop-model').val();
        $.getJSON(base_url + 'admin/ajax/house_inclusion_model/'+model, function(response){
            if(response) {
                var html = '<option value="">-- Please select --</option>';
                $.each(response, function(i, v){
                    html +='<option value="'+v.id+'">'+v.name+'</option>';
                    //console.debug(v);
                });
            } else {
                var html = '<option value="">-- No Facades available --</option>';
            }
            $('#prop-facade_id').html(html);
        });
    });


}

function init_package_builder() {
    //$('#facade').html('<option value="">-- Please select from Inclusion Package --</option>').selectpicker('refresh');
    var lot = $('#selected_lot').val();
    $('#stories').on('change', function(){
        var val = $(this).val();
        var stories = $('#stories').val();
        $.getJSON(base_url + 'admin/ajax/house_based_inclusions/'+stories+'/'+lot, function(response){
            if(response) {
                var html = '<option value="">-- Please select --</option>';
                html += response;
            } else {
                var html = '<option value="">-- No House Types available --</option>';
            }
            $('#model').html(html);
        });
    });

    //get models based on collection
    // $('#inclusions').on('change', function(){
    //     var el  = $(this);
    //     var val = el.val();
    //     //$('#house_price').val(el.find(":selected").data('price'));
    //     $.getJSON(base_url + 'admin/ajax/house_based_inclusions/'+val+'/'+$('#stories').val()+'/'+lot, function(response){
    //         if(response) {
    //             var html = '<option value="">-- Please select --</option>';
    //             html += response;
    //         } else {
    //             var html = '<option value="">-- No House Types available --</option>';
    //         }
    //         $('#model').html(html).selectpicker('refresh');
    //     });
    // });

    //get facades
    $('#model').on('change', function(){
        var el = $(this);
        var val = el.val();
        //var inclusions = $('#inclusions').val();
        var price = el.find(":selected");
        $.getJSON(base_url + 'admin/ajax/house_package_inclusion_facades/'+val, function(response){
            //console.debug(response);
            if(response) {
                var html = '<option value="">-- Please select --</option>';
                if(response)
                {
                    var html = '<option value="">-- Please select --</option>';
                    html += response;
                    
                } else {
                    var html = '<option value="">-- No Facades available --</option>';
                }
                $.getJSON(base_url + 'admin/ajax/house_series/'+val, function(house){
                    if (house) {
                        //var oEditor = CKEDITOR.instances.ckfinder;
                        var the_ed=CKEDITOR.instances['details']
                        the_ed.setData(house.package_description);
                        //CKEDITOR.instances[ckeditor_id].insertText('test' );
                        //$( 'textarea.editor' ).val('test');
                        //console.debug(house);
                    }
                });
                $('#facade').html(html);
                $('#house_price').val(price.data('price'));
                $('#pack_price').val(price.data('pack'));
            }
        });
    });

    $('#facade').on('change', function(){
        var val = $(this).val();
        $.getJSON(base_url + 'admin/ajax/facade_price/'+val, function(response){
            if(response) {
                $('#facade_price').val(response.price);
                calc_total_package();
            }
        });
        
    });

    // $('#discount_amount').keyup(function() {
    //     calc_delay(function(){
    //       calc_total_package();
    //     }, 800 );
    // });

    $('.calc-up').keyup(function() {
        calc_delay(function(){
          calc_total_package();
        }, 800 );
    });
}

var calc_delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

function calc_total_package() {
    var total = 0;
    $.each($('.add-to-total'), function(){
        var el = $(this);
        
        if(parseFloat(el.val())) {
            total += parseFloat(el.val());
        }
    });
    $('#package_total').val(parseFloat(total));
}

function init_maps() {
    $('.toggle-map').click(function(e){
        e.preventDefault();
        $('.drop-map').slideToggle();
    });
}

function init_pop_ups() {
/*
    $(".pop-up").click(function(e){
        e.preventDefault();
        $(this).fancybox({
        type: 'iframe'
    });
    })
*/
    $("a.pop-up").each(function() {
        var location = $(this).attr('href');
        if(location.indexOf('?') === -1)
        {
          location += '?dy=1';
        } else {
            location += '&dy=1';
        }
        console.log(location);
        $(this).attr('href', location);
    });
    var target = $('a.pop-up')
    $('a.pop-up').fancybox({
        type: 'iframe',
        autoSize : 'false',
        beforeLoad : function() {
            this.width  = 960;
        }
    });

}

function init_inline_pop_ups()
{
    $('a.inline-popup').fancybox(
    {
        maxWidth: 640
    });
}

function init_gallery_pop_ups()
{
    $('a.gallery-popup').fancybox({ });
}


function init_advert_select() {
    $('#prop-add-option').change(function(e, i){
        var page_id     = $(this).data('page');
        var selected    = $(this).val();
         $.get(base_url + 'admin/pages/advertising/new/' + selected + '/' + page_id, function(response){
             //success
             if(response.error = 0) {

             }
         });
    });
}

function init_multi_uploader()
{
        var el = $('.multi-uploader');
        var multiUuploader = $(el).dropzone({
            url: el.data('action'),
            dictDefaultMessage: el.data('title'),
            paramName: el.data('name'),
            uploadMultiple: false,
            addRemoveLinks: false,
            previewsContainer: false,
            init: function() {
                this.on("success", function(file, responseText) {
                    if(responseText == 1) {
                        console.debug(file);
                    } else {
                        var response = JSON.parse(responseText);
                        if (response.error) {
                            alert(response.error);
                        } else if(response.html) {
                            $('.upload-new-image').before(response.html);
                            //$(response.html).insertBefore('.upload-new-image')
                            Dropzone.forElement('.multi-uploader').removeFile(file);
                            
                        }
                    }
                });
            }
        });
        // multiUuploader.on("complete", function (file) {
        //     //console.log('sdf');
        //     console.debug(file);
        //     // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        //     //     document.location.reload(true);
        //     // }

        //     multiUuploader.removeFile(file);// remove file from the zone.
        // });
}

// load any color pickers
function ini_colorPicker(){
    if($('.colour').length) {
        $('.colour').each(function(){
            var el = $(this);
            el.css('backgroundColor', el.val());
            el.ColorPicker({
                color: el.val(),
                onShow: function (colpkr) {
                        $(colpkr).fadeIn(500);
                        return false;
                },
                onHide: function (colpkr) {
                        $(colpkr).fadeOut(500);
                        return false;
                },
                onChange: function (hsb, hex, rgb) {
                        el.css('backgroundColor', '#' + hex);
                        el.val('#' + hex);
                }
        });
            
        });
    }
}

//update an element with the geo location from google maps
function update_geo(target, lat, long) {
    if($(target).length) {
        $(target).val(lat + ', '+long);
    }
}


function init_threesixty() {
   $('#prop-threesixty').on('change', function(){
    alert('sdf')
    console.debug($('#prop-threesixty option:selected').val());
       var option = $('#prop-threesixty option:selected').val();
       if(option == 'video') {
           $('#dummy-threesixty').show();
       }
   }); 
}

function init_postcode() {
    $('#postlocation').on('keyup', function(e){
        var val = $(this).val();
        $.post(base_url+'admin/ajax/postcode', {'address': val}, function(response){
            if(response != '0') {
                var html = '';
                var arr = $.parseJSON(response);
                console.debug(arr);
                $(arr).each(function() {
                    $.each(this, function(k, v) {
                        html += '<li data-value="'+k+'" >'+v+'</li>';
                        console.debug(k);
                    });                    
                });
                if(html != '') {
                    html = '<ul class="autofill">'+html+'</ul>';
                    $('.autofill').remove();
                    $("#postlocation").after(html);
                    $('.autofill li').unbind().on('click', function(){
                       var val = $(this).data('value');
                       $('#address').val(val);
                    });
                }
            }
        });
    });
}

function init_show_children() {
    $(".sub-pages-show").on("click", function(e) {
            $(this).closest('.item-parent').children('.manage-list').toggle();

             if ($(this).hasClass('icon-rightarrow')) {
                 $(this).removeClass('icon-rightarrow').addClass('icon-downarrow');
             } 
            else {
                  $(this).removeClass('icon-downarrow').addClass('icon-rightarrow');
             } 

    });
    $(".sub-pages-show").trigger('click');
}

function init_sort_lists() {
    if ($('.manage-list').length) {

        $('.manage-list').each(function(key, item) {
            $(item).sortable({
                cancel: '.title',
                handle: '.item-child',
                update: function(event, ui) {
                    var ul = $(this);
                    var order = ul.sortable("serialize") + '&rel=' + ul.attr('data-rel');
                    $.post(ul.attr('data-url'), order, function() {
                        set_message('success', 'Order updated.');
                    });
                }
            });
            $(item).sortable("disable");
        });

        $('.start-ordering').on('click tap touch', function(e) {
            e.preventDefault();
            $('.manage-list').addClass('allow-sort');
            $('.manage-list').each(function(key, item) {
                $(item).sortable("enable");
            });
            $('.start-ordering').hide();
            $('.stop-ordering').show();
        });

        $('.stop-ordering').on('click tap touch', function(e) {
            e.preventDefault();
            $('.manage-list').removeClass('allow-sort');
            $('.manage-list').each(function(key, item) {
                $(item).sortable("disable");
            });
            $('.stop-ordering').hide();
            $('.start-ordering').show();
        });

    }
    //FIXED SORTING - the ul(s) that has the class 'allow-manage-list' can be sorted any time and are not dependent on the start order button click //
    if ($('.allow-manage-list').length) {
        $('.allow-manage-list').each(function(key, item) {
            $(item).sortable({
                cancel: '.title',
                handle: '.handle',
                update: function(event, ui) {
                    var ul = $(this);
                    var order = ul.sortable("serialize") + '&rel=' + ul.attr('data-rel');
                    $.post(ul.attr('data-url'), order, function() {
                        set_message('success', 'Order updated.');
                    });
                }
            });
        });
    }
}

function init_tabs() {
    $('.tabs').each(function(index) {
        $('.tabs:eq(' + index + ') > div').hide().first().show();
        var rows = $('.tabs:eq(' + index + ') > ul li a').click(function(e) {
            e.preventDefault();
            var target = $(this).attr('href');
            var tab_index = rows.index(this);
            //tabs
            $('.tabs:eq(' + index + ') > ul li').removeClass('selected');
            $(this).parent('li').addClass('selected');
            //content
            $('.tabs:eq(' + index + ') > div').removeClass('selected').hide();
            $('' + target + ' ').addClass('selected').show();
        });
    });
    //open hash tag
    if (window.location.hash) {
        var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
        set_tab(hash);
    }
}

function set_tab(rel) {
    if ($('.tabs li a[href="#' + rel + '"]').length) {
        var master_parent = $('.tabs li a[href="#' + rel + '"]').parent().parent().parent();
        $(master_parent).find('li').removeClass('selected');        
        $('.tabs li a[href="#' + rel + '"]').parent().addClass('selected');
        master_parent.find('> div').hide().removeClass('selected');
        master_parent.find('#'+rel).show().addClass('selected');
    }
}



function generic_tabs()
{
    var tab_container = '.tab-container';
    var tab_link = '.tab-link'; //dom element for link to tab
    var tab_content = '.tab-content'; //dom element for tab contents

    if (tab_container.length)
    {
        $(tab_container).each(function() {

            var container = $(this);
            var $links = container.find(tab_link);
            var $content = container.find(tab_content);

            $content.hide();
            $links.removeClass('active');
            console.log($(location).attr('hash'));

            if ($($(location).attr('hash')).length)
            {
                $links.each(function() {

                    var current_link = $(this);
                    if (current_link.attr('href') == $(location).attr('hash'))
                    {
                        //e.preventDefault();

                        var current_content = container.find('#' + current_link.attr('href').substring(1));

                        $content.hide();
                        $links.removeClass('active');

                        current_link.addClass('active');
                        current_content.show();

                        //update the form action?
                        if (current_link.data('form')) {
                            var form_action = $('#' + current_link.data('form')).attr('action');
                            $('#' + current_link.data('form')).attr('action', form_action + current_link.attr('href'));
                        };
                    }

                });

            }
            else
            {
                $links.eq(0).addClass('active');
                container.find('#' + $links.eq(0).attr('href').substring(1)).show();
            }

            $links.on('click tap touch', function(e) {

                e.preventDefault();
                //$(location).attr('hash', '');

                var current_link = $(this);
                var hash = current_link.attr('href').substring(1);
                var current_content = container.find('#' + hash);

                var form = current_link.closest("form");
                if(form.length) {
                    document.location.hash = hash;
                }

                $content.hide();
                $links.removeClass('active');

                current_link.addClass('active');
                current_content.show();

                //update the form action?
                if (current_link.data('form')) {
                    $('#' + current_link.data('form')).attr('action', form_action + current_link.attr('href'));
                };

            });



        });
    }

}


function set_message(message_type, message) {
    var box = '<div class="site-notifacition" style="display:none;"><div class="' + message_type + '" id="info-box">' + message + '</div></div>';
    $(".site-notifacition").remove();
    $('.mainbody').after(box);
    $('.site-notifacition').fadeIn();
}

function init_permalink_events() {
    var url_timer;
    if ($('.demo-url').length) {
        $('#title').keyup(function() {
            clearTimeout(url_timer);
            url_timer = setTimeout(function() { init_permalink(); }, 300);
        });
    }
    $('#parent').change(function() {
        init_permalink();
    });
}

function init_permalink() {
    var parent = $('#parent').val();
    var permalink = $('#title').val();
    var parent_url = $('#dummy_parent').val();
    $.post(base_url + 'admin/ajax/validate_url', {parent_rel: parent, permalink_uri: permalink, parent_segment: parent_url}, function(data) {
        var obj = jQuery.parseJSON(data);
        if (obj.success == 1) {
            $('#permalink-input').val(obj.segment);
            $('.demo-url').html(obj.uri);
            $('#permalink').val(obj.permalink);
        }
    });
}

function init_blog_permalink_events() {
    
    if ($('.demo-url').length) {
        var url_timer;
        $('#blog-title').data('original', $('#blog-title').val());
        $('#permalink').data('original', $('#permalink').val());
        $('.demo-url').data('original', $('.demo-url').html());
        $('#blog-title').keyup(function() {
            clearTimeout(url_timer);
            url_timer = setTimeout(function() { init_blog_permalink(); }, 300);
        });
    }
//    $('#date_added').change(function() {
//        init_blog_permalink();
//    });
    //init_blog_permalink();
}

function init_blog_permalink(dateText) {
    var pre_date = typeof dateText !== 'undefined' ? dateText : $('#date_added').val();
    var permalink = $('#blog-title').val();
    var parent_url = $('#dummy_parent').val();
    $.post(base_url + 'admin/ajax/validate_blog_url', {date: pre_date, permalink_uri: permalink, parent_segment: parent_url}, function(data) {
        var obj = jQuery.parseJSON(data);
        //console.debug(data);
        if (obj.success == 1) {
            $('#permalink-input').val(obj.segment);
            $('.demo-url').html(obj.uri);
            $('#permalink').val(obj.permalink);
        }
        else
        {
            $('#blog-title').val($('#blog-title').data('original'));
            $('.demo-url').html($('.demo-url').data('original'));
            $('#permalink').val($('#permalink').data('original'));
            alert('That Blog Title is already taken');
        }
    });
}

function init_select_sale_lot() {
    $('#lot-select-for-sale').change(function() {
        var el = $(this).val();
        $.get(base_url + '');
        console.debug(el);
    });
}

/**
 * Makes a js confirm box
 * usage: <a class="confirm" href="target url" title="The content of the confirm box.">Button text</a>
 * 
 * @returns void
 */
function init_confirm() {
    $( "body" ).delegate( "a.confirm", "click", function(e) {
        e.preventDefault();
        var el = $(this);
        var targetUrl = el.attr("href");
        if (confirm(el.attr("title"))) {

            if(el.data('rel') && el.hasClass('slide-out')) {
                var target_el = el.data('rel');
                if($(target_el).length) {
                    $.get(targetUrl, function(){
                        $(target_el).animate({width: 'toggle'}, function(){
                            $(target_el).remove();
                        });
                    });
                } else {
                    window.location = targetUrl;
                }
            } else if(el.data('rel') && el.hasClass('slide-out-top')) {
                var target_el = el.data('rel');
                if($(target_el).length) {
                    $.get(targetUrl, function(){
                        $(target_el).animate({height: 'toggle'}, function(){
                            $(target_el).remove();
                        });
                    });
                } else {
                    window.location = targetUrl;
                }
            } else {
                window.location = targetUrl;
            }
        }
    });

    $( "body" ).delegate( "button.confirm", "click", function(e) {
        e.preventDefault();
        var el = $(this);
        var targetUrl = el.data('rel');
        if (confirm(el.attr("title"))) {
            $(targetUrl).submit();
        }
    });
    
}

function init_profiler() {
    $('#codeigniter_profiler').prepend( '<a class="toggleprofiler" href="#">Profiler</a>' );
    $('#codeigniter_profiler fieldset').wrapAll( "<div class='profiler-wrapper' />");
    
    $('#codeigniter_profiler .toggleprofiler').click(function(e){
        e.preventDefault();
        $('.profiler-wrapper').slideToggle();
    });
}

function init_notifications() {

    $('#notifications').children().on('click tap touch', '.notification-close',  function() {
        var $currElem = $(this).closest('#info-box');
        $currElem.fadeOut(function() {
                $currElem.remove();
        });
    });

    $('#notifications').children().each(function() {
        var $currElem = $(this);
        $currElem.fadeIn();
        if ($currElem.data('timeout')) {
            var waitSecs = $currElem.data('timeout') * 1000;
            var tmp = setTimeout(function() { $currElem.fadeOut(500, function() { $currElem.remove(); } ); }, waitSecs);
        }
    });

    $('#notifications').delay(750).fadeIn(400);

    $('#notifications .additional').delay(1200).fadeIn(400);

    $('#notifications .additional .close-additional').on('click tap touch',  function() {
        $('#notifications .additional').fadeOut(600);
    });

}

function init_filter_search() {

    var el = $('.filter-search');
    var list = el.data('list');

    el.on('keyup', function(){
        //alert('sdf');
        var value = $(this).val().toLowerCase();
        var $li = $('#'+ list + ' .filter');

        //console.log(value);
         $li.hide();
         $li.filter(function() {
             return $(this).text().toLowerCase().indexOf(value) > -1;
         }).show();
    });
}

function init_sticky()
{

    var $sticky = $('.page-heading > .row');
    var $stickycontainer = $('.page-heading');

    var stickyTop = $stickycontainer.offset().top;
    var stickyheight = $stickycontainer.height();

    
    $(window).resize(function(){ 
        stickyTop = $stickycontainer.offset().top; 
    });
    
    $(window).scroll(function(){ 
        
        stickyTop = $stickycontainer.offset().top; 
        var windowTop = $(window).scrollTop(); 
        
        if (stickyTop < windowTop)
        {
            $stickycontainer.css('height', stickyheight + 'px');
            $sticky.addClass('sticky');
        }
        else 
        {
            $stickycontainer.css('height', 'auto');
            $sticky.removeClass('sticky');
        }
    
    });
}

function scroll_to(el) {
    $('html, body').animate({
        scrollTop: $("#"+el).offset().top
    }, 500);
}



function initiate_menu()
{
}

function ini_sopts() {
    var floorplan = $('#floorplan');
    $( ".marker" ).draggable({
        containment: '#floorplan',
        stop : function(e, el){
            var spot    = $(this).data('rel');
            var left    = parseInt($(this).css("left")) / ($("#floorplan img").width() / 100)+"%";
            var top     = parseInt($(this).css("top")) / ($("#floorplan img").height() / 100)+"%";
            var str     = 'left:' + left + '; top:' + top + ';';
            console.debug(spot);
            $('#'+spot).val(str);
        }
    }).dblclick(function() {
        var el = $(this);
        if (confirm('You are about to remove '+el.attr("title") + ' do you wish to continue?')) {        
            $.get(base_url+'admin/tour/remove_scene/' + el.data('id'), function(response){
                if (response) {
                    el.fadeOut();
                }
            });
        }
    });
    // .mousedown(function(event) {
    //     if (event.which == 2) {
    //         var el = $(this);
    //         if (confirm('You are about to remove '+el.attr("title") + ' do you wish to continue?')) {        
    //             $.get(base_url+'admin/tour/remove_scene/' + el.data('id'), function(response){
    //                 if (response) {
    //                     el.fadeOut();
    //                 }
    //             });
    //         }
    //     }
    // }
}
/******************************************************************************************** MAPS ****************************************************/
function map_poly_complete(data, save_target, ref_id)
{
    var len = data.getPath().getLength();
    var htmlStr = '';
    for (var i = 0; i < len; i++) {
        htmlStr += data.getPath().getAt(i).toUrlValue(5) + '|';
    }
    save_poly(htmlStr.split('|'), save_target, ref_id)    
}

function remove_poly(data)
{
    data.setMap(null);
    console.debug(data);
}

function edit_poly(data)
{
    var details = get_poly_details(data);
    console.debug(htmlStr);
}

function populate_input(id, val)
{
    var pointsArray = [];
    var vertices = val.getPath();

    for (var i =0; i < vertices.getLength(); i++) {
        var xy = vertices.getAt(i);
        pointsArray[pointsArray.length] = '"' + xy.toUrlValue() + '"';
    }

    var inputStr = '[' + pointsArray.toString() + ']';

    $('#' + id).val(inputStr);
}

function clear_input(id)
{
    $('#' + id).val('');
}
/******************************************************************************************** MAPS ****************************************************/

// function get_poly_details(data) {
//     var len = data.getPath().getLength();
//     var htmlStr = '';
//     for (var i = 0; i < len; i++) {
//         htmlStr += data.getPath().getAt(i).toUrlValue(5) + '|';
//     }
//     return htmlStr;
// }

function save_poly(latLngs, save_target, ref_id) {
    $.post(save_target, {'lat_lngs' : latLngs, 'ref_id' : ref_id}, function(){});
}
