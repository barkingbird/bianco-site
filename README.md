# This is a seed project based on Whitelabel.



---
Install node dependencies for gulp under /assets



```
npm install
```

Compile (and watch) SCSS 

```
gulp 
```

```
gulp  watch
```

Add database connection settings to application/config/database.php and use dump.sql as a starting point

Change the site name in web_application.php

Also remember to generate favicons and put them in assets/images/favicon.ico/ folder