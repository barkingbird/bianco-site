<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// hr_title is the name of the section shown in the admin
// main is the info that is one off to the section - think the main title and description above the row
// items are the multiple "columns" inside the row
// item_class is applied to items to make them span full width or horizontally stack
// field types - text, textarea, ckeditor, image, file
// on the same level as the field type, 'class' can be specified to limit the width - similar to item_class above.
// image_sizes - specifies the name of a diffrent image sizing array to use ie. 'section_sizes'
// file_path - specify a different save path for uploaded images
// information - a piece of explananatory text to show for the admin user.
// limit - limits the amount of items/columns that can be added.

$config['section_sizes'] = array(
    'full' 		=> array('2400', '2400'),
    'desktop' 	=> array('1200', '1200'),
    'tablet' 	=> array('1000', '1000'),
    'mobile' 	=> array('800', '800'),
    'thumb' 	=> array('400', '400'),
);

$config['section_path'] = 'assets/media/sections/';

$config['section_type']['contact_form'] = array(
	'hr_title' => 'Contact Form',
	'main' => array(
        'form_id' => array(
            'field' => 'text',
            'class' => 'col-sm-30',
        ),
        'title' => array(
			'field' => 'text',
		),
		'subheading' => array(
			'field' => 'textarea',
		),
    ),
	'limit' => '',
	'image_sizes' => '',
	'file_path' => '',
);

$config['section_type']['contact_form2'] = array(
	'hr_title' => 'Contact Form 2',
	'main' => array(
        'form_id' => array(
            'field' => 'text',
            'class' => 'col-sm-30',
        ),
        'title' => array(
			'field' => 'text',
		),
		'subheading' => array(
			'field' => 'textarea',
		),
    ),
	'limit' => '',
	'image_sizes' => '',
	'file_path' => '',
);


$config['section_type']['image_article'] = array(
	'hr_title' => 'Article with Images',
	
	'main' => array(
		
		'text_position' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'left' => 'Left',
				'right' => 'Right',
			)
		),
		'title' => array(
			'field' => 'text',
		),
		'subheading' => array(
			'field' => 'textarea',
		),
		'description' => array(
			'field' => 'textarea',
		),
        'body_copy' => array(
            'field' => 'textarea',
        ),

	),

	'items' => array(
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-60'
		),
		'show_artists_impression' => array(
			'field' => 'select',
			'class' => 'col-sm-60',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
	),

	'limit' => '',
	'item_class' => 'col-sm-20',
	'image_sizes' => '',
	'file_path' => '',

);





// $config['section_type']['full_image'] = array(
// 	'hr_title' => 'Full Image',


// 	'main' => array(
// 		'full_width' => array(
// 			'field' => 'select',
// 			'class' => 'col-sm-20',
// 			'options' => array(
// 				'yes' => 'Yes',
// 				'no' => 'No',
// 			)
// 		),
// 	),

// 	'items' => array(
// 		'image' => array(
// 			'field' => 'image',
// 			'class' => 'col-sm-60'
// 		),
// 		'show_artists_impression' => array(
// 			'field' => 'select',
// 			'class' => 'col-sm-60',
// 			'options' => array(
// 				'yes' => 'Yes',
// 				'no' => 'No',
// 			)
// 		),
// 	),

// 	'limit' => '',
// 	'item_class' => 'col-sm-20',
// 	'image_sizes' => '',
// 	'file_path' => '',

// );

$config['section_type']['3_column_article'] = array(
	'hr_title' => '3 Column Article',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
	),

	'items' => array(
		'title' => array(
			'field' => 'text',
		),
		'description' => array(
			'field' => 'ckeditor',
		),
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-20'
		),
		
	),

	'limit' => '3',
	'item_class' => 'col-sm-60',
	'image_sizes' => '',
	'file_path' => '',

);

$config['section_type']['image-blocks'] = array(
	'hr_title' => 'Image Blocks',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'title' => array(
			'field' => 'text',
		),
		'description' => array(
			'field' => 'textarea',
		),
	),

	'items' => array(
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-30'
		),
		'link' => array(
			'field' => 'text'
		),
		'linkname' => array(
			'field' => 'text'
		),
	),

	'limit' => '2',
	'item_class' => 'col-sm-30',
	'image_sizes' => '',
	'file_path' => '',

);


$config['section_type']['image_text'] = array(
	'hr_title' => 'Image with text',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
	),

	'items' => array(
		'bgcolor' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
		'subheading' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'description' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-60',
		),
		
	),

	'limit' => '',
	'item_class' => 'col-sm-25',
	'image_sizes' => '',
	'file_path' => '',

);

$config['section_type']['full_image_with_text'] = array(
	'hr_title' => 'Full Background Image with text',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
	),

	'items' => array(
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
		'subheading' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'description' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-60',
		),
		
	),

	'limit' => '',
	'item_class' => 'col-sm-25',
	'image_sizes' => '',
	'file_path' => '',

);



$config['section_type']['section_image_text'] = array(
	'hr_title' => 'Section w/ Image & text',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-50',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'name' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'bgcolor' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'white' => 'White',
				'beige' => 'Beige',
				'lightbeige' => 'LightBeige',

			)
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),

		'logo' => array(
			'field' => 'image',
			'class' => 'col-sm-60',
		),
		'caption' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		
	),

	'items' => array(
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
		'subheading' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'description' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-60',
		),
		'position' => array(
			'field' => 'select',
			'class' => 'col-sm-30',
			'options' => array(
				'center' => 'Center',
				'left' => 'Left',
				'right' => 'Right'
			)
		),
		'size' => array(
			'field' => 'select',
			'class' => 'col-sm-30',
			'options' => array(
				'full' => '100%',
				'fourth' => '25%',
				'half' => '50%',
				'fifth'=> '15%'
				
			)
		),
	),

	
	'limit' => '',
	'item_class' => 'col-sm-25',
	'image_sizes' => '',
	'file_path' => '',

);

$config['section_type']['section_with_form'] = array(
	'hr_title' => 'Section w/ Form',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-50',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'bgcolor' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'white' => 'White',
				'beige' => 'Beige',
			)
		),
		'name' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),

		
		'caption' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		
	),

	'items' => array(
		'subheading' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'details' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'message' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
	),

	
	'limit' => '',
	'item_class' => 'col-sm-25',
	'image_sizes' => '',
	'file_path' => '',

);


$config['section_type']['section_video_text'] = array(
	'hr_title' => 'Section w/ Video with text',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-50',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'name' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'bgcolor' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'white' => 'White',
				'beige' => 'Beige',
			)
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),

		
		'caption' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		
	),

	'items' => array(
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
		'subheading' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'description' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-60',
		),
		'videourl' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
		'size' => array(
			'field' => 'select',
			'class' => 'col-sm-30',
			'options' => array(
				'full' => '100%',
				'fourth' => '25%',
				'half' => '50%',
				
			)
		),
	),

	
	'limit' => '',
	'item_class' => 'col-sm-25',
	'image_sizes' => '',
	'file_path' => '',

);

$config['section_type']['image_text_below'] = array(
	'hr_title' => 'Image with text below',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-30',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'bgcolor' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'white' => 'White',
				'beige' => 'Beige',
			)
		),
		'name' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'logo' => array(
			'field' => 'image',
			'class' => 'col-sm-30',
		),
		'subheading' => array(
			'field' => 'textarea',
			'class' => 'col-sm-50',
		),
		'description' => array(
			'field' => 'textarea',
			'class' => 'col-sm-50',
		),
		'author' => array(
			'field' => 'text',
			'class' => 'col-sm-50',
		),
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-30',
		),
		'signature' => array(
			'field' => 'image',
			'class' => 'col-sm-20',
		),
		
	),

	'limit' => '',
	'item_class' => 'col-sm-40',
	'image_sizes' => '',
	'file_path' => '',

);


$config['section_type']['section_text_logo'] = array(
	'hr_title' => 'Section text with Logo',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-50',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'name' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'logo' => array(
			'field' => 'image',
			'class' => 'col-sm-30',
		),
		'subheading' => array(
			'field' => 'textarea',
			'class' => 'col-sm-50',
		),
		'subheading2' => array(
			'field' => 'textarea',
			'class' => 'col-sm-50',
		),
		'description' => array(
			'field' => 'textarea',
			'class' => 'col-sm-50',
		),
		'author' => array(
			'field' => 'text',
			'class' => 'col-sm-50',
		),
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-30',
		),
		
	),

	'limit' => '',
	'item_class' => 'col-sm-40',
	'image_sizes' => '',
	'file_path' => '',

);


$config['section_type']['section_icon_blocks'] = array(
	'hr_title' => 'Section w/ Icon Blocks',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-50',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'name' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'bgcolor' => array(
			'field' => 'select',
			'class' => 'col-sm-20',
			'options' => array(
				'white' => 'White',
				'beige' => 'Beige',
			)
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
		'subheading' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		'description' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
		
	),

	'items' => array(
		
		'icon' => array(
			'field' => 'image',
			'class' => 'col-sm-60',
		),
		'caption' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),

		
	),

	
	'limit' => '',
	'item_class' => 'col-sm-15',
	'image_sizes' => '',
	'file_path' => '',

);

$config['section_type']['multi_images'] = array(
	'hr_title' => 'Multi Images Text',
	
	'main' => array(
		'full_width' => array(
			'field' => 'select',
			'class' => 'col-sm-30',
			'options' => array(
				'yes' => 'Yes',
				'no' => 'No',
			)
		),
		'bgcolor' => array(
			'field' => 'select',
			'class' => 'col-sm-30',
			'options' => array(
				'white' => 'White',
				'beige' => 'Beige',
			)
		),
		'title' => array(
			'field' => 'text',
			'class' => 'col-sm-30',
		),
		'subheading' => array(
			'field' => 'text',
			'class' => 'col-sm-60',
		),
		
		'caption' => array(
			'field' => 'textarea',
			'class' => 'col-sm-60',
		),
	),

	'items' => array(
		'image' => array(
			'field' => 'image',
			'class' => 'col-sm-50',
			
		),
		'size' => array(
			'field' => 'select',
			'class' => 'col-sm-30',
			'options' => array(
				'full' => 'Full',
				'half' => '50%',
				'fourth' => '25%',
			)
		),
	),

	'limit' => '4',
	'item_class' => 'col-sm-20',
	'image_sizes' => '',
	'file_path' => '',

);





// $config['section_type']['testimonial'] = array(
// 	'hr_title' => 'Testimonial',
	
// 	'main' => array(
// 		'full_width' => array(
// 			'field' => 'select',
// 			'class' => 'col-sm-20',
// 			'options' => array(
// 				'yes' => 'Yes',
// 				'no' => 'No',
// 			)
// 		),
// 	),

// 	'items' => array(
// 		'author' => array(
// 			'field' => 'text',
// 		),
// 		'quote' => array(
// 			'field' => 'textarea',
// 		),	
// 	),

// 	'limit' => '',
// 	'item_class' => 'col-sm-60',
// 	'image_sizes' => '',
// 	'file_path' => '',

// );


// $config['section_type']['featured_blog'] = array(
//     'hr_title' => 'Feature Article',

//     'main' => array(

//         'background_colour' => array(
//             'field' => 'select',
//             'class' => 'col-sm-30',
//             'options' => array(
//                 'grey' => 'Grey',
//                 'white' => 'White',
//                 'beige' => 'Beige',
//                 'black' => 'Black',
//             )
//         ),
//         'text_position' => array(
//             'field' => 'select',
//             'class' => 'col-sm-30',
//             'options' => array(
//                 'left' => 'Left',
//                 'right' => 'Right',
//             )
//         ),
//         'image' => array(
//             'field' => 'image',
//             'class' => 'col-sm-30',
//         ),
//         'small_heading' => array(
//             'field' => 'text',
//             'class' => 'col-sm-30',
//         ),
//         'large_heading' => array(
//             'field' => 'textarea',
//             'class' => 'col-sm-30',
//         ),
//         'intro' => array(
//             'field' => 'textarea',
//             'class' => 'col-sm-30'
//         ),

//         'link_url' => array(
//             'field' => 'text',
//             'class' => 'col-sm-15'
//         ),
//         'link_text' => array(
//             'field' => 'text',
//             'class' => 'col-sm-15',
//         ),
//     ),
//     'image_sizes' => '',
//     'file_path' => '',
// );