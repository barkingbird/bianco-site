<?php 

function validateTableName($tableName)
{
	if(($tableName = trim($tableName)) === '')
		throw new Exception('Valid Table name required for Query!!!');
	return $tableName;
}

/**
 * This function inserts data into DB
 * 
 * @param String $tableName
 * @param Array/Object $data
 * @param Boolean $multiInsert
 * 
 * @throws Exception if no data is provided
 * 
 * @return int the last inserted id from the DB 
 */
function insertInDbFromArray($tableName, $data, $multiInsert = false)
{
	if(!is_admin())
		return false;
	
	$tableName = validateTableName($tableName);
	if(!is_array($data) || count($data) === 0)
		throw new Exception('No data provided for insert');
			
	$ci = &get_instance();
	if($multiInsert === false)
		$ci->db->insert($tableName, $data);
	else
		$ci->db->insert_batch($tableName, $data);
		
	return $ci->db->insert_id();
}

/**
 * This function deletes data into DB
 *
 * @param String $tableName
 * @param String $where
 *
 * @throws Exception if no where conditioin is provided
 *
 * @return int the number of row(s) deleted from the DB
 */
function deleteFromDb($tableName, $where)
{
	if(!is_admin())
		return false;
	
	$tableName = validateTableName($tableName);
	if(!is_array($where) || count($where) === 0)
		throw new Exception('Where condition for delete must be provided as an array');
	
	$ci = &get_instance();
	$ci->db->delete($tableName, $where);
	return $ci->db->affected_rows();
}


?>