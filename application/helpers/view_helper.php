<?php

/*
 This function generates the drop down options for the page template value
*/
function generateTemplateOptionsForPage($pageTemplate = '')
{
	$templates = config_item('page_templates');
	$tArray = array('home' => array('display' => 'Home'), 'default' => array('display' => 'Page'), 'contact' => array('display' => 'Contact'));
	$returnValue = '';

	$pageTemplate = trim($pageTemplate);
	if($pageTemplate !== '' && !array_key_exists($pageTemplate, $templates))
		$pageTemplate = 'default';

	foreach($templates as $key => $value)
	{
		$selected = ($pageTemplate !== '' && $key === $pageTemplate) ? ' selected = selected ' : '';
		$returnValue .= '<option value="'.$key.'"'.$selected.'>'.$value['display'].'</option>';
	}

	return $returnValue;
}

function format_order_by_param($orderBy)
{
	if(strpos($orderBy, "~") === false)
		return array();
	$temp = explode("~", $orderBy);
	
	if(count($temp) < 3)
		return array();

	$output['alias'] = $temp[0];
	$output['column'] = $temp[1];
	$output['order'] = $temp[2];
	
	if(count($temp) > 3)
		$output['function'] = $temp[3];

	return $output;
}

function generate_asc_desc_symbol($orderArray, $tableName, $column, $link)
{
	if(!is_array($orderArray) || count($orderArray) === 0 || !isset($orderArray['alias']) || !isset($orderArray['column']) || !isset($orderArray['order']))
		return '';

	if(trim($orderArray['column']) === trim($column))
	{
		if($orderArray['order'] == 'asc')
		{
			$orderBy = $orderArray['alias'].'~'.$orderArray['column'].'~desc';
			return '<a href = "'.base_url($link.'&order_by='.$orderBy).'"><img src = "'.base_url('assets/images/sort-desc.png').'" alt = "ASC"></a>';
		}
		else
		{
			$orderBy = $orderArray['alias'].'~'.$orderArray['column'].'~asc';
			return '<a href = "'.base_url($link.'&order_by='.$orderBy).'"><img src = "'.base_url('assets/images/sort-asc.png').'" alt = "DESC"></a>';
		}
	}
	else
	{
		$orderBy = $tableName.'~'.$column.'~asc';
		return '<a href = "'.base_url($link.'&order_by='.$orderBy).'"><img src = "'.base_url('assets/images/sort-default.png').'" alt = "ASC"></a>';
	}
}
