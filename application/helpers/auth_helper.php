<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
    
    //static $current_user = FALSE;
           
    function encrypt_password($password = FALSE)
    {
        return sha1($password.config_item('encryption_key')); 
    }

    function admin_user()
    {
    	$ci 			= &get_instance();
    	$admin_user_id 	= $ci->session->userdata('admin_user_id');

    	$ci->db->select('admin_users.*, admin_groups.name AS group_name');
    	$ci->db->where('MD5(admin_users.id)', $admin_user_id);
    	$ci->db->join('admin_groups', 'admin_users.group_id = admin_groups.id', 'left');
    	$query = $ci->db->get('admin_users', 1);

    	return ($query->num_rows() == 1) ? $query->row() : FALSE;
    }

    function logged_in($redirect = FALSE)
    {
		$ci = &get_instance();
		$allowed = ($ci->session->userdata('admin_user_id')) ? TRUE : FALSE;

		return $allowed; 
    }

    function redirect_if_not_logged_in()
    {
		$ci = &get_instance();
		$allowed = ($ci->session->userdata('admin_user_id')) ? TRUE : FALSE;

		if (!$allowed)
		{
			set_message('info', 'Please login');
			redirect('admin/login');
		}

		return $allowed; 
    }

	function access_area($area = FALSE)
	{
		if (!logged_in())
		{
			set_message('info', 'Please login');
			redirect('admin/login');
		}

		if ($area)
		{
			$ci = &get_instance();
			$admin_group_id 	= $ci->session->userdata('admin_group_id');
			$permissions 		= config_item('access_permissions');

			if (isset($permissions[$admin_group_id]))
			{
				$permissions = $permissions[$admin_group_id];

				if ($permissions == 'ALL')
				{
					return TRUE;
				}
				elseif (in_array($area, $permissions))
				{
					return TRUE;
				}
			}
		}

		set_message('error', 'Permission Denied');
		redirect('admin');
	}

	function access_menu($area = FALSE)
	{
		if ($area)
		{
			$ci = &get_instance();
			$admin_group_id 	= $ci->session->userdata('admin_group_id');
			$permissions 		= config_item('access_permissions');

			if (isset($permissions[$admin_group_id]))
			{
				$permissions = $permissions[$admin_group_id];

				if ($permissions == 'ALL')
				{
					return TRUE;
				}
				elseif (in_array($area, $permissions))
				{
					return TRUE;
				}
			}
		}

		return FALSE;
	}


    /*
    static function user_id()
    {
        if(!@self::$current_user) self::get_user();        
        return (self::$current_user)? self::$current_user->id : FALSE;
    }

    static function admin_id()
    {
        $ci = &get_instance();
        return $ci->session->userdata('admin');
    }

    static function public_id()
    {
        if(!@self::$current_user) self::get_public_user();
        return (self::$current_user)? self::$current_user->id : FALSE;
    }
    
    static function master_id()
    {
        $ci         = &get_instance();
        $user_id    = $ci->session->userdata('admin');

        $ci->db->where('MD5(id)', $user_id);
        $ci->db->where('group_id', 1);
        $query = $ci->db->get('admin_users', 1);
        if($query->num_rows() != 1) return FALSE;
        
        self::$current_user = $query->row();

        return self::$current_user->id;
    }

    static function master_access()
    {
        if(!self::is_master())
        {
            set_message('error', 'Sorry you do not have access to this section');
            redirect('admin');
        }
    }

    static function group_id()
    {
        if(!@self::$current_user) self::get_user();
        return self::$current_user->group_id;
    }
    
    static function get_user()
    {
        $ci = &get_instance();
        $user_id = $ci->session->userdata('admin');
        
        if(!$user_id) {
            $user_id = $ci->session->userdata('user');
        }

        if(!$user_id)
        {
            self::$current_user = FALSE;
            return self::$current_user;
        }

        $ci->db->where('MD5(id)', $user_id);
        $query = $ci->db->get('admin_users', 1);
        //view($ci->db->last_query());
        self::$current_user = $query->row();

        return self::$current_user;
    }

    static function get_public_user()
    {
        $ci = &get_instance();
        $user_id = $ci->session->userdata('user');

        if(!$user_id)
        {
            self::$current_user = FALSE;
            return self::$current_user;
        }

        $ci->db->where('MD5(id)', $user_id);
        $query = $ci->db->get('users', 1);
        //view($ci->db->last_query());
        self::$current_user = $query->row();

        return self::$current_user;
    }

    static function is_user()
    {
        $ci = &get_instance();
        $user_id = $ci->session->userdata('admin');
        
        if(!$user_id) {
            $user_id = $ci->session->userdata('user');
        }
        $ci->db->where('MD5(id)', $user_id);
        $query = $ci->db->get('admin_users', 1);
        
        self::$current_user = $query->row();
    }

    static function has_access($section)
    {
        return true;
        //Master admin has access into everything do anything
        //$master = $this->session->userdata('master_admin');
        //if($master) return TRUE;
        $ci         = &get_instance();
        $segments = $ci->uri->segment_array();
        // $access = array(
        //     'admin/pages',
        //     'admin/users',
        //     'admin/blog'

        // );

        $access = config_item('admin_access');
        return (bool)(in_array($section, $access));
    }

    static function is_master()
    {
        $ci         = &get_instance();
        return $ci->session->userdata('master_admin');        
    }
    */
