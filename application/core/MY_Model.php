<?php

class MY_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function array_to_map($data = False, $value_field = False, $key_field = False)
	{
		$array = array();
		if(!$data || !$value_field) return FALSE;
		foreach ($data as $key => $value) {
			if($key_field)
			{
				$array[$value->$key_field] = $value->$value_field;
			}
			else
			{
				$array[] = $value->$value_field;
			}	
		}
		return $array;
	}
	
}



?>