<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="" lang="en"> <!--<![endif]-->

<head>
	<title><?php echo (($meta_title)? $meta_title  : config_item('site_name')).' - '.config_item('site_name'); ?></title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="<?php echo $meta_description; ?>">
	<meta name="google-site-verification" content="foBaEz_qfqB_pgAgPd9iwSShhrtewTWqllyCeygc7Ac" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="canonical" href="<?php echo base_url(substr($_SERVER['REQUEST_URI'], 1)); ?>">

	<link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicon.ico/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicon.ico/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon.ico/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicon.ico/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicon.ico/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicon.ico/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon.ico/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicon.ico/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon.ico/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/favicon.ico/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon.ico/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon.ico/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.ico/favicon-16x16.png">
	<link rel="manifest" href="/assets/images/favicon.ico/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/assets/images/favicon.ico/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">


	<?php 
		$settings = array(
			'logo' => site_settings::get_var('logo'),
			'phone' => site_settings::get_var('contact_phone'),
			'address_1' => site_settings::get_var('address_1'),
			'address_2' => site_settings::get_var('address_2'),
			'address_3' => site_settings::get_var('address_3'),
			'studio_address_1' => site_settings::get_var('studio_address_1'),
			'studio_address_2' => site_settings::get_var('studio_address_2'),
			'studio_address_3' => site_settings::get_var('studio_address_3'),
		);
		?>

	<!-- <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/fonts.css'); ?>" /> -->
	
	<?php if(isset($css) && count($css) > 0) : 
		foreach ($css as $key => $file) : ?>
			<link type="text/css" rel="stylesheet" href="<?php echo base_url($file); ?>" />
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if (isset($og) && is_array($og)): ?>
		<?php foreach ($og as $og_key => $og_value): ?>
			<meta property="og:<?php echo $og_key; ?>" content="<?php echo $og_value; ?>" />  
		<?php endforeach ?>
	<?php endif ?>

	<?php if(isset($js) && count($js) > 0) : ?>
		<?php  foreach ($js as $key => $file): ?>
			<script type="text/javascript" src="<?php echo base_url($file); ?>"></script>
		<?php endforeach ?>
	<?php endif; ?>

	<?php if(isset($map['js']) && $map['js']) : ?>
		<?php echo $map['js']; ?>
	<?php endif; ?>

	<!--[if lt IE 9]>
	<script src="<?php echo base_url('assets/js/lib/html5shiv.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/lib/respond.js'); ?>"></script>
	<![endif]-->

<!------------------- BUILD BY -----------------------
   ___           __    _             ___  _        __
  / _ )___ _____/ /__ (_)__  ___ _  / _ )(_)______/ /
 / _  / _ `/ __/  '_// / _ \/ _ `/ / _  / / __/ _  / 
/____/\_,_/_/ /_/\_\/_/_//_/\_, / /____/_/_/  \_,_/  
                           /___/                     	
----------------------------------------------------->

</head>
<body class="<?php echo 'template-'.@$template; ?> <?php echo 'page-'.url_title(uri_string()); ?>"  data-spy="scroll" data-target=".navbar" data-offset="50">

	<div id="wrap">
		<!-- Navigation -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top">
		  <div class="container">
		   <!--  <a class="navbar-brand" href="#"></a> -->
		    <div class="mobile-nav">
			    <a class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><!-- <span class="navbar-toggler-icon"></span> -->Menu</a>
			    <a class="reg" href="#register"> Register</a>
			</div>
		    <div class="collapse navbar-collapse" id="navbarResponsive">
		      <ul class="navbar-nav ml-auto">
		        <li class="nav-item">
		          <a class="nav-link" href="#architecture">Architecture
		                <span class="sr-only">(current)</span>
		              </a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="#interiors">Interiors</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="#lifestyle">Lifestyle</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="#team">Team</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="#register">Register</a>
		        </li>
		      </ul>
		    </div>
		  </div>
		</nav>

		<!-- <header>
			<div class="main-overlay"></div>
			<div class="overlay"></div>
			<video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
			<source src="assets/stone-water.mp4" type="video/mp4">
			</video>
			<div class="container h-100">
				<div class="d-flex h-100 text-center align-items-center">
					<div class="w-100 text-white">
						<h1 class="display-3">BIANCO<span>Brighton</span></h1>
						<h2>The best of Urban Coastal Living</h2>
						<p>A Boutique Collection of 22, 2 & 3 bedroom apartments.</p>
						<a class="go-to" href="#section1">Explore<i class="far fa-long-arrow-alt-down"></i></a>
					</div>
				</div>
			</div>
		</header> -->
		<main id="main">
			<?php echo get_message(); ?>
			<?php echo $page_content; ?>
		</main>
	</div>

	<div class="sticky-footer">
		<!-- <a href="tel:+61-3-0000-0000"><i class="fal fa-phone"></i>Call</a> -->
		<a href="/contact" style="border-right: none;width: 100%;"><i class="fal fa-envelope"></i>Enquire</a>
	</div>

	<footer id="footer" class="flex">
		<img class="center" src="<?php echo base_url('assets/images/bianco-gold-logo.png'); ?>">
		<span>Project Team</span>
		<div>
			<img class="half" src="<?php echo base_url('assets/images/damgar_logo.png'); ?>">
			<img class="half" src="<?php echo base_url('assets/images/cerastribley_logo.png'); ?>">
		</div>
		<p>In partnership with:</p>
		<img class="center" src="<?php echo base_url('assets/images/progressio-logo.png'); ?>">
		<p class="copyright">Creative by Barking Bird</p>
		<p>
			<a href="#popup1">Disclaimer</a>
			<a href="#popup2">Privacy Policy</a>
		</p>

		
		<div id="popup1" class="overlay">
			<div class="popup">
					<div class="container">
					<h2>Disclaimer</h2>
					<a class="close" href="#footer">&times;</a>
					<div class="content">
						<p>This website has been prepared for information purposes only. Bianco Brighton do not warrant or represent, either expressly or impliedly, the accuracy or completeness of information contained in this website. No information contained in this website or any conclusion derived from that information can be relied upon by any person as a warranty or representation. The provision of this website to any person does not constitute the giving of investment, financial or other advice. Interested parties should not rely on any information contained in this website and should make their own enquiries and consult their own professional advisers. Bianco Brighton, to the fullest extent permitted by law will not have any responsibility or liability for any loss or damage however arising in relation to the provision of this website, any person’s purported reliance on this website or any error in or omission from this website.</p>
					</div>
					<img class="center" src="<?php echo base_url('assets/images/bianco-logo-white.png'); ?>">
					<p class="copyright">Creative by Barking Bird</p>
					<p class="links">
						<a href="#popup1">Disclaimer</a>
						<a href="#popup2">Privacy Policy</a>
					</p>
				</div>
			</div>
		</div>
		
		<div id="popup2" class="overlay">
			<div class="popup">
					<div class="container">
					<h2>Privacy Policy</h2>
					<a class="close" href="#footer">&times;</a>
					<div class="content">
						<p>This website has been prepared for information purposes only. Bianco Brighton do not warrant or represent, either expressly or impliedly, the accuracy or completeness of information contained in this website. No information contained in this website or any conclusion derived from that information can be relied upon by any person as a warranty or representation. The provision of this website to any person does not constitute the giving of investment, financial or other advice. Interested parties should not rely on any information contained in this website and should make their own enquiries and consult their own professional advisers. Bianco Brighton, to the fullest extent permitted by law will not have any responsibility or liability for any loss or damage however arising in relation to the provision of this website, any person’s purported reliance on this website or any error in or omission from this website.</p>

						<p>Your privacy is important to us at Bianco Brighton. To protect the privacy of visitors to this website, and the privacy of its customers who provide personal information by other means to Bianco Brighton we adhere to the Privacy Act 1988 (The Act).</p>

						<h2>Use of Personal information</h2>
						<p>Generally, we will only use, store and disclose personal information that we collect about you so that we can do business together and for the reason/s it was collected. In the course of our functions and activities, Bianco Brighton collects personal information about subscribers and people who request information about our products and services. When you use an Bianco Brighton website or mobile application, the site or application may automatically collect information such as the IP address of your device, type of browser and your use of the website or application using “Cookies” and similar technologies. We may contact you in relation to the above via electronic messaging such as SMS and email, by mail, by fax, by phone or in any other lawful manner.</p>

						<h2>Disclosure of Personal Information</h2>
						<p>We may disclose your personal information to our employees and selected third parties, as we deem necessary to facilitate our dealings with you and provide our services, legal matters, products and properties (and/or information about the same). We do so in a lawful manner and do not sell your information to third parties.</p>

						<h2>Access, Correction and Opting Out</h2>
						<p>You can always opt out of receiving marketing material you receive from us via email, by following the instructions in the relevant email to unsubscribe, or opt out otherwise by contacting us on admin@damgargroup.com.au with your request.</p>

						<h2>Marketing</h2>
						<p>Bianco Brighton may send advertising mail to its customers where the advertising mail is related to the purpose for which the information was collected. If a customer no longer wishes to receive promotional information from Bianco Brighton, the customer should advise Bianco Brighton by email. Bianco Brighton will endeavor to amend their records within 30 days.</p>

						<h2>External Links</h2>
						<p>Bianco Brighton offers links to other sites that may not be operated by Bianco Brighton. If you visit one of these linked sites, you should review their privacy and other policies. Bianco Brighton is not responsible for the policies and practices of other companies.</p>

						<h2>Concerns or Complaints</h2>
						<p>If you have a concern or complaint in relation to our handling of your personal information or you believe that we have breached the Act or any other applicable privacy laws or codes, you can contact us by email at  admin@damgargroup.com.au. We will investigate your concerns and will respond to you in writing as soon as possible (usually within 14 working days).</p>

						<h2>Changes to Bianco Brighton’s Privacy Policy</h2>
						<p>Bianco Brighton reserves the right to change this Privacy Policy at any time. Bianco Brighton will notify of any changes by posting an updated version of the policy on Bianco Brighton’s website at BiancoBrighton.COM.AU. This Privacy Policy was last updated Dec 2018.</p>
					</div>
					<img class="center" src="<?php echo base_url('assets/images/bianco-logo-white.png'); ?>">
					<p class="copyright">Creative by Barking Bird</p>
					<p class="links">
						<a href="#popup1">Disclaimer</a>
						<a href="#popup2">Privacy Policy</a>
					</p>
				</div>
			</div>
		</div>
	</footer>
		
	



	<?php /* need to render all possible recaptcha fields on the page */ ?>
	<?php /* echo $this->recaptcha->add_reCAPTCHA(array('')); */?>

	<script type="text/javascript">
		var base_url = '<?php echo base_url() ?>';
	</script>

	<script type="text/javascript">
	<?php
	if (isset($_POST) && isset($_POST['ref']) && $_POST['ref']) :
		foreach ($this->form_validation->form_errors_array() as $error_key => $error_value) :
	?>
	$('form[name="<?php echo $_POST['ref']; ?>"]').find('[name="<?php echo $error_key; ?>"]').addClass('form-error');
	<?php			
		endforeach;
	endif; 
	?>
	</script>

</body>
</html>