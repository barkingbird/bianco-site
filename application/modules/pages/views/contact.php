<?php 
if(isset($gallery_images) && $gallery_images) : 
	$this->load->view('pages/_partials/hero_gallery', array('images' => $gallery_images));
endif; 
?>
<section class="contact">
<?php if ($row->sub_heading || $row->detail) : ?>
<div class="page-content">
	<div class="container">
		
		<?php if ($row->sub_heading) : ?>
		<h1 class="heading animateMe" data-animation="fadeIn" data-animation-delay="0.6s"><?php echo $row->sub_heading; ?></h1>
		<?php endif; ?>

		<?php if ($row->detail) : ?>
		<div class="wysiwyg-content animateMe" data-animation="fadeIn" data-animation-delay="0.7s">
			<?php echo filter_wysiwyg($row->detail); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
<?php endif; ?>

<?php 
if ($page_sections) : 
	foreach ($page_sections as $section) : 
		echo $section->html; 
	endforeach;
endif;
?>


	<div class="container">
		<div class="register__form here">
		<?php
			$form_name = 'contact';
			$form_id = 'NQ';
			$sent = FALSE;
			$errors = FALSE;

			if (isset($form_result) && $form_result) 
			{
				if (isset($form_result->form_name) && $form_result->form_name == $form_name) 
				{
					$errors = TRUE;
					if (isset($form_result->form_success) && $form_result->form_success == TRUE) 
					{
						$sent = TRUE;
						$errors = FALSE;
					}
				}
			} 
			?>
				<?php if ($sent) : ?>

				<div class="form-thankyou">
					<h1 class="h2">Thank you for your enquiry.</h1>
				</div>

				<?php else : ?>

				<?php if ($errors && validation_errors()) : ?>
				<div class="form-validation-errors">
					<p>Please fill in the required fields.</p>
					<p><?php echo $errors; ?></p>
				</div>
				<?php endif; ?>


				<form class="animateMe" data-animation="fadeIn" data-animation-delay="0.8s" method="POST" action="" name="<?php echo $form_name; ?>" id="enquiry">
					<input type="hidden" name="ref" value="<?php echo $form_name; ?>" />
					<input type="hidden" name="form" value="<?php echo $form_id;?>" />
					<input type="hidden" name="db_lead_source" value="Web" />

					<div class="contact-row">
						<input type="text" name="db_first_name" placeholder="Name*" value="<?php echo set_value('db_first_name'); ?>" required />
						<input type="text" name="db_phone" placeholder="Phone*" value="<?php echo set_value('db_phone'); ?>" required />
					</div>
					
					<!-- <div class="contact-row">
						<div class="custom-select">
							<select name="db_source" required >
								<option selected disabled>How did you hear about us?*</option>
								<option value="Google Search" <?php echo set_select('db_source', 'Google Search'); ?> >Google Search</option>
								<option value="Realestate.com.au" <?php echo set_select('db_source', 'Realestate.com.au'); ?> >Realestate.com.au</option>
								<option value="Signage" <?php echo set_select('db_source', 'Signage'); ?> >Signage</option>
								<option value="Walk-in" <?php echo set_select('db_source', 'Walk-in'); ?> >Walk-in</option>
								<option value="Word of month" <?php echo set_select('db_source', 'Word of month'); ?> >Word of month</option>
							</select>
						</div>
					</div> -->
					<input type="email" name="db_email" placeholder="Email*" value="<?php echo set_value('db_email'); ?>" required />
					<textarea name="db_message" placeholder="Enter your enquiry here..." value="<?php echo set_value('db_message'); ?>" required > </textarea>
					<span>*Required fields</span>
					<button type="submit" class="button button--secondary register__submit">Submit</button>
				</form>
			<?php endif; ?>
		</div>
	</div>
</section>