<!-- Can be either custom (hardcode) content or default template -->
<?php 
if(isset($gallery_images) && $gallery_images) : 
	$this->load->view('pages/_partials/home_gallery', array('images' => $gallery_images));
endif; 
?>

<?php if ($row->sub_heading || $row->detail) : ?>
<div class="page-content">
	<div class="container">
		
		<?php if ($row->sub_heading) : ?>
		<h1 class="heading"><?php echo $row->sub_heading; ?></h1>
		<?php endif; ?>

		<?php if ($row->detail) : ?>
		<div class="wysiwyg-content">
			<?php echo filter_wysiwyg($row->detail); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
<?php endif; ?>

<?php 
if ($page_sections) : 
	foreach ($page_sections as $section) : 
		echo $section->html; 
	endforeach;
endif;
?>