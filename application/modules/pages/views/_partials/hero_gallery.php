
<?php if(isset($images) && $images) : ?>
	<div class="flexslider-wrapper">
		<div class="flexslider hero-gallery js-fade-gallery ">
			<ul class="slides">
				<?php foreach ($images as $image) : ?>
					<li>
						<div class="image-center"  style="background-image: url('<?php echo display_image($image->image, 'full_'); ?>');"> 
							<?php if ($image->video_url): $embed_url = rectify_youtube_url($image->video_url);?>
								<div class="fullscreen-video-background">
									<div class="_pattern-overlay"></div>
									<div class="_buffering-background">
										<img src="<?php echo display_image($image->image, 'full_'); ?>" alt="<?php echo @$image->description; ?>" class="" />
									</div>
									<div class="_youtube-iframe-wrapper">
										<div id="_youtube-iframe" data-youtubeurl="<?php echo $embed_url; ?>"></div>
									</div>
								</div>
							<?php else: ?>
								<img src="<?php echo display_image($image->image, 'full_'); ?>" alt="<?php echo @$image->description; ?>" class="" />
							<?php endif ?>

						</div>
						<?php if ($image->title) : ?>
						<i class="hero-gallery__overlay"></i>
						<?php endif; ?> 
						<?php if (isset($image->mp4)) : ?>
						<video class="video-js vjs-default-skin vjs-big-play-centered cover autoplay"
							controls preload="auto" width="640" height="480"
							poster="<?php echo display_image($image->image, $image_quality); ?>"
						>
							<source src="<?php echo base_url($image->mp4); ?>" type="video/mp4" />
						</video>
						<?php endif; ?>
						<?php if (isset($image->title) || isset($image->description)) : ?>
						<div class="overlay"></div>
							<div class="caption caption--<?php echo $image->caption_position?> container">
								
								<div class="valign-parent">
									<div class="main-logo flex">
										<img src="<?php echo base_url('assets/images/bianco-logo-white.png'); ?>" alt="Bianco">
									</div>
									<i class="valign"></i><span class="valign">
								<?php if ($image->title) : ?>
								<p class="caption-heading"><?php echo $image->title ; ?></p>
								<?php endif; ?>
								<?php if (isset($image->description) && $image->description) : ?>
								<p class="caption-description"><?php echo $image->description; ?></p>
								<?php endif; ?>
								<?php if ($image->link) : ?>
								<a class="button button--primary" href="<?php echo base_url($image->link); ?>">
									<?php echo ($image->link_text)? $image->link_text : 'Learn More'; ?>
								</a>
								<?php endif; ?>
								<a class="go-to" href="#video">Explore <i class="fas fa-long-arrow-alt-down"></i></a>
								</span></div>

							</div>
						<?php endif; ?>

						<?php if ($image->artists_impression == 'yes') : ?>
						<span class="artist-impression">
							Artist's Impression
						</span>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
<?php endif; ?>
