
<?php if(isset($images) && $images) : ?>
	<div class="flexslider-wrapper">
		<div class="flexslider hero-gallery js-fade-gallery ">
			<ul class="slides">
				<?php foreach ($images as $image) : ?>
					<li>
						<div class="image-center"  style="background-image: url('<?php echo display_image($image->image, 'full_'); ?>');"> 
							<img src="<?php echo display_image($image->image, 'full_'); ?>" alt="<?php echo @$image->description; ?>" class="" />
						</div>
						<?php if ($image->title) : ?>
						<i class="hero-gallery__overlay"></i>
						<?php endif; ?> 
						<?php if (isset($image->mp4)) : ?>
						<video class="video-js vjs-default-skin vjs-big-play-centered cover autoplay"
							controls preload="auto" width="640" height="480"
							poster="<?php echo display_image($image->image, $image_quality); ?>"
						>
							<source src="<?php echo base_url($image->mp4); ?>" type="video/mp4" />
						</video>
						<?php endif; ?>
						<?php if (isset($project->title) || isset($project->intro)) : ?>
							<div class="caption caption--<?php echo $image->caption_position?> container">
								<div class="valign-parent">
									<span class="valign">
										<?php if ($project->title) : ?>
										<p class="animateMe caption-heading" data-animation="fadeIn"><?php echo $project->title ; ?></p>
										<?php endif; ?>
										<?php if (isset($project->intro) && $project->intro) : ?>
										<p class="animateMe caption-description" data-animation="fadeIn"><?php echo $project->intro; ?></p>
										<?php endif; ?>
									</span>
									<div class="animateMe hero-gallery__anchor"  data-animation="fadeInUp">
										<a href="#project-detail" class="js-scroll-to hero-gallery__anchor__link"><i class="fal fa-long-arrow-down"></i> Information below</a>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
			
		</div>
	</div>
<?php endif; ?>
