<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Pages extends MX_Controller {
    
    var $permalink;
    var $data;
    
    public function __construct()
    {
        parent::__construct();
        $this->data         = array(
            'page_content'  => FALSE
        );
        $this->permalink    = uri_string();
        modules::run('layout/autoload/initiate');
        $this->data['form_result'] = (object) array('form_name' => FALSE, 'form_success' => FALSE);
        $this->load->model('page');
        $this->load->model('sections/section');
        //$this->output->enable_profiler(TRUE);
    }

    public function index()
    {
        $url                  = $this->permalink;
        $this->data['row']    = $this->page->get_permalink($url);

        if ($this->input->post('ref'))
        {   
            $this->data['form_result'] = $this->_contact_submission($this->data);
            if ($this->data['form_result']->form_name === 'register' && $this->data['form_result']->form_success)
            {
                redirect('/');
            }
        }

        /** constants */
        $this->data['address_1'] = Site_settings::get_var('address_1') ? Site_settings::get_var('address_1') : FALSE;
        $this->data['address_2'] = Site_settings::get_var('address_2') ? Site_settings::get_var('address_2') : FALSE;
        $this->data['address_3'] = Site_settings::get_var('address_3') ? Site_settings::get_var('address_3') : FALSE;
        $this->data['contact_phone'] = Site_settings::get_var('contact_phone') ? Site_settings::get_var('contact_phone') : FALSE;
        $this->data['contact_email'] = Site_settings::get_var('contact_email') ? Site_settings::get_var('contact_email') : FALSE;
        $this->data['instagram'] = Site_settings::get_var('instagram') ? Site_settings::get_var('instagram') : FALSE;
        $this->data['facebook'] = Site_settings::get_var('facebook') ? Site_settings::get_var('facebook') : FALSE;
        $this->data['linkedin'] = Site_settings::get_var('linkedin') ? Site_settings::get_var('linkedin') : FALSE;
        $this->data['vimeo'] = Site_settings::get_var('vimeo') ? Site_settings::get_var('vimeo') : FALSE;
        $this->data['houzz'] = Site_settings::get_var('houzz') ? Site_settings::get_var('houzz') : FALSE;
        $this->data['gmap_get_dir'] = Site_settings::get_var('gmap_get_dir') ? Site_settings::get_var('gmap_get_dir') : FALSE;
        $this->data['address'] = Site_settings::get_var('address') ? Site_settings::get_var('address') : FALSE;
        $this->data['opening_hours'] =  Site_settings::get_var('opening_hours') ? Site_settings::get_var('opening_hours') : FALSE;
        /** END constants */


        $blog_url = rtrim(page_url_by_template('blog'), '/');
        $project_url            = project_url();

        if ($blog_url && strpos(uri_string(), $blog_url) !== FALSE)
        {
        	$this->data['row']    = $this->page->get_permalink($blog_url);
            $this->data = array_merge($this->data, modules::run('blog', $this->data));
        }
        elseif($project_url && strpos(uri_string().'/', $project_url) !== FALSE)
        {
            if(isset($this->data['row']->template) && $this->data['row']->template)
            {
                $this->data['template'] = $this->data['row']->template;
            }
            // get all sections
            if (isset($this->data['row']->id))
            {
                $this->data['page_sections'] = $this->section->get_sections($this->data['row']->id);
            }
            $this->data = array_merge($this->data, modules::run('projects', $this->data));
            
            
        }
        elseif ($this->data['row'])
        {
            $this->data['page_content']  = $this->_page();
        }

        if (!$this->data['page_content'])
        {
            //1st lets check to see if we have any old pages
            $this->page->redirects($url);

            header("HTTP/1.1 404 Not Found");
            $this->data['row'] = $this->page->get_permalink('404');
            if($this->data['row'] === false)
            {
				$this->data['page_content'] = '<div class="container mg-t-lg">The Requested Page Is Not Found!</div>';
            }
            else 
            {
            	$this->data['page_content']  = $this->_page();
            }
        }

        //footer links
        for($i = 0; $i < 6; $i++)
        {
            $this->data['footerlinks'][$i] = $this->page->footer_links($i);
        }

        $this->data['site_name']          = config_item('site_name');
       
        $this->data['css']    = isset($this->data['css'])? array_merge($this->data['css'], config_item('css')) : config_item('css');
        $this->data['js']     = isset($this->data['js'])? array_merge($this->data['js'], config_item('js')) : config_item('js');
        
        if (isset($this->data['row'])) 
        {
//            $this->data['meta_keywords']      = (@$this->data['row']->meta_keywords) ? $this->data['row']->meta_keywords : generate_keywords($this->data['page_content']);
            $this->data['meta_description']   = @$this->data['row']->meta_description;
            $this->data['meta_title']         = (@$this->data['row']->meta_title)? $this->data['row']->meta_title : @$this->data['row']->title;
            $this->data['current_page']       = url_title(@$this->data['row']->permalink, '-', TRUE);
            $this->data['template'] 	 	  = @$this->data['row']->template;
        }

        $this->load->view('template', $this->data);
    }

    function _page()
    {
        /// check if the page is published using the status of the page, if no, then if the user is master, go forward, otherwise show 404 ///
        if ($this->data['row']->status != 1) 
        {
            if(logged_in())
            {
                set_message('info', 'This page is not published');
            }
            else
            {
                show_404();
            }
        }

        // get all sections
        $this->load->model('sections/section');
        $this->data['page_sections'] = $this->section->get_sections($this->data['row']->id, $this->data);
        
        //get gallery images
        $this->data['gallery']  = FALSE;

        if($this->data['row']->gallery_id) 
        {
            $this->data['gallery']          = $this->gallery->get_gallery($this->data['row']->gallery_id);
            $this->data['gallery_images']   = $this->gallery->get_images($this->data['row']->gallery_id);
        }
        
        return $this->load->view($this->data['row']->template, $this->data, TRUE);
    }

    function _contact_submission($data = FALSE) 
    {

        $form = $this->input->post('ref'); 

        $method = '_'.$form.'_form_validation';

        if (is_callable(array($this, $method))) 
        {

            call_user_func(array($this, $method));

            $this->form_validation->set_error_delimiters('', '<br />');
        
            if($this->form_validation->run()) 
            {   
                /** Akismet checking spam */
                $this->load->library('MicroAkismet');
                $akismet	= new MicroAkismet( '752341d02fce', current_url(), 'barkingbird' );

                $vars 							= array();
                $vars["user_ip"]           		= @$_SERVER["REMOTE_ADDR"];
                $vars["user_agent"]        		= @$_SERVER["HTTP_USER_AGENT"];
                $vars["referrer"] 				= @$_SERVER['HTTP_REFERER'];
                $vars['comment_type'] 			= 'Registration';
                $vars["comment_content"]   		= @$_POST["db_message"];
                $vars["comment_author"]			= isset($_POST["db_name"]) ? @$_POST["db_name"] : @$_POST["db_firstname"];
                $vars["comment_author_email"]	= @$_POST["db_email"];
                $is_spam = $akismet->check( $vars );

                $this->load->library('leads_submission');

                // second parameter is the prefix for the field name
                $sent = $this->leads_submission->add_lead($_POST, 'db_', $is_spam);

                if($sent )
                {
                    //$this->form_validation->clear_field_data();
                    //set_message('success', 'Thankyou. Your message has sent');
                    return (object) array('form_name' => $form, 'form_success' => TRUE);
                }
                else
                {
                	set_message('error', 'An error has occurred');
                }

            }
            else if ($errors = validation_errors())
            {
                set_message('error', $errors);
            }
        }
        //unset($_POST['ref']);
        return (object) array('form_name' => $form, 'form_success' => FALSE);
    }

    private function _contact_form_validation()
    {
        $this->form_validation->set_rules('db_first_name', 'First Name', 'trim|required');
        //$this->form_validation->set_rules('db_last_name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('db_email', 'Email', 'trim|valid_email|required');
        $this->form_validation->set_rules('db_phone', 'Phone', 'trim|required');
        //$this->form_validation->set_rules('db_source', 'Where did you hear about us', 'trim|required');
        $this->form_validation->set_rules('db_message', 'Message', 'trim|required');
        //$this->form_validation->set_rules('g-recaptcha-response', 'ReCaptcha Reponse', 'verify_recaptcha|required');
    }

    public function email($path, $data = array()) 
    {
        $ci = &get_instance();
        $template = 'email/template';

        $data['page_content'] = $ci->load->view($path, $data, TRUE);
        return $ci->load->view($template, $data, TRUE);
    }

    public function sitemap()
    {
        $data['pages'] = $this->page->step_sitemap();

        // News
        $this->load->model('blog/posts');
        $posts = $this->posts->sitemap();

        if(array_search(base_url('news'), $data['pages']) !== false)
            unset($posts['news']);

        $data['pages'] =  array_merge($data['pages'], $posts);
       
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->load->view("sitemap",$data);
    }

    
}