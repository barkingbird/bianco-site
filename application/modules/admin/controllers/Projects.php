<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('auth_helper');
		access_area('admin/projects');

		$this->load->helper('ckeditor');
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
		$this->ckfinder->SetupCKEditor($this->ckeditor,'/assets/admin/ckfinder/'); 

		$this->load->model('admin/project');
        $this->load->model('admin/gallery');
        $this->load->library('thumb');
        $this->load->helper('model');

        //$this->output->enable_profiler(TRUE);
	}

	public function index()
	{
		$data['projects'] = $this->project->build_list();
		$this->layout->view('admin/projects/manage', $data);
	}

	public function remove()
	{
		$project_id = decode($this->uri->segment('4'));
		if(!$project_id) 
		{
			set_message('error', 'Could not find that project.');
			redirect(base_url('admin/projects'));
		}
		
		$projectIdsDeleted = array();
		if($this->project->remove_row($project_id, $projectIdsDeleted))
		{
			set_message('success', 'Project has been removed.');
		}
		else
		{
			set_message('error', 'Could not find that project.');
		}

		redirect(base_url('admin/projects'));
	}

	/**
	 * new project
	 *
	 * @return void
	 **/
	public function new_project()
	{
		$data['projects'] 			= $this->project->get_all();
		$data['categories'] 				= $this->project->categories();

		//used for the page templates
		//$data['templates'] 		= config_item('page_templates');

		$this->layout->view('admin/projects/new', $data);
	}

	/**
	 * save an edit
	 *
	 * @return void
	 **/
	public function save_new()
	{
		$data = array();
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('subtitle', 'Sub Title', 'trim');
		$this->form_validation->set_rules('width', 'Full Width', 'trim');
		$this->form_validation->set_rules('address', 'Address', 'trim');
		$this->form_validation->set_rules('type', 'Type', 'trim');
		$this->form_validation->set_rules('budget', 'Budget', 'trim');
		$this->form_validation->set_rules('commenced', 'Commenced', 'trim');
		$this->form_validation->set_rules('architect', 'Architect', 'trim');
		$this->form_validation->set_rules('interior', 'Interior Designer', 'trim');
		$this->form_validation->set_rules('landscaper', 'Landscaper', 'trim');
		$this->form_validation->set_rules('detail', 'Page Content', 'trim');
		$this->form_validation->set_rules('meta_title', 'Meta Title', 'trim');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'trim');
		$this->form_validation->set_rules('permalink', 'Permalink', 'trim|required');

		if ($this->form_validation->run()) 
		{
			$_POST['safe_name'] = url_title($_POST['title'], '-', true);
			$existing_project 	= $this->project->get_project_by_safename($_POST['safe_name']);

			if($existing_project !== false && is_object($existing_project))
				set_message('error', 'Another project exists in the system with the same title.');
			else
			{
				$success	= $this->project->new_row($_POST);

				if($success) 
				{
					set_message('success', 'Project has been created.');
					redirect(base_url('admin/projects'));
				} 
				else 
				{
					set_message('error', 'There was an error saving the project, please try again.');
				}
			}
		}
		elseif($error = validation_errors())
		{
			set_message('error', $error);
		}
		$data['projects'] = $this->project->get_all();
		$this->layout->view('admin/projects/new', $data);
	}

	/**
	 * edit a project
	 *
	 * @return void
	 **/
	public function edit()
	{

		$project_id 			= decode($this->uri->segment('4'));
		$data['project'] 		= $this->project->get_project($project_id);
		$data['projects'] 		= $this->project->get_all($project_id);
		//$data['galleries'] 	= $this->gallery->get_all();
		$data['categories'] 				= $this->project->categories();
		$assigned_categories 				= $this->project->get_assigned_categories($project_id);

		$data['assigned_categories'] 		= array();

		foreach($assigned_categories as $ack => $acv)
		{
			$data['assigned_categories'][] = $acv->category_id;
		}

		if(!$data['project']) 
		{
			set_message('error', 'Selected project could not be found');
			redirect(base_url('admin/projects'));
		}
		if(!$data['project']->gallery_id)
		{
			//create a Gallery
        	$this->db->insert('gallery', array('title' => $data['project']->title.' Gallery'));
			$data['project']->gallery_id = $this->db->insert_id();
			$this->project->update_row($project_id, (array)$data['project']);
			$data['project'] = $this->project->get_project($project_id);
		}
		
		$data['gallery_images']	= $this->gallery->get_all_images_for_gallery($data['project']->gallery_id);

		//used for the page templates
		// $data['templates'] 		= config_item('page_templates');

		$this->layout->view('admin/projects/edit', $data);		
	}

	/**
	 * save an edit
	 *
	 * @return void
	 **/
	public function save_edit()
	{
		$project_id 		= decode($this->uri->segment('4'));
		$data['project'] 	= $this->project->get_project($project_id);

		if(!$data['project'])
		{
			set_message('error', 'Invalid Project ID provided');
			redirect(base_url('admin/projects'));
		}

		$this->_project_edit_validate();
		if($this->form_validation->run()) 
		{
			$path = config_item('gallery_path').$_POST['permalink'];
            $this->_upload_gallery_file($_POST['gallery_id'], $path);
            $this->_update_existing_gallery_file($path);
			unset($_POST['images']);

			//Have we changed the urls?
			if ($data['project']->permalink != $_POST['permalink']) 
			{
				$this->project->make_redirect($data['project']->permalink, $_POST['permalink']);
			}
			$success = $this->project->update_row($project_id, $_POST);
			if($success) 
            {
       			$redirect = '';
				if (isset($_FILES['image']['size']))
				{
					$redirect = '#hero-gallery';
				}

				set_message('success', 'Project has been saved.');
				redirect(base_url('admin/projects/edit/'.encode($project_id).$redirect));
			} 
			else 
				set_message('error', 'Selected project could not be found.....');
		}
		elseif($error = validation_errors())
		{
			set_message('error', $error);
		}

		redirect(base_url('admin/projects/edit/'.encode($project_id)));
	}

	private function _update_existing_gallery_file($path)
	{
		if(isset($_POST['images']))
		{
			if(is_array($_POST['images']))
			{
				$files = $_FILES;
				foreach($_POST['images'] as $key => $value)
				{
					if(isset($files['hero_images']) && $files['hero_images']['name'][$key]['video_file'] != '')
			        {
			            $config['upload_path']      = $path;
			            $config['allowed_types']    = '*';
			            $config['remove_spaces']    = TRUE;
			            $config['detect_mime']    	= FALSE;

			            $_FILES['dummy'] = array(
			            	'name'		=> $files['hero_images']['name'][$key]['video_file'],
			            	'type'		=> $files['hero_images']['type'][$key]['video_file'],
			            	'tmp_name'	=> $files['hero_images']['tmp_name'][$key]['video_file'],
			            	'error'		=> $files['hero_images']['error'][$key]['video_file'],
			            	'size'		=> $files['hero_images']['size'][$key]['video_file'],
			            );
			            
			            $this->upload->initialize($config);

			            if (! $this->upload->do_upload('dummy')){
			                set_message('error', 'Sorry there was an issue uploading Video. '.$this->upload->display_errors());
			                die($this->upload->display_errors());
			            }
			            else
			            {
			                $file = $this->upload->data();
			                $value['mp4'] = str_replace(ROOT, '', $file['full_path']);
			            }
			        }
			        if(isset($files['hero_images']) && $files['hero_images']['name'][$key]['image'] != '')
			        {

			            $config['upload_path']      = $path;
			            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            			$config['remove_spaces']    = TRUE;

			            $_FILES['dummy'] = array(
			            	'name'		=> $files['hero_images']['name'][$key]['image'],
			            	'type'		=> $files['hero_images']['type'][$key]['image'],
			            	'tmp_name'	=> $files['hero_images']['tmp_name'][$key]['image'],
			            	'error'		=> $files['hero_images']['error'][$key]['image'],
			            	'size'		=> $files['hero_images']['size'][$key]['image'],
			            );
			            
			            $this->upload->initialize($config);

			            if (! $this->upload->do_upload('dummy')){
			                set_message('error', 'Sorry there was an issue uploading Video. '.$this->upload->display_errors());
			                die($this->upload->display_errors());
			            }
			            else
			            {
			                $file = $this->upload->data();
                			$value['image'] = str_replace(ROOT, '', $file['full_path']);				
							$this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
			            }
			        }

					$this->gallery->update_image_row($key, $value);
				}					
			}
			unset($_POST['images']);
		}
	}

	public function _upload_gallery_file($gallery_id = FALSE, $path = FALSE)
	{
		if(!$gallery_id || !$path) return FALSE;

		make_folder(ROOT.$path);
		$this->load->library('upload');
		$this->load->library('thumb');
		$data = array();

		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('image'))
                set_message('error', 'Sorry there was an issue uploading Image. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['image'] = str_replace(ROOT, '', $file['full_path']);				
				$this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
            }
        }
		if(isset($_FILES['image_hover']) && $_FILES['image_hover']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('image_hover'))
                set_message('error', 'Sorry there was an issue uploading Image. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['image_hover'] = str_replace(ROOT, '', $file['full_path']);
				$this->thumb->make_thumbs($file['full_path'], 'hero_sizes');
            }
        }
        if(isset($_FILES['webm']) && $_FILES['webm']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'webm';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('webm'))
                set_message('error', 'Sorry there was an issue uploading video. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['webm'] = str_replace(ROOT, '', $file['full_path']);
            }
        }
        if(isset($_FILES['mp4']) && $_FILES['mp4']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = '*';
            $config['remove_spaces']    = TRUE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('mp4'))
                set_message('error', 'Sorry there was an issue uploading video. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['mp4'] = str_replace(ROOT, '', $file['full_path']);
            }
        }
        if(isset($_FILES['new_image_video_file']) && $_FILES['new_image_video_file']['name'] != '')
        {
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'mp4';
            $config['remove_spaces']    = TRUE;
            $config['detect_mime']    	= FALSE;
            
            $this->upload->initialize($config);

            if (! $this->upload->do_upload('new_image_video_file'))
                set_message('error', 'Sorry there was an issue uploading video. '.$this->upload->display_errors());
            else
            {
                $file = $this->upload->data();
                $data['mp4'] = str_replace(ROOT, '', $file['full_path']);
            }
        }

        if (count($data) > 0) 
        {
        	$data['gallery_id'] = $gallery_id;
        	//$data['width'] = @$_POST['new_image_width'];
        	//$data['height'] = @$_POST['new_image_height'];
        	$data['title'] = @$_POST['new_image_title'];
        	$data['description'] = @$_POST['new_image_description'];
        	$data['caption_position'] = @$_POST['new_image_caption_position'];
        	$data['link'] = @$_POST['new_image_link'];
        	$data['link_text'] = @$_POST['new_image_link_text'];
        	$this->db->insert('gallery_images', $data);
        }

        unset($_POST['new_image_width']);
        unset($_POST['new_image_height']);
        unset($_POST['new_image_title']);
        unset($_POST['new_image_description']);
        unset($_POST['new_image_caption_position']);
        unset($_POST['new_image_link']);
        unset($_POST['new_image_link_text']);
        
	}

	public function publish()
	{
		$project_id = $this->uri->segment('4');
		$project_status = (int)$this->uri->segment('5');
		$success = $this->project->update_status($project_id, array('status' => $project_status));
		
		if($success) {
			set_message('success', 'Project status updated.');
		} else {
			set_message('error', 'Selected project could not be found.');
		}
		redirect(base_url('admin/projects'));
	}

	public function remove_project_image()
	{
        $projectId = decode($this->uri->segment(4));
        $project = $this->project->get_project($projectId);

        if(!$project)
        {
            set_message('error', 'That Work could not be found.');
            redirect('admin/projects/edit/'.encode($projectId));
        }

        $success = $this->project->remove_project_image($projectId);
        if($success)
            set_message('success','Project Image has been removed');
        else
            set_message('error','There was an issue removing the Project Image. Please try again.');

        redirect('admin/projects/edit/'.encode($projectId));
	}

	//DRAG-N-DROP - gallery_images
	public function upload_gallery_image()
	{

		$gallery_id    		= decode($this->uri->segment(4));
		$project_id    		= decode($this->uri->segment(5));
		$data['gallery'] 	= $this->gallery->get_gallery_by_id($gallery_id);
		$project				= $this->project->get_project($project_id);

        if($project && $data['gallery'] && $_FILES["file"]["name"] != '') 
        {
        	$path = config_item('gallery_path').$project->permalink;

            make_folder(ROOT.$path);
            $config['upload_path']      = ROOT.$path;
            $config['check_mime']       = FALSE;
            $config['allowed_types']    = 'gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG';

            $this->load->library('upload');
            $this->upload->initialize($config);
            if (! $this->upload->do_multi_upload('file'))
            {
                respond('error: '.$this->upload->display_errors());
                return;
            }
            $file = $this->upload->data();
            $this->load->library('thumb');
            				
			$this->thumb->make_thumbs($file['full_path'], 'hero_sizes');

            $project_data['image'] 		= $path.$file['file_name'];
            $project_data['thumb'] 		= str_replace(ROOT, '', $file['full_path']);
            $project_data['main'] 			= $path.$file['raw_name'].'_main'.$file['file_ext'];
            $project_data['gallery_id'] 	= $gallery_id;

            $image = $this->gallery->save_gallery_image($project_data, TRUE);
            // view($image);

            //$html .= $this->load->view('admin/_partials/blog_image_partial', array('image' => $image, 'redirect' => base_url('admin/blog/edit/'.encode($post_id))), TRUE);
            $html = '<li class="col-xs-60 col-sm-30 col-md-20 std-pd" id="sort_'. $image->id .'" >'.$this->load->view('admin/_partials/gallery_item_media', array('image' => $image, 'redirect' => 'admin/projects/edit/'.encode($project->id).'#hero-gallery'), TRUE).'</li>';
            
            respond(json_encode(array('html' => $html)));
            return;
        }
        respond('error: Could not find the item');
	}

	############################################################### CATEGORIES
	

	function new_category()
	{
		$view = 0;
		$post_id = decode($this->uri->segment(4));
		$this->form_validation->set_rules('categories');
		if ($this->form_validation->run()) 
		{
			$created = $this->posts->new_category($_POST['categories'], $post_id);
			if($created)
			{
				$data['categories'] 			= $this->posts->categories();
				$data['assigned_categories'] 	= $this->posts->post_categories($post_id);

				$view = $this->load->view('admin/projects/project_categories_partial', $data, TRUE);
			}
		}
		else
		{
			$new['name'] = 'New Category';
			$new['safe_name'] = url_title($new['name'], '-', TRUE);
			$created = $this->project->create_category($new);
			if(!$created) set_message('error', 'There was an issue creating a new category');

			redirect('admin/projects/categories/'.$created);
		}
		respond($view);
	}

	function remove_category()
	{
		$category_id = decode($this->uri->segment(4));
		$this->project->remove_category($category_id);
		redirect('admin/projects/categories/');
	}

	public function add_new_project_category()
	{
		if(isset($_POST['new_category']) && ($newCategory = trim($_POST['new_category'])) !== '')
		{
			$new_category = $this->project->check_and_add_new_category($_POST['new_category'], @$_POST['parent_id']);
			if(is_object($new_category) && $new_category !== false)
			{
                $output = '<li class="list-none">
							<label class="custom-checkbox txt-responsive-xs" for="category-'.$new_category->id.'" >
								<input class="" id="category-'.$new_category->id.'" type="checkbox" '.set_checkbox('category[]', $new_category->id, FALSE).' value="'.$new_category->id.'" name="category[]">
								<span class="circle"><i></i></span>
								<span class="text">'.$new_category->name.'</span>
							</label>
						</li> ';
				respond($output);                		   
			}
			else
				respond('0');
		}
		else
		{
			if(is_ajax())
				respond('0');
		}
	}


	public function categories()
	{
		$this->form_validation->set_rules('category', 'Category', 'is_array');
		if ($this->form_validation->run()) 
		{
			$this->project->update_categories($_POST);
			set_message("success", "Your categories have been updated");
			redirect('admin/projects/categories');
		}
		$data['categories'] = $this->project->get_categories();
		//view($data['categories']); //array of objects
		$data['all_categories'] = $this->project->get_categories();
		$this->layout->view('admin/projects/project_categories', $data);
	}

	############################################################### CATEGORIES END
	

	#####################################################################################################################################################################################
	#####################################################################################################################################################################################
	#																						validation
	#####################################################################################################################################################################################
	#####################################################################################################################################################################################

	private function _project_edit_validate()
	{
		$this->form_validation->set_rules('display', 'Display', 'trim');
        $this->form_validation->set_rules('gallery_id', 'Gallery', 'trim|is_natural');
		$this->form_validation->set_rules('status', 'Status', 'trim|is_natural');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('detail', 'Page Content', 'trim');
		$this->form_validation->set_rules('sub_heading', 'Sub-heading', 'trim');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'trim');
		$this->form_validation->set_rules('permalink', 'Permalink', 'trim|required');
		$this->form_validation->set_rules('new_image_width', 'New Image Width', 'trim');
		$this->form_validation->set_rules('new_image_height', 'New Image Height', 'trim');
		$this->form_validation->set_rules('new_image_title', 'New Image Title', 'trim');
		$this->form_validation->set_rules('new_image_description', 'New Image Description', 'trim');

		if(isset($_POST['existing_image']))
		{
			foreach($_POST['existing_image'] as $key => $value)
			{
				$valueKeyArray = array_keys($value);
				foreach($valueKeyArray as $k)
					$this->form_validation->set_rules('existing_image['.$key.']['.$k.']', 'Existing Image '.$k, 'trim');
			}
		}
	}
	
}