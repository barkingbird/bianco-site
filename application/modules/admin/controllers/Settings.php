<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('auth_helper');
		access_area('admin/settings');

		$this->load->model('admin/site_setting');
	}

	function index()
	{
		$data['settings'] = $this->site_setting->get_all();
		$this->layout->view('admin/settings/edit', $data);
	}

	function save()
	{
		$this->site_setting->save_batch($_POST);
		set_message('success', 'Your settings has been saved');
		redirect('admin/settings');

	}
}