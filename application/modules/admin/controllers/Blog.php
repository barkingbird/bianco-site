<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('auth_helper');
		access_area('admin/blog');

		$this->load->helper('ckeditor');
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
		$this->ckfinder->SetupCKEditor($this->ckeditor,'/assets/admin/ckfinder/'); 

		$this->load->model('admin/posts');	
	}
    
	function index()
	{
		$data['categories'] 	= $this->posts->categories();
		$data['articles'] 		= $this->posts->get_all($this->input->post());
		
		$this->layout->view('admin/blog/post_manage', $data);
	}
	
	function new_post()
	{
        $data = array();
        $data['categories'] = $this->posts->categories();

		$this->layout->view('admin/blog/new_post', $data);
	}
	
	function save_new()
	{
		$data = array();
		$this->form_validation->set_rules('author_id', 'Author', 'trim|is_natural');
		$this->form_validation->set_rules('display', 'Display', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'trim|is_natural');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('content', 'Content', 'trim|required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'trim');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'trim');
		$this->form_validation->set_rules('permalink', 'Permalink', 'trim|required');
		$this->form_validation->set_rules('video', 'Video URL', 'trim');

		$post_permalink_exists = $this->posts->valid_premalink(FALSE, $this->input->post('permalink'));

		if ($post_permalink_exists)
		{
			set_message('error', 'That Post title already exists');
		}
		elseif ($this->form_validation->run()) {
            
            unset($_POST['dummy_parent']);
            unset($_POST['new_category']);

            $success = $this->posts->new_row($_POST);

			if($success)
			{
				set_message('success', 'Post has been created.');
				redirect(base_url('admin/blog'));
			} 
			else 
			{
				set_message('error', 'There was an error saving, please try again.');
			}
		}
		elseif($error = validation_errors())
		{
			set_message('error', $error);
		}
		return $this->new_post();
	}

	function remove()
	{
		$page_id = $this->uri->segment('4');
		if(!$page_id) {
			set_message('error', 'Could not find that post.');
			redirect(base_url('admin/senses'));
		}
		if($this->posts->remove_row(decode($page_id)))
		{
			set_message('success', 'Post has been removed.');
		}
		else
		{
			set_message('error', 'Could not find that Post.');
		}
		redirect(base_url('admin/blog'));
	}

	function remove_content() 
	{
		$page_id = $this->uri->segment('4');
		if(!$page_id) {
			set_message('error', 'Could not find that Sense.');
			redirect(base_url('admin/senses'));
		}
		if($this->sense->remove_content($page_id))
		{
			set_message('success', 'Sense has been removed.');
		}
		else
		{
			set_message('error', 'Could not find that Sense.');
		}
		redirect(base_url('admin/senses'));
	}

	function new_category()
	{
		$view = 0;
		$post_id = decode($this->uri->segment(4));
		$this->form_validation->set_rules('categories');
		if ($this->form_validation->run()) 
		{
			$created = $this->posts->new_category($_POST['categories'], $post_id);
			if($created)
			{
				$data['categories'] 			= $this->posts->categories();
				$data['assigned_categories'] 	= $this->posts->post_categories($post_id);

				$view = $this->load->view('admin/blog/blog_categories_partial', $data, TRUE);
			}
		}
		respond($view);
	}

	function remove_category()
	{
		$category_id = decode($this->uri->segment(4));
		$this->posts->remove_category($category_id);
		redirect('admin/blog/categories/');
	}

	public function add_new_blog_category()
	{

		if(isset($_POST['new_category']) && ($newCategory = trim($_POST['new_category'])) !== '')
		{
			$new_category = $this->posts->check_and_add_new_category($_POST['new_category'], @$_POST['parent_id']);

			if(is_object($new_category) && $new_category !== false)
			{
                $output = '<li class="list-none">
							<label class="custom-checkbox txt-responsive-xs" for="category-'.$new_category->id.'" >
								<input class="" id="category-'.$new_category->id.'" type="checkbox" '.set_checkbox('category[]', $new_category->id, FALSE).' value="'.$new_category->id.'" name="category[]">
								<span class="circle"><i></i></span>
								<span class="text">'.$new_category->name.'</span>
							</label>
						</li> ';
				respond($output);                		   
			}
			else
				respond('0');
		}
		else
		{
			if(is_ajax())
				respond('0');
		}
	}

	public function categories()
	{
		$this->form_validation->set_rules('category[]', 'Category', 'required');
		if ($this->form_validation->run()) 
		{
			$this->posts->update_categories($_POST);
			set_message("success", "Your categories have been updated");
			redirect('admin/blog/categories');
		}
		$data['categories'] = $this->posts->build_category_list();
		$data['all_categories'] = $this->posts->categories();
		$this->layout->view('admin/blog/blog_categories', $data);
	}

	function edit()
	{
		$page_id = $this->uri->segment('4');
		$data['post'] = $this->posts->get_page(decode($page_id));
	
		if(!$data['post']) {
			set_message('error', 'Selected post could not be found');
			redirect(base_url('admin/blog'));
		}
		$data['categories'] 			= $this->posts->categories();
		$data['assigned_categories'] 	= $this->posts->post_categories(decode($page_id));
		$data['post_images'] 			= $this->posts->images(decode($page_id));
		
		if ($data['post_images']) 
		{
			foreach ($data['post_images'] as $image_details)
			{
				$data['post']->image_details[$image_details->id]['title'] = $image_details->title;
				$data['post']->image_details[$image_details->id]['link'] = $image_details->link;
			}
		}
		$this->layout->view('admin/blog/post_edit', $data);
		
	}

	function update_image_order()
	{
		$this->posts->re_order_images($_POST);
	}

	function update_blog_image_order()
	{
		$this->posts->re_order_blog_images($this->input->post('sort'));
	}

	function remove_image()
	{
		$image_id = decode($this->uri->segment('4'));
		$post_id = decode($this->uri->segment('5'));
		$this->posts->remove_image($image_id);
		redirect(base_url('admin/blog/edit/'.encode($post_id)));
	}

	function save_edit()
	{
		$post_id = decode($this->uri->segment(4));

		$this->form_validation->set_rules('author_id', 'Author', 'trim|is_natural');
		$this->form_validation->set_rules('display', 'Display', 'trim');
		$this->form_validation->set_rules('status', 'Status', 'trim|is_natural');
		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'trim');
		$this->form_validation->set_rules('content', 'Content', 'trim|required');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'trim');
		$this->form_validation->set_rules('permalink', 'Permalink', 'trim|required');
		$this->form_validation->set_rules('video', 'Video URL', 'trim');

		$post_permalink_exists = $this->posts->valid_premalink(FALSE, $this->input->post('permalink'));
		$permalink_belongs_to_current_post = $this->posts->valid_premalink($post_id, $this->input->post('permalink'));

		$_POST['featured'] = (isset($_POST['featured'])) ? 1 : 0;

		if ($this->form_validation->run()) {
			unset($_POST['dummy_parent']);
			unset($_POST['new_category']);
			unset($_POST['new_category_parent']);
			$_POST['safe_name'] = 
			$success = $this->posts->update_row($post_id, $_POST);
			if($success) 
			{

				set_message('success', 'Post has been updated.');
				redirect(base_url('admin/blog/'));
			} 
			else 
			{
				set_message('error', 'There was an error saving the Post, please try again.');
			}
		}
		else 
		{
			set_message('error', validation_errors());
			redirect(base_url('admin/blog/edit/'.encode($post_id)));
		}
		$data['post'] = $this->posts->get_page($post_id);
		$this->layout->view('admin/blog/post_edit', $data);
	}

	public function publish()
	{
		$page_id = $this->uri->segment('4');
		$page_status = (int)$this->uri->segment('5');
		$success = $this->posts->publish($page_id, array('published' => $page_status));
		
		if($success) {
			set_message('success', 'Blog status updated.');
		} else {
			set_message('error', 'Selected Blog Post could not be found.');
		}
		redirect(base_url('admin/blog'));
	}

	function from_feed()
	{
		$feed_url = 'http://blog.urbanangles.com/feed/';
		$this->load->library('RSSParser',array('url' => $feed_url, 'life' => 2));

		$data = $this->rssparser->getFeed(60);

		if($data)
		{
			foreach ($data as $key => $row) 
			{
				$segments = array_filter(explode('/', str_replace('http://blog.urbanangles.com/', '', $row['link'])));
				//view($segments);
				$permalink = 'news/'.$segments[0].'/'.$segments[1].'/'.end($segments).'/';
				//view($row);
				$new_data['title'] 		= $row['title'];
				$new_data['excerpt'] 	= $row['description'];
				$new_data['date_added'] = formate_date('Y-m-d H:i:s', strtotime($row['pubDate']));
				$new_data['permalink'] 	= $permalink;
				//$success = $this->posts->new_row($new_data);
				//view($new_data);
				# code...
			}
		}

		view($data);  
	}

	function upload_blog_image()
	{
		$post_id     	= decode($this->uri->segment('4'));
		$data['post'] 	= $this->posts->get_page($post_id);

        if($post_id && $_FILES["file"]["name"] != '') 
        {

            $path = config_item('blog_file_path').$data['post']->permalink;

            make_folder(ROOT.$path);
            $config['upload_path']      = ROOT.$path;
            $config['check_mime']       = FALSE;
            $config['allowed_types']    = 'gif|jpg|png|GIF|JPG|PNG';

            $this->load->library('upload');
            $this->upload->initialize($config);
            if (! $this->upload->do_multi_upload('file'))
            {
                respond('error: '.$this->upload->display_errors());
                return;
            }

            $file = $this->upload->data();

            $this->load->library('thumb');

            $this->thumb->make_thumbs($file['full_path'], 'hero_sizes');

            $page_data['image'] = $path.$file['file_name'];
            $page_data['post_id'] = $post_id;

            $image = $this->posts->save_gallery_image($page_data, TRUE);

            $html = $this->load->view('admin/_partials/blog_image_partial', array('post_id' => $post_id, 'image' => $image), TRUE);
            
            respond(json_encode(array('html' => $html)));
            return;
        }
        respond('error: Could not find the item');
	}

	function update_category_order()
	{
		$this->posts->re_order_categories($this->input->post('sort'));
	}

}