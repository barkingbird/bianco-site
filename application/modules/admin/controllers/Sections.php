<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sections extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('auth_helper');
		redirect_if_not_logged_in();

		$this->load->helper('ckeditor');
		$this->load->library('ckeditor');
		$this->load->library('ckfinder');
		$this->ckeditor->basePath = base_url().'assets/admin/ckeditor/';
		$this->ckfinder->SetupCKEditor($this->ckeditor,'/assets/admin/ckfinder/'); 

		$this->load->model('admin/section');
        $this->load->library('thumb');
        $this->load->helper('model');

        $this->template_location = APPPATH.'modules/admin/views/sections/';
	}

	public function index()
	{
	}


	public function add_section()
	{
		$page_id 		= decode($this->uri->segment('4'));
		$section_type 	= $this->uri->segment('5');
		
		$success 		= $this->section->create_section($page_id, $section_type);

		if ($success) 
		{
			set_message('success', 'New Section added.');
			redirect('admin/pages/edit/'.encode($page_id).'#content');
		}

		set_message('error', 'There was an error adding your new section.');
		redirect('admin/pages/edit/'.encode($page_id));
	}

	public function remove_section()
	{
		$page_id 		= decode($this->uri->segment('4'));
		$section_id 	= decode($this->uri->segment('5'));
		
		$success 		= $this->section->delete_section($page_id, $section_id);

		if ($success) 
		{
			set_message('success', 'Section removed.');
			redirect('admin/pages/edit/'.encode($page_id).'#content');
		}

		set_message('error', 'There was an error removing your section.');
		redirect('admin/pages/edit/'.encode($page_id));
	}

	public function edit_section() 
	{
		$data 			= array();
		$page_id 		= decode($this->uri->segment('4'));
		$section_id 	= decode($this->uri->segment('5'));

		$section 		= $this->section->get_section($section_id);

		if ($section)
		{
			if (isset($section->content))
			{
				$section->content = json_decode($section->content, TRUE);
			}
			$section->items = $this->section->get_section_items($section_id);

			if ($section->items)
			{
				$items = array();
				foreach ($section->items as $item_key => $item_value)
				{
					$items[$item_value->id] = json_decode($item_value->content, TRUE);
				}
				$section->items = $items;
			}
		}

		if (isset($_POST['ref']))
		{
			$this->update_section($section);
			$this->update_items($section);
			redirect('admin/sections/edit_section/'.encode($page_id).'/'.encode($section_id));
		}

		$data['section'] = $section;

		$view = 'default';
		if (is_file($this->template_location . $section->type . '.php'))
		{
			$view = $section->type;
		}

		$this->layout->view('admin/sections/' . $view, $data);
	}

	public function update_section($section = FALSE) 
	{

		$section_types 		= config_item('section_type');
		$structure 			= (isset($section_types[$section->type])) ? $section_types[$section->type] : FALSE;

		if (isset($structure['main']) && is_array($structure['main']))
		{
			$post_data = array();
			foreach ($structure['main'] as $name => $field_type)
			{
				$this->form_validation->set_rules($name, $name, 'trim');
				if ($this->input->post($name))
				{
					$post_data[$name] = $this->input->post($name);
				}
				elseif ($field_type['field'] == 'image')
				{
					$result = $this->upload_section_image($section, $structure, $name);
					if ($result)
					{
						$post_data[$name] = $result;
					}
					else
					{
						$post_data[$name] = $section->content[$name];
					}
				}
				elseif ($field_type['field'] == 'file')
				{
					$result = $this->upload_section_file($section, $structure, $name);
					if ($result)
					{
						$post_data[$name] = $result;
					}
					else
					{
						$post_data[$name] = $section->content[$name];
					}
				}
				else
				{
					$post_data[$name] = '';
				}
			}

			if ($this->form_validation->run()) 
			{
				$this->section->save_section($section->id, $post_data);
				set_message('success', 'Section updated.');
			}
		}

	}

	public function publish_section()
	{
		$page_id = decode($this->uri->segment('4'));
		$section_id = decode($this->uri->segment('5'));
		$publish_status = (int)$this->uri->segment('6');
		
		$success = $this->section->publish_status($page_id, $section_id, $publish_status);
		
		if ($success) 
		{
			set_message('success', 'Section status updated.');
		} 
		else 
		{
			set_message('error', 'Section could not be found.');
		}

		redirect('admin/pages/edit/'.encode($page_id).'#content');
	}

	public function reorder_section()
	{
		$this->section->order_section($this->input->post('sort'));
	}

	public function update_items($section = FALSE) 
	{
		$section_types 		= config_item('section_type');
		$structure 			= (isset($section_types[$section->type])) ? $section_types[$section->type] : FALSE;

		if (isset($_POST['items']) && is_array($_POST['items']))
		{
			
			foreach ($_POST['items'] as $id => $values)
			{
				$post_data = array();
				foreach ($structure['items'] as $name => $field_type)
				{
					$this->form_validation->set_rules('items['.$id.']['.$name.']', $name, 'trim');
					if (isset($values[$name]) && $values[$name])
					{
						$post_data[$name] = $values[$name];
					}
					elseif ($field_type['field'] == 'image')
					{
						$result = $this->upload_item_image($section, $structure, $id, $name);
						if ($result)
						{
							$post_data[$name] = $result;
						}
						else
						{
							$post_data[$name] = $section->items[$id][$name];
						}
					}
					elseif ($field_type['field'] == 'file')
					{
						$result = $this->upload_item_file($section, $structure, $id, $name);
						if ($result)
						{
							$post_data[$name] = $result;
						}
						else
						{
							$post_data[$name] = $section->items[$id][$name];
						}
					}
					else
					{
						$post_data[$name] = '';
					}
				}

				if ($this->form_validation->run()) 
				{
					$item_id = $id;
					$this->section->save_section_items($section->id, $item_id, $post_data);
					set_message('success', 'Section updated.');
				}
			}

			
		}

	}

	public function add_item()
	{
		$page_id 		= decode($this->uri->segment('4'));
		$section_id 	= decode($this->uri->segment('5'));
		$section_type 	= $this->uri->segment('6');
		
		$success 		= $this->section->create_section_item($section_id, $section_type);

		if ($success) 
		{
			set_message('success', 'New item added.');
		}
		else
		{
			set_message('error', 'There was an error adding your new item.');
		}
		
		redirect('admin/sections/edit_section/'.encode($page_id).'/'.encode($section_id));
	}

	public function remove_item()
	{
		$page_id 		= decode($this->uri->segment('4'));
		$section_id 	= decode($this->uri->segment('5'));
		$item_id	 	= decode($this->uri->segment('6'));
		
		$success 		= $this->section->delete_section_item($section_id, $item_id);

		if ($success) 
		{
			set_message('success', 'Item deleted.');
		}
		else
		{
			set_message('error', 'There was an error deleting your item.');
		}
		
		redirect('admin/sections/edit_section/'.encode($page_id).'/'.encode($section_id));
	}

	public function reorder_item()
	{
		$this->section->order_section_item($this->input->post('sort'));
	}

	function upload_section_image($section = FALSE, $structure = FALSE, $field_name = FALSE)
	{
		if (!$section || !$structure || !$field_name) return FALSE;

		if (isset($_FILES[$field_name]['name']) && $_FILES[$field_name]['name'])
		{
			$this->load->library('upload');
			
			$path = config_item('section_path').encode($section->id).'/';
			if (isset($structure['file_path']) && $structure['file_path'])
			{
				$path = $section['file_path'].encode($section->id).'/';
			}
			
			make_folder(ROOT.$path);
			$config['upload_path']      = ROOT.$path;
			$config['check_mime']       = FALSE;
			$config['allowed_types']    = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG|svg|SVG';
			$this->upload->initialize($config);

			if (!$this->upload->do_upload($field_name))
			{
				set_message('error', 'Sorry there was an issue uploading your file. '.$this->upload->display_errors());
			}
			else
			{
				$image_sizes = 'section_sizes';
				if (isset($structure['image_sizes']) && $structure['image_sizes'])
				{
					$image_sizes = $section['image_sizes'];
				}

				$uploaded_file = $this->upload->data();

				$this->load->library('thumb');
				$this->thumb->make_thumbs($uploaded_file['full_path'], $image_sizes);

				return $path.$uploaded_file['file_name'];
			}
		}	

		return FALSE;
	}

	function upload_item_image($section = FALSE, $structure = FALSE, $item_id = FALSE, $field_name = FALSE)
	{
		if (!$section || !$structure || !$item_id || !$field_name) return FALSE;

		if (isset($_FILES['items']['name'][$item_id][$field_name]) && $_FILES['items']['name'][$item_id][$field_name])
		{
			$temp_FILES = array('item_image' => array());
			$temp_FILES['item_image']['name'] 		= 	$_FILES['items']['name'][$item_id][$field_name];
			$temp_FILES['item_image']['type'] 		= 	$_FILES['items']['type'][$item_id][$field_name];
			$temp_FILES['item_image']['tmp_name'] 	= 	$_FILES['items']['tmp_name'][$item_id][$field_name];
			$temp_FILES['item_image']['error'] 		= 	$_FILES['items']['error'][$item_id][$field_name];
			$temp_FILES['item_image']['size'] 		= 	$_FILES['items']['size'][$item_id][$field_name];
		
			$_FILES['item_image'] = $temp_FILES['item_image'];

			$this->load->library('upload');
			
			$path = config_item('section_path').encode($section->id).'/';
			if (isset($structure['file_path']) && $structure['file_path'])
			{
				$path = $section['file_path'].encode($section->id).'/';
			}
			
			make_folder(ROOT.$path);
			$config['upload_path']      = ROOT.$path;
			$config['check_mime']       = FALSE;
			$config['allowed_types']    = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG|svg|SVG';
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('item_image'))
			{
				set_message('error', 'Sorry there was an issue uploading your file. '.$this->upload->display_errors());
			}
			else
			{
				$image_sizes = 'section_sizes';
				if (isset($structure['image_sizes']) && $structure['image_sizes'])
				{
					$image_sizes = $section['image_sizes'];
				}

				$uploaded_file = $this->upload->data();

				$this->load->library('thumb');
				$this->thumb->make_thumbs($uploaded_file['full_path'], $image_sizes);

				return $path.$uploaded_file['file_name'];
			}
		}

		return FALSE;
	}

	function upload_section_file($section = FALSE, $structure = FALSE, $field_name = FALSE)
	{
		if (!$section || !$structure || !$field_name) return FALSE;

		if (isset($_FILES[$field_name]['name']) && $_FILES[$field_name]['name'])
		{
			$this->load->library('upload');
			
			$path = config_item('section_path').encode($section->id).'/';
			if (isset($structure['file_path']) && $structure['file_path'])
			{
				$path = $section['file_path'].encode($section->id).'/';
			}
			
			make_folder(ROOT.$path);
			$config['upload_path']      = ROOT.$path;
			$config['check_mime']       = FALSE;
			$config['allowed_types']    = 'doc|docx|pdf|zip|mp4';
			$this->upload->initialize($config);

			if (!$this->upload->do_upload($field_name))
			{
				set_message('error', 'Sorry there was an issue uploading your file. '.$this->upload->display_errors());
			}
			else
			{
				$uploaded_file = $this->upload->data();
				return $path.$uploaded_file['file_name'];
			}
		}	

		return FALSE;
	}

	function upload_item_file($section = FALSE, $structure = FALSE, $item_id = FALSE, $field_name = FALSE)
	{
		if (!$section || !$structure || !$item_id || !$field_name) return FALSE;

		if (isset($_FILES['items']['name'][$item_id][$field_name]) && $_FILES['items']['name'][$item_id][$field_name])
		{
			$temp_FILES = array('item_image' => array());
			$temp_FILES['item_image']['name'] 		= 	$_FILES['items']['name'][$item_id][$field_name];
			$temp_FILES['item_image']['type'] 		= 	$_FILES['items']['type'][$item_id][$field_name];
			$temp_FILES['item_image']['tmp_name'] 	= 	$_FILES['items']['tmp_name'][$item_id][$field_name];
			$temp_FILES['item_image']['error'] 		= 	$_FILES['items']['error'][$item_id][$field_name];
			$temp_FILES['item_image']['size'] 		= 	$_FILES['items']['size'][$item_id][$field_name];
		
			$_FILES['item_image'] = $temp_FILES['item_image'];

			$this->load->library('upload');
			
			$path = config_item('section_path').encode($section->id).'/';
			if (isset($structure['file_path']) && $structure['file_path'])
			{
				$path = $section['file_path'].encode($section->id).'/';
			}
			
			make_folder(ROOT.$path);
			$config['upload_path']      = ROOT.$path;
			$config['check_mime']       = FALSE;
			$config['allowed_types']    = 'doc|docx|pdf|zip|mp4';
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('item_image'))
			{
				set_message('error', 'Sorry there was an issue uploading your file. '.$this->upload->display_errors());
			}
			else
			{
				$uploaded_file = $this->upload->data();
				return $path.$uploaded_file['file_name'];
			}
		}

		return FALSE;
	}

	static function field_helper($type = FALSE, $name = FALSE, $value = FALSE, $data = FALSE)
	{
		if (!$type || !$name) return FALSE;

		$html = '';

		if ($type == 'text')
		{
			$html .= '<input type="text" name="'.$name.'" ';
			$html .= 'id="'.@$data['id'].'" ';
			$html .= 'class="'.@$data['class'].'" ';
			$html .= 'style="'.@$data['style'].'" ';
			$html .= 'placeholder="'.@$data['placeholder'].'" ';
			$html .= 'value="'.set_value($name, $value).'" ';
			$html .= '/>';
		}
		elseif ($type == 'ckeditor')
		{
			$html .= '<textarea name="'.$name.'" ';
			$html .= 'id="'.@$data['id'].'" ';
			$html .= 'class="ckeditor '.@$data['class'].'" ';
			$html .= 'style="'.@$data['style'].'" ';
			$html .= 'placeholder="'.@$data['placeholder'].'" ';
			$html .= '>';
			$html .= set_value($name, $value);
			$html .= '</textarea>';
		}
		elseif ($type == 'textarea')
		{
			$html .= '<textarea name="'.$name.'" ';
			$html .= 'id="'.@$data['id'].'" ';
			$html .= 'class="'.@$data['class'].'" ';
			$html .= 'style="'.@$data['style'].'" ';
			$html .= 'placeholder="'.@$data['placeholder'].'" ';
			$html .= '>';
			$html .= set_value($name, $value);
			$html .= '</textarea>';
		}
		elseif ($type == 'image')
		{
			if ($value)
			{
				$html .='<figure class="row mg-b-sm image-center" style="background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(\''.display_image($value).'\')"></figure>';
			}
			else
			{
				$html .='<figure class="row mg-b-sm bg-grey image-center"></figure>';
			}
			
			$html .= '<input type="file" name="'.$name.'" ';
			$html .= 'id="'.@$data['id'].'" ';
			$html .= 'class="txt-responsive-xs '.@$data['class'].'" ';
			$html .= 'style="'.@$data['style'].'" ';
			$html .= 'placeholder="'.@$data['placeholder'].'" ';
			$html .= '/>';
		}
		elseif ($type == 'file')
		{
			if ($value)
			{
				$html .='<div class="row mg-b-sm"><a href="'.base_url($value).'" target="_blank">View File</a></div>';
			}
			
			$html .= '<input type="file" name="'.$name.'" ';
			$html .= 'id="'.@$data['id'].'" ';
			$html .= 'class="txt-responsive-xs '.@$data['class'].'" ';
			$html .= 'style="'.@$data['style'].'" ';
			$html .= 'placeholder="'.@$data['placeholder'].'" ';
			$html .= '/>';
		}
		if ($type == 'select')
		{
			$html .= '<select name="'.$name.'" ';
			$html .= 'id="'.@$data['id'].'" ';
			$html .= 'class="'.@$data['class'].'" ';
			$html .= 'style="'.@$data['style'].'" ';
			$html .= '>';
			// view(array($name, $data));
			if (isset($data['options']) && is_array($data['options']))
			{
				foreach ($data['options'] as $option_value => $option_name)
				{
					$html .= '<option '.set_select($name, $option_value, (bool) ($option_value == $value)).' value="'.$option_value.'">'.ucfirst($option_name).'</option>';
				}
			}

			$html .= '</select>';
		}

		return $html;

	}


}