<div class="row action-buttons">

	<div class="mg-t-md mg-b-md pull-right">

		<a href="<?php echo base_url('admin/leads/create_form'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-plus">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				New Form
			</span>
		</a>
	</div>

</div>

<section>

	<div class="row">
		<div class="col-xs-60 col-sm-60">
		 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
		 		Forms
		 	</div>
	 	</div>
	</div>

	<?php if ($forms) : ?>
	<div class="bg-white row pd-all-md">

		<div class="manage-list-labels text-center stop-prop pull-right mg-r-xxxs">
			<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">edit</div>
			<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">publish</div>
			<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">delete</div>
		</div>

		<ul class="manage-list" data-rel="#" data-parent="0" data-url="#">

			<?php foreach ($forms as $form) : ?>
			<li class="item-parent list-none" id="sort_<?php echo $form->id; ?>">
				<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
					<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs">
						<?php echo $form->name; ?>
					</div>
					<div class="icon-container text-right stop-prop pull-right">
					<a href="<?php echo base_url('admin/leads/edit_form/'.$form->id); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
						<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
							<?php echo config_item('icon_image'); ?>
						</span>
					</a>

					<?php if ($form->published == 1): ?>
					<a href="<?php echo base_url('admin/leads/publish_form/'.$form->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
						<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
							<?php echo config_item('icon_image'); ?>
						</span>
					</a>
					<?php else: ?>
					<a href="<?php echo base_url('admin/leads/publish_form/'.$form->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
						<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
							<?php echo config_item('icon_image'); ?>
						</span>
					</a>
					<?php endif ?>

					<a href="<?php echo base_url('admin/leads/delete_form/'.$form->id); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete <?php echo $form->name; ?>, do you wish to continue?">
						<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
							<?php echo config_item('icon_image'); ?>
						</span>
					</a>
					</div>
				</div>
			</li>
			<?php endforeach; ?>

		</ul>

	</div>
	<?php else: ?>
	<p>There are no forms.</p>
	<?php endif; ?>	

		<div class="row">
		<div class="col-xs-60 col-sm-60 mg-t-md">
		 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
		 		Leads
		 	</div>
	 	</div>
	</div>		
	<div class="bg-white row pd-all-md">
		<div class="manage-list" data-rel="#" data-parent="0" data-url="#">
			<?php
			if (isset($forms) && $forms) :
				foreach ($forms as $form) : 
			?>
			<div class="item-parent list-none" id="sort_<?php echo $form->id; ?>">
				<div class="pd-all-xxs">
					<div class="txt-responsive-lg stop-prop col-xs-60 mg-t-xxs">
						<?php echo $form->name; ?>
					</div>
				</div>
				<?php if ($leads && is_array($leads[$form->id])) : ?>
				<table class="lead-table table-striped js-datatables">
					<thead>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Date</th>
							<th>Origin</th>
							<th class="text-center">More</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($leads[$form->id] as $lead) : 
						$content = json_decode($lead->content);?>
						<tr>
							<td><a href="<?php echo base_url('admin/leads/detail/'.encode($lead->id));?>"  class="lead-table__link"><?php echo (isset($content->first_name))? $content->first_name : '';?></a></td>
							<td><a href="<?php echo base_url('admin/leads/detail/'.encode($lead->id));?>"  class="lead-table__link"><?php echo (isset($content->last_name))? $content->last_name : '';?></a></td>
							<td><?php echo $lead->email;?></td>
							<td><?php echo (isset($content->phone))? $content->phone : ''; ?></td>
							<td><?php echo date("Y-m-d", strtotime($lead->date_created));?></td>
							<td><?php echo (isset($content->lead_origin))? $content->lead_origin : ''; ?></td>
							<td class="text-center"><a href="<?php echo base_url('admin/leads/detail/'.encode($lead->id));?>" class="lead-table__link"><i class="glyphicon glyphicon-eye-open"></i></a>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
				<?php else : ?>
				<p>There are no leads.</p>
				<?php endif; ?>
			</div>
			<?php endforeach; ?>
			<?php else : ?>
				<p>There are no leads.</p>
			<?php endif; ?>
		</div>
	</div>
</section>