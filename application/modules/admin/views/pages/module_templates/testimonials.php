<!-- This part is for editing -->
<?php if ($page_mode === 'edit'): //view($article); ?>

	<?php echo form_open_multipart(); ?>
	
		<div class="row action-buttons">
			<div class="mg-t-md mg-b-md pull-right">
				<a href="<?php echo base_url('/admin/pages/edit/'.$page_id.'#content'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-crosscircle">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Cancel
					</span>
				</a> 
				<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-disk">
					  <?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
					  Save
					</span>
				</button>
			</div>
		</div>

		<section>
			
			<?php echo Input_helper::heading('Edit Article: '. @$article->title); ?>
			
			<div class="col-xs-60 bg-white pd-all-md">

				<div class="row">
					<div class="col-xs-60">
						<label for="prop-title">Title *</label>
					</div>
					<div class="col-xs-60">
						<input type="text" class="form-control" name="title" id="prop-title" value="<?php echo set_value('title', @$article->title) ?>" />
					</div>
					<div class="col-xs-60 mg-t-md">
						<label for = "prop_content">Description</label>
					</div>
					<div class = "col-xs-60">
						<?php echo $this->ckeditor->editor("description", set_value('description', @$article->description)); ?>
					</div>
				</div>

				<?php echo Input_helper::heading('Testimonials: '); ?>
				<a href="<?php echo base_url('admin/pages/testimonials/add_article/'.encode($article->id).'?redirect='.encode(full_url())); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-plus">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Add Testimonial
					</span>
				</a>
				<?php foreach ($article->values as $key => $content): ?>
					<div class="col-xs-20 mg-t-md mg-r-md">
						<div class="row mg-b-sm">
							<div class="pull-right">
							  <a href="<?php echo base_url('admin/pages/testimonials/remove_article/'.encode($article->id).'/'.encode($key).'?redirect='.encode(full_url())); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete this Image, is this correct?" >
								<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
								  <?php echo config_item('icon_image'); ?>
								</span>
							  </a>
							</div>
						</div>
						<div class="col-xs-60 mg-t-md">

							<div class="col-xs-60">
								<label for="prop-values[<?php echo $key; ?>][title">Title</label>
							</div>
							<div class="col-xs-60">
								<input type="text" class="form-control" name="values[<?php echo $key; ?>][title]" id="prop-values[<?php echo $key; ?>][title" value="<?php echo set_value('values['.$key.'][title]', @$content->title) ?>" />
							</div>

							<div class="col-xs-60">
								<label for="prop-values[<?php echo $key; ?>][author">Author</label>
							</div>
							<div class="col-xs-60">
								<input type="text" class="form-control" name="values[<?php echo $key; ?>][author]" id="prop-values[<?php echo $key; ?>][author" value="<?php echo set_value('values['.$key.'][author]', @$content->author) ?>" />
							</div>

							<div class="col-xs-60">
								<div class="col-xs-60">Quote</div>
								<div class="col-xs-60">
									<textarea class="form-control" name="values[<?php echo $key; ?>][quote]"><?php echo set_value('values['.$key.'][quote]', @$content->quote); ?></textarea>									
								</div>
							</div>
						</div>
					</div>
				<?php endforeach ?>
				
			</div>

		</section>
	<?php echo form_close(); ?>

<!-- This part is for viewing -->
<?php else : ?>		
	<div class="bg-lighter-grey padding-bottom-large">
		<div class="col-xs-60">
			<span><?php echo (!@$article->title ? 'Title' : $article->title); ?></span>
		</div>
		<div class="col-xs-60">
			<?php echo (!@$article->description ? 'Content' : $article->description); ?>
		</div>
	</div>
<?php endif; ?>	