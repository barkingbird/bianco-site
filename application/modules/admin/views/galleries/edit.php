<?php 
	$fromModule = false;
	if(isset($info))
	{
		$fromModule = true;
		$moduleId = trim($info['moduleId']);
		$moduleTypeName = trim($info['moduleTypeName']);
	}

	$fromService = false;	
	if(isset($service) && is_object($service))
	{
		$fromService = true;
		$serviceId = $service->id;
	}

	$customRedirect = false;	
	if(isset($custom_redirect_url) && trim($custom_redirect_url) !== '')
	{
		$customRedirect = true;
		$redirectURL = $custom_redirect_url;
	}


?>

<?php echo form_open_multipart('admin/galleries/save_edit/'.encode($gallery->id), array('class' => 'bottom-100')); ?>

<?php if($fromModule === true): ?>
	<input type = "hidden" name = "fromModuleId" value = "<?php echo $moduleId; ?>" />
	<input type = "hidden" name = "fromModuleTypeName" value = "<?php echo $moduleTypeName; ?>" />
<?php endif; ?>	

<?php if($fromService === true): ?>
	<input type = "hidden" name = "fromServiceId" value = "<?php echo $serviceId; ?>" />
<?php endif; ?>	

<?php if($customRedirect === true): ?>
	<input type = "hidden" name = "custom_redirection" id = "custom_redirection" value = "<?php echo $redirectURL; ?>" />
<?php endif; ?>


<div class="row mg-t-sm">
    <div class="col-xs-60 col-sm-60 col-md-30 pull-right">
       <div class="bg-light-blue pd-t-xs pd-b-xxs txt-center txt-responsive-xs txt-white widget-button-one parent-buttons col-xs-60 col-sm-60 col-md-29">
          <a href="#" class="txt-white">
            <span class="icon icon-size-150 outline-light icon-preview">
              <?php echo config_item('icon_image'); ?>
             </span>
             Preview
           </a>
        </div>  
        <div class="hidden-xs hidden-sm bg-seagreen pd-t-xs pd-b-xxs txt-responsive-xs mg-l-xs txt-white txt-center widget-button-one parent-buttons col-xs-60 col-sm-60 col-md-29">
             <a href="#" class="txt-white">
               <span class="icon icon-size-150 outline-light icon-disk">
                 <?php echo config_item('icon_image'); ?>
               </span>
               Save
             </a>    
        </div>
        <div class="visible-xs visible-sm bg-seagreen pd-t-xs pd-b-xxs txt-responsive-xs txt-white txt-center widget-button-one parent-buttons col-xs-60 col-sm-60 col-md-29">
           <a href="#" class="txt-white">
             <span class="icon icon-size-150 outline-light icon-disk">
               <?php echo config_item('icon_image'); ?>
             </span>
             Save
           </a>    
         </div>
    </div>       
</div>
            
<div class="row mg-t-sm pd-all-xs bg-dark-grey">   
   <div class="pd-l-xxs col-xs-40 col-md-30">
      <div class="col-xs-60">
         <div class="row">
           <div class="col-xs-60">
              <h4 class="txt-white">Edit Gallery: <?php echo $gallery->title; ?></h4>   
              <!-- <input type = "hidden" name = "edit_house" id = "edit_house" value = "1" />   -->       
            </div>
         </div>
      </div>
   </div>  
</div>  

<div class="row pd-all-md bg-white">

<!-- <div class="col-xs-60 page-heading">
	<div class="row">
		
		<div class="col-xs-60">
		
			<h4 class="margin-top-small">Edit Gallery: <?php echo $gallery->title; ?></h4>
			
			<button class="btn bg-green pull-right margin-left-medium">Save</button>
			<button class="btn bg-blue pull-right margin-left-medium">Update</button>
			<button class="btn bg-yellow pull-right margin-left-medium">Cancel</button>
			
		</div>
		
	</div>
</div> -->


<div class="col-xs-60">

	<!-- <div class="row margin-bottom-large">
		<div class="col-xs-5">
			<label for="prop-status">Active</label>
		</div>
		
		<div class="col-xs-10">
			<div class="switch margin-top-small"><input id="prop-status" type="checkbox" name="status" <?php echo set_radio('status', '1', (bool)($gallery->status)); ?> value="1" data-toggle="switch" /></div>
		</div>
        <div class="col-xs-5">
			<label for="prop-arrows">Arrows</label>
		</div>
		
		<div class="col-xs-10">
			<div class="switch margin-top-small"><input type="checkbox" id="prop-arrows" name="arrows" <?php echo set_radio('arrows', '1', (bool)($gallery->arrows)); ?> value="1" data-toggle="switch" /></div>
		</div>
		
		<div class="col-xs-5">
			<label>Hover</label>
		</div>
		<div class="col-xs-10">
			<div class="switch margin-top-small"><input type="checkbox" name="hover" <?php echo set_radio('hover', '1', (bool)($gallery->hover)); ?> value="1" data-toggle="switch" /></div>
		</div>

		<div class="col-xs-5">
			<label for="thumbs">Thumbs</label>
		</div>
		<div class="col-xs-10">
			<div class="switch margin-top-small"><input id="thumbs" type="checkbox" name="thumbs" <?php echo set_radio('thumbs', '1', (bool)($gallery->thumbs)); ?> value="1" data-toggle="switch" /></div>
		</div>

		<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3"></div>
        
        <!-- <div class="col-xs-5">
			<label>Hover Colour</label>
		</div>
        <div class="col-xs-10">
            <input type="text" name="bg_colour" class="colour" value="<?php echo set_value('bg_colour' , $gallery->bg_colour); ?>"/>
		</div> ->
	</div> -->
    
	<div class="row">
		<div class="col-xs-60">
		    <label for="title">Title</label>
		</div>
		<div class="col-xs-60">
			<input class="mg-t-xxs form-control" type="text" name="title" id="title" value="<?php echo set_value('title' , $gallery->title); ?>"/>
		</div>
	</div>

	<!-- <div class="row margin-bottom-large">
		<div class="col-xs-5">
		    <label for="title">Title</label>
		</div>
		<div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
			<input class="form-control" type="text" name="title" id="title" value="<?php echo set_value('title' , $gallery->title); ?>"/>
		</div>
		<div class="col-xs-25"></div>
	</div>

    <div class="row margin-bottom-large">
    	<label for="content" class="col-xs-5">Content</label>
    	<div class="full-ck col-xs-55"><?php echo $this->ckeditor->editor("content", set_value('content', $gallery->content)); ?></div>
	</div> -->
	
</div><!-- END 12 COL -->

<div class="row mg-t-sm col-xs-60">
    <div class="col-xs-60" >Images</div>
    <div class="col-xs-row std-pd">
        <?php if (isset($images) && $images !== false): ?>
            <ul id="gallery-images" class="list-none manage-list col-xs-60" data-rel="gallery_images" data-url="<?php echo base_url('admin/ajax/update_page_order') ?>">
                <?php foreach ($images as $image): ?>
                <li id="sort_<?php echo $image->id; ?>" class="mg-t-sm col-xs-60 col-sm-60 col-md-20 std-pd">
                	<div class="bg-light-grey pd-all-xs">
	                	<i class="handle glyphicon glyphicon-sort"></i>
	                	<div>
	                    	<a href="<?php echo base_url($image->image); ?>" class="pop-up"><img class="img-responsive" alt="Gallery image" src="<?php echo base_url($image->thumb); ?>" /></a>
	                    
	                        <div class="mg-t-xs image-title">
	                            <label for="title-<?php echo $image->id; ?>">Title</label>
	                            <input type="text" class="form-control" id="title-<?php echo $image->id; ?>" name="images[<?php echo $image->id; ?>][title]" value="<?php echo set_value('images['. $image->id.'][title]', $image->title); ?>" />
	                        </div>
	                        <div class="mg-t-xs image-description">
	                            <label for="description-<?php echo $image->id; ?>">Description</label>
	                            <input type="text" class="form-control" id="description-<?php echo $image->id; ?>" name="images[<?php echo $image->id; ?>][description]" value="<?php echo set_value('images['. $image->id.'][description]', $image->description); ?>" />
	                        </div>
	                        <div class="mg-t-xs image-video_url">
	                            <label for="video_url-<?php echo $image->id; ?>">Video URL</label>
	                            <input type="text" class="form-control" id="video_url-<?php echo $image->id; ?>" name="images[<?php echo $image->id; ?>][video_url]" value="<?php echo set_value('images['. $image->id.'][video_url]', $image->video_url); ?>" />
	                        </div>
	                        <div class="mg-t-xs image-link">
	                            <label for="link-<?php echo $image->id; ?>">Link</label>
	                            <input type="text" class="form-control" id="link-<?php echo $image->id; ?>" name="images[<?php echo $image->id; ?>][link]" value="<?php echo set_value('images['. $image->id.'][link]', $image->link); ?>" />
	                        </div>

	                        <a class="mg-t-xxs remove-parent slide-out confirm btn btn-danger btn-xs" data-rel='#sort_<?php echo $image->id; ?>' title="You are about to delete this Image, is this correct?" href="<?php echo base_url('admin/galleries/remove_image/'.encode($image->id)) ?>">
	                        	<span class="icon icon-size-75 outline-light icon-cross">
              						<?php echo config_item('icon_image'); ?>
             					</span>
	                        </a>
	                    </div>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
	</div>
</div>
	
</form>

<div class="col-xs-60">
    <div class="row">
        <div class="mg-t-sm col-xs-60">
        <?php echo form_open_multipart('admin/galleries/upload_image/'.$gallery->id.(($fromModule)?'/modules/'.encode($moduleId) : ''), array('id' => 'galery-uploader', 'class' => 'dropzone multi-uploader', 'data-title' => 'Upload Images', 'data-name' => 'file')); ?>
            <div class="fallback">
                <input name="facades" type="file" multiple />
            </div>
            <div class="pd-t-md pd-l-sm pd-r-sm dz-default dz-message">
                Drag files here to upload (or click) to upload a new Image
                
            </div>
        </form>
        </div>
    </div>
</div>
