<!-- 

<div class="col-xs-60 margin-bottom-large page-heading">
	<div class="row">
		
		<div class="col-xs-50">
			<h4 class="margin-top-small">Users</h4>			
		</div>
		<div class="col-xs-5">
			<a class="btn btn-success" href="<?php echo base_url('admin/users/new_user'); ?>">New User <i class="glyphicon glyphicon-plus"></i></a>
		</div>
		
	</div>
</div> -->

<?php /* if ($users): ?>
	<ul class="manage-list listings" data-url="<?php echo base_url('admin/ajax/update_user_order') ?>">
	<?php foreach ($users as $user): ?>
		<li id="sort_<?php echo $user->id; ?>">
			<i class="handle glyphicon glyphicon-sort"></i>
			<?php echo $user->username; ?> <span style="font-size: 0.75em; padding-left: 10px;"> <?php echo $user->group_name; ?></span>
			<div class="btn-group btn-group-sm">
				<a title="You are about to delete the user: <?php echo $user->name; ?>, do you wish to continue?" href="<?php echo base_url('admin/users/remove/'.encode($user->id)); ?>" class="confirm btn btn-danger">Delete <i class="glyphicon glyphicon-remove"></i></a>
				<a href="<?php echo base_url('admin/users/edit/'.encode($user->id)); ?>"  class="btn btn-success">Edit <i class="glyphicon glyphicon-pencil"></i></a>
			</div>
		</li>
	<?php endforeach ?>
	</ul>
<?php else: ?>
	<h4>No Users found</h4>
	<p>There are no Users to manage, Please <a href="<?php echo base_url('admin/users/new_user') ?>">create a new User</a> first
<?php endif */ ?>














<div>

	<div class="row action-buttons">

		<div class="mg-t-md mg-b-md pull-right">	

			<!-- <a href="#" class="txt-grey start-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm ">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Order
				</span>
			</a>

			<a href="#" class="txt-grey stop-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Stop Ordering
				</span>
			</a> -->

			<a href="<?php echo base_url('admin/users/new_user'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-plus">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					New User
				</span>
			</a>

		</div>
	</div>

	<section>
		<!-- <div class="row">
			<div class="col-xs-60 col-sm-60">
			 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
			 		Users
			 	</div>
		 	</div>
		</div> -->

		<?php if ($users) : ?>
			<div class="bg-white row pd-all-md">

				<div class="manage-list-labels text-center stop-prop pull-right mg-r-xxxs">
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">edit</div>
					<!-- <div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">publish</div> -->
					<div class="pull-left upper mg-r-xxxs txt-responsive-xxxs">delete</div>
				</div>

		 		<ul class="manage-list" data-url="<?php echo base_url('admin/ajax/update_user_order') ?>">
		 		
	 				<?php foreach ($users as $user) : ?>
	 					<li class="item-parent list-none" id="sort_<?php echo $user->id; ?>">
	 						<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
		 						<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs pd-l-xxs">
									<!-- <span class="icon icon-size-75 outline-dark mg-l-xs mg-r-xxs">
									</span> -->
			 						<?php echo $user->username; ?>
			 						<span class="txt-responsive-xs pd-l-sm"><?php echo $user->group_name; ?></span>

		 						</div>
		 						<div class="icon-container text-right stop-prop pull-right">
									<a href="<?php echo base_url('admin/users/edit/'.encode($user->id)); ?>" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-edit stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>

									<?php /*if ($user->status == 1): ?>
									<a href="<?php echo base_url('admin/users/publish/'.$user->id.'/0'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-tick stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php else: ?>
									<a href="<?php echo base_url('admin/users/publish/'.$user->id.'/1'); ?>" class="bg-light-blue pd-all-xxs pd-l-xs pd-r-xs pull-left block mg-r-xxxs">
										<span class="icon icon-size-125 outline-light icon-minus stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
									<?php endif;*/ ?>
									<?php if ($user->id == 1): ?>
                                    <span class="pd-all-xxs pd-l-xs pd-r-xs pull-left block">
                                        <span class="icon icon-size-125">
                                        </span>
                                    </span>
                                    <?php else: ?>
									<a href="<?php echo base_url('admin/users/remove/'.encode($user->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm" title="You are about to delete <?php echo $user->username; ?>, do you wish to continue?">
										<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
									</a>
								<?php endif; ?>
		 						</div>
	 						</div>

	 					</li>
	 				<?php endforeach; ?>	
		 		</ul>
		 </div>
		<?php else: ?>
			<p>There are no Users.</p>
		<?php endif; ?>
	</section>

</div>
