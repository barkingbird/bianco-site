
<div class="login-form col-xs-60 col-sm-60 col-md-50 col-lg-40 pd-all-xl pd-b-md mg-t-xl bg-white center-block">

	<form action="" method="post" accept-charset="utf-8" class="row">

		<div class="txt-center col-xs-60">
			<img class="login-icon" src="<?php echo base_url('assets/admin/images/icon-login-large.png'); ?>" />
		</div>

		<div class="txt-grey txt-center txt-responsive-xxl mg-b-md mg-t-md col-xs-60">
			Sign in to the CMS
		</div>

		<div class="clearfix"></div>

		<div class="col-xs-60 col-sm-44 center-block">

			<div class="mg-b-md row">
				<label for="user" class="col-xs-60 col-sm-20 txt-grey">User Name</label>
				<div class="col-xs-60 col-sm-40">
				<input type="text" class="txt-grey text-center txt-responsive-sm username" value="<?php echo set_value('user'); ?>" name="user" id="user">
				</div>
			</div>

			<div class="mg-b-md row">
				<label for="password" class="col-xs-60 col-sm-20 txt-grey">Password </label>
				<div class="col-xs-60 col-sm-40">
					<input type="password" class="txt-grey text-center txt-responsive-sm password" value="" name="password" id="password">					
				</div>
			</div>

			<div class="mg-b-sm row">
				<div class="col-xs-60 col-sm-40 pull-right">
					<button class="border-none pd-all-xxs col-xs-60 button bg-dark-grey txt-white text-center txt-responsive-sm" type="submit">SIGN IN</button>
				</div>
			</div>	

		<div class="txt-responsive-xxs txt-center col-xs-60 col-sm-40 pull-right">
			<a class="login-link txt-left block txt-responsive-xxs" href="<?php echo base_url('admin/forgot_password'); ?>"> Forgot your password?</a>
		</div>

		<div class="txt-responsive-xxs txt-center col-xs-60 col-sm-60 pull-right mg-t-xs pd-t-md "> Powered By  &nbsp; &nbsp; <img src="<?php echo base_url('assets/admin/images/bb-logo.png'); ?>" width="100"/></div>


		</div>

		<div class="clearfix"></div>
	
	</form>

</div>






