<div class="login-form col-xs-60 col-sm-60 col-md-50 col-lg-40 pd-all-xl mg-t-xl bg-white center-block">

	<div class="reset_password-form row">

		<?php echo form_open(); ?>

			<input type = "hidden" name = "submit_password_reset" id = "submit_password_reset" value = "1" />

			<div class="col-xs-60 txt-center">
				<span class="icon icon-size-400 icon-users fill-dark txt-responsive-xxl">
					<?php echo config_item('icon_image'); ?>
				</span>
			</div>

			<div class="clearfix"></div>

			<div class="col-xs-60 col-sm-35 center-block text-center">
				
				<h1 class="txt-grey txt-center txt-responsive-xxl mg-b-md mg-t-sm">
					Forgotten your password?
				</h1>

				<p>Please enter your email address below and we will send you a link to reset your password.</p>

				<input type="text" class="mg-t-md form-control login-field" value="<?php echo set_value('email_address'); ?>" placeholder="Email Address" name="email_address" id="email_address" />

				<button class="center-block mg-t-md border-none pd-all-xxs col-xs-60 col-sm-30 button bg-dark-grey txt-white text-center txt-responsive-sm" type="submit">Submit</button>

			</div>	
	    
		</form>
	    
	</div>

</div>