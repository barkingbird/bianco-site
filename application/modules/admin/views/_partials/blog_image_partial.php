<?php $file_segments = explode("/", $image->image); ?>

<li id="<?php echo 'sort_'.$image->id; ?>" class="col-xs-60 col-sm-30 col-md-20 pd-r-xs">

	<div class="item-child row bg-middle-grey mg-t-sm pd-all-sm bordered">

		<div class="row mg-b-sm">
			<div class="pull-right">

			  <a class="ordering-hide bg-red pd-all-xxs pd-l-xs pd-r-xs pull-left block confirm slide-out" data-rel='#sort_<?php echo $image->id; ?>' title="You are about to delete this Image, is this correct?" href="<?php echo base_url('admin/blog/remove_image/'.encode($image->id).'/'.encode($post_id)); ?>">
				<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
				  <?php echo config_item('icon_image'); ?>
				</span>
			  </a>
			</div>
			<div class="col-xs-60 col-sm-40 pull-left txt-responsive-sm txt-bold">
				<?php echo character_limiter($image->title, 24); ?>
			</div>
		</div>

		<div class="col-xs-60">
			<div class="img-placeholder image-center">
			<?php if($image->image) : ?>
				<img src="<?php echo display_image($image->image,'thumb_'); ?>" class="img-responsive" title="<?php echo $image->title; ?>">    
			<?php endif; ?>
			</div>

			<div class="row mg-t-sm txt-responsive-xs txt-light inherit">
				<div class="col-xs-60 col-sm-20 txt-book">
					Image
				</div>
				<div class="col-xs-60 col-sm-40">
					<a href="<?php echo base_url($image->image); ?>" class="gallery-popup" rel="gallery-<?php echo $image->id; ?>">
					<?php echo ($image->image) ? substr(end($file_segments), 0, 20) : 'none'; ?>
					</a>
				</div>
			</div>

			<div class="ordering-hide row txt-responsive-xs inherit mg-t-xs">
				<div class="col-xs-60 col-sm-20 inherit">
					<label for="prop-title_<?php echo $image->id; ?>">Title</label>
				</div>
				<div class="col-xs-60 col-sm-40 inherit">
					<input type="text" name="image_details[<?php echo $image->id; ?>][title]" value="<?php echo set_value('image_details['.$image->id.'][title]', $image->title); ?>" id="prop-title_<?php echo $image->id; ?>" />
				</div>
			</div>

			<div class="ordering-hide row txt-responsive-xs inherit mg-t-xs">
				<div class="col-xs-60 col-sm-20 inherit">
					<label for="prop-link_<?php echo $image->id; ?>">Link</label>
				</div>
				<div class="col-xs-60 col-sm-40 inherit">
					<input type="text" name="image_details[<?php echo $image->id; ?>][link]" value="<?php echo set_value('image_details['.$image->id.'][link]', $image->link); ?>" id="prop-link_<?php echo $image->id; ?>" />
				</div>
			</div>


		</div>

		<div class="clearfix"></div>

	</div>

</li>