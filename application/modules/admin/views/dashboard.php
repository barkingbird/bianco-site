

<?php /* echo form_open(base_url('admin/update-dashboard'));  ?>
<div>

		<div class="row">
			<div class="mg-t-md mg-b-md pull-right">		
				
					<button  type="submit" class="txt-white btn bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
		                <span class="icon icon-size-150 outline-light icon-disk">
		                  <?php echo config_item('icon_image'); ?>
		                </span>
		                <span class="text txt-white">
		                  Save
		                </span>
		            </button>
			</div>
		</div>	

		<div class="row col-sm-60">

		 <section>		

	 		<div class="row">
				<div class="col-xs-60 col-sm-60">
				 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">
				 		<span class="icon icon-size-175 outline-light icon-note">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
				 		My Notes
				 		</span>
				 	</div>
			 	</div>
			</div>
			<div class="row">
		 		<div class="bg-white pd-all-xs col-sm-60">
		 		  <?php echo $this->ckeditor->editor("notes", $user->notes); ?>
		 		</div>
	 		</div>

	 		<div class="row">
				<div class="col-xs-60 col-sm-60">
				 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">
				 		<span class="icon icon-size-175 outline-light icon-staff">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
				 		Website Analytics
				 		</span>
				 	</div>
			 	</div>
			</div>
			<div class="row">
		 		<div class="bg-white pd-all-xs col-sm-60">
		 		  <?php 

		 		  	echo $this->gcharts->LineChart('Analytics')->outputInto('analytics_div');
					echo $this->gcharts->div(800, 300);
		 		   ?>
		 		</div>
	 		</div>
			 	
		</section>

		</div>
</div>	 
</form>
*/ ?>

	<div class="row">
		<div class="col-xs-60 col-sm-60 mg-t-md">
		 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
		 		Leads
		 	</div>
	 	</div>
	</div>		
	<div class="bg-white row pd-all-md">
		<div class="manage-list" data-rel="#" data-parent="0" data-url="#">
			<?php
			if (isset($forms) && $forms) :
				foreach ($forms as $form) : 
			?>
			<div class="item-parent list-none" id="sort_<?php echo $form->id; ?>">
				<div class="pd-all-xxs">
					<div class="txt-responsive-lg stop-prop col-xs-60 mg-t-xxs">
						<?php echo $form->name; ?>
					</div>
				</div>
				<?php if ($leads && is_array($leads[$form->id])) : ?>
					<?php if ($leads[$form->id] && count($leads[$form->id]) > 0):?>
					<table class="lead-table table-striped js-datatables">
						<thead>
							<tr>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Date</th>
								<th>Origin</th>
								<th class="text-center">More</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($leads[$form->id] as $lead) : 
							$content = json_decode($lead->content);?>
							<tr>
								<td><a href="<?php echo base_url('admin/leads/detail/'.encode($lead->id));?>"  class="lead-table__link"><?php echo (isset($content->first_name))? $content->first_name : '';?></a></td>
								<td><a href="<?php echo base_url('admin/leads/detail/'.encode($lead->id));?>"  class="lead-table__link"><?php echo (isset($content->last_name))? $content->last_name : '';?></a></td>
								<td><?php echo $lead->email;?></td>
								<td><?php echo (isset($content->phone))? $content->phone : ''; ?></td>
								<td><?php echo date("Y-m-d", strtotime($lead->date_created));?></td>
								<td><?php echo (isset($content->lead_origin))? $content->lead_origin : ''; ?></td>
								<td class="text-center"><a href="<?php echo base_url('admin/leads/detail/'.encode($lead->id));?>" class="lead-table__link"><i class="glyphicon glyphicon-eye-open"></i></a>
								</td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
				<?php else: ?>
				<p>There are no leads in <?php echo $form->name; ?>.</p>
				<?php endif; ?>
				<?php else : ?>
				<p>There are no leads.</p>
				<?php endif; ?>
			</div>
			<?php endforeach; ?>
			<?php else : ?>
				<p>There are no leads.</p>
			<?php endif; ?>
		</div>
	</div>
