<?php echo form_open(); ?>

<div class="row action-buttons">

		<div class="mg-t-md mg-b-md pull-right">

			<a href="#" class="txt-grey start-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm ">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Order
				</span>
			</a>

			<a href="#" class="txt-grey stop-ordering bg-white pd-all-xs txt-responsive-sm txt-grey pull-left block mg-l-sm">
				<span class="icon icon-size-150 icon-reorder">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					Stop Ordering
				</span>
			</a>

			<a href="<?php echo base_url('admin/projects/new_category'); ?>" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
				<span class="icon icon-size-150 outline-light icon-plus">
					<?php echo config_item('icon_image'); ?>
				</span>
				<span class="text">
					New
				</span>
			</a>

			<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
					<span class="icon icon-size-150 outline-light icon-disk">
						<?php echo config_item('icon_image'); ?>
					</span>
					<span class="text">
						Save
					</span>
			</button>
		</div>
	</div>	


	<section>
		<div class="row">
			<div class="col-xs-60 col-sm-60">
			 	<div class="txt-responsive-lg txt-light bg-dark-grey txt-white pd-all-md pd-t-sm pd-b-sm txt-white col-sm-60">	
			 		Categories
			 	</div>
		 	</div>
		</div>
	
		<?php if ($categories): ?>
		<div class="bg-white row pd-all-md">

			<div class="row" id="edit-footer">

			<?php if ($all_categories): $new = $this->uri->segment(4); ?>
				<?php foreach($all_categories as $category):?>
						<div class="edits <?php echo (@$new && $new == $category->id)? 'editing' : '' ?> row bg-middle-grey bordered pd-all-sm mg-b-md" id="edit-<?php echo $category->id; ?>">

							<div class="col-xs-60">
								<div class="pull-right">
									<a href="<?php echo base_url('/admin/projects/categories'); ?>" class="bg-red pd-all-xs txt-responsive-xs txt-white block" >
										<span class="icon icon-size-150 outline-light icon-crosscircle stop-prop icon-container">
											<?php echo config_item('icon_image'); ?>
										</span>
										<span class="text txt-white">
											Cancel
										</span>
									</a> 	
								</div>
							</div>

							<div class="col-xs-60">
								<label for="prop-title-<?php echo $category->id; ?>" class="">Title</label>
							</div>
							<div class="col-xs-60">
								<input name="title[<?php echo $category->id ?>]" class="form-control" value="<?php echo set_value('title['.$category->id.']', $category->name); ?>" id="prop-title-<?php echo $category->id; ?>" />
							</div>
							<div class="col-xs-60">
								<label for="prop-description-<?php echo $category->id; ?>" class="">Description</label>
							</div>
							<div class="col-xs-60">
								<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor('description['.$category->id.']', set_value('description['.$category->id.']', $category->description)); ?></div>
							</div>
							
						</div>
				<?php endforeach; ?>
			<?php endif ?>
		
		</div>

			<ul class="manage-list" data-url="<?php echo base_url('admin/projects/update_category_order'); ?>">
		    
		    <?php foreach ($categories as $key => $category): ?>

				<li class="item-parent list-none" id="sort_<?php echo $category->id; ?>">
		 			<div class="item-child row bg-light-grey mg-t-sm pd-all-xxs">
		 				<div class="txt-responsive-sm stop-prop col-xs-40 mg-t-xxs pd-l-xxs">
						<?php echo $category->name ?>
						</div>

			            <div class="pull-right">	
							<a href="" class="bg-seagreen pd-all-xxs pd-l-xs pd-r-xs mg-r-xxxs block pull-left editable" data-rel="#edit-<?php echo $category->id; ?>">
								<span class="icon icon-size-100 outline-light icon-edit stop-prop icon-container">
									<?php echo config_item('icon_image'); ?>
								</span>
							</a>
							<a title="You are about to delete the category: <?php echo $category->name; ?>, do you wish to continue?" href="<?php echo base_url('admin/projects/remove_category/'.encode($category->id)); ?>" class="bg-red pd-all-xxs pd-l-xs pd-r-xs pull-right block confirm">
								<span class="icon icon-size-125 outline-light icon-cross stop-prop icon-container">
									<?php echo config_item('icon_image'); ?>
								</span>
							</a>	
							
							<div class="clearfix"></div>

						</div>

					<div class="clearfix"></div>

					</div>
		        </li>
		    <?php endforeach ?>
		    </ul>
		</div>
		<?php else: ?>
		<p>There are no Project Categories.</p>
		<?php endif; ?>

	</section>
</form>

<script type="text/javascript">
$(document).ready(function(){
	if($('.editable').length) {
		$('.edits').not('.editing').hide();
		$('.editable').on('click', function(e){
			e.preventDefault();
			$('.edits').hide();
			var target = $(this).data('rel');
			if($(target).length){
				$(target).show();
				scroll_to('edit-footer');
			}
		});
	}
});

</script>