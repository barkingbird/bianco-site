<?php
	if(isset($user) && is_object($user))
	{
		$hash = $user->hash;
		$hashExpieryDate = $user->hash_date;
		$userName = $user->name;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
	img, a img { border: none; }
	.shadow {
  		-webkit-box-shadow: 0 8px 6px -8px black;
     	-moz-box-shadow: 0 8px 6px -8px black;
         box-shadow: 0 8px 6px -8px black;
	}
	</style>
  </head>
  <body style="width: 100% !important;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%; margin: 0;padding: 0;font-family: Arial, Helvetica, sans-serif; color: #000000;font-size: 14px; background-color: #eeeeee;; color:#635046;">
		<table id="backgroundTable" style="background-color: #eeeeee; width: 100%;" bgcolor="#eeeeee" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td bgcolor="#eeeeee" valign="top">
						<table class="shadow" style="background-color:#ffffff;" width="600" align="center" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td align="left" valign="middle" bgcolor="#ffffff">&nbsp;</td>
								</tr>
								<tr>
									<td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tbody>
									    <tr>
									      <td width="20">&nbsp;</td>
									      <td width=""><table bgcolor="#ffffff" style="padding:20px 0px" width="100%">
									        <tbody>
									          <tr>
									            <td align="left"><a href="<?php echo site_url(); ?>"> <img height="100" src="<?php echo base_url(Site_settings::get_var('logo')); ?>" alt="<?php echo base_url(); ?>" /> </a></td>
								              </tr>
									          <tr>
									            <td align="left">&nbsp;</td>
								              </tr>
								            </tbody>
								          </table>
								          
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: arial, helvetica, sans-serif;">
    <tr>
      <td width="85%" class="contenttit">
          <p>Dear <?php echo $userName; ?>,<br><br></p></td>
      <td width="15%" class="content"><?php echo date("d/m/Y"); ?></td>
    </tr>
    <tr>
      <td colspan="2" class="content">
          <p align="justify">
              We have received a request to reset your password for the CMS.
          </p>
        <p align="justify">
            To reset your password, just click the link below and follow the instructions. <br/><i>Note you have 24 hours, after that the link becomes inactive.</i>
        </p>
        <p align="justify">
            <a target="_blank" href="<?php echo base_url('admin/enter_reset_password/'.encode($user->id).'/'.$hash); ?>">Reset Password</a><br><br>
        </p></td>
    </tr>
    <tr>
      <td colspan="2" class="content"><p>Sincerely,<br>
        <?php $nice_url = explode(':', base_url());   echo rtrim(end($nice_url), '/'); ?></p>
      </td>
    </tr>
</table>



								          <p>&nbsp;</p></td>
									      <td width="20">&nbsp;</td>
								        </tr>
								      </tbody>
								    </table></td>
								</tr>
								<tr>
									<td align="center" valign="middle">
										<table style="background-color: #ffffff; width: 650px;" border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td align="center" valign="top">&nbsp;</td>	
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
		</tbody>
	</table>
</body>
</html>