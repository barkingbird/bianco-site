<?php echo form_open_multipart('admin/blog/save_edit/'.encode($post->id), array('id' => 'edit-blog-form')); ?>

<div class="row action-buttons">
	<div class="pull-right mg-t-md mg-b-md">
		<a href="<?php echo base_url('/admin/blog'); ?>" class="txt-white bg-red pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-crosscircle">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				Close
			</span>
		</a>    

		<button type="submit" class="txt-white bg-seagreen pd-all-xs txt-responsive-sm txt-white pull-left block mg-l-sm">
			<span class="icon icon-size-150 outline-light icon-disk">
				<?php echo config_item('icon_image'); ?>
			</span>
			<span class="text">
				Save
			</span>
		</button>
	</div>
</div>

<section>
	<?php echo Input_helper::heading($post->title); ?>

	<div class="tab-container bg-white pd-all-md">
			
		<div class="mg-b-md">
			<a href="#details" data-form="edit-blog-form" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Details</a>
			<a href="#hero-gallery" data-form="edit-blog-form" class="tab-link txt-responsive-sm col-md-9 pd-all-xs">Images</a>
			<a href="#content-blog" data-form="edit-blog-form" class="tab-link txt-responsive-sm col-md-10 pd-all-xs">Content</a>
			<div class="txt-responsive-xxs text-right pull-right col-md-14 pd-all-xxs pd-t-sm">Please complete ALL tabs</div>
			<div class="clearfix"></div>
			<div class="tab-link-divider"></div>
		</div>   

		<div class="tab-content row dbl-pd" id="details">
			<div class="col-xs-60 dbl-pd">
				<!--TITLE -->
				<div class="col-sm-40">
					<div class="col-xs-60">
						<label for="blog-title">Title</label>
					</div>
					<div class="col-xs-60">
						<input class="form-control" type="text" name="title" id="blog-title" value="<?php echo set_value('title', $post->title); ?>"/>
					</div>
				</div>
				<!-- DATE ADDED-->
				<div class="col-sm-20 pd-l-md">
					<div class="col-xs-60">
						<label for="date_added">Date added</label>
					</div>
					<div class="col-xs-60">
						<input class="date form-control" type="text" name="date_added" id="date_added" value="<?php echo set_value('date_added', date("d/m/Y", strtotime($post->date_added))); ?>"/>
					</div>
				</div>

<!-- 				<div class="col-sm-60 mg-t-md">
					<div class="col-xs-60">
						<label for="date_added">Video URL</label>
					</div>
					<div class="col-xs-60">
						<input class="form-control" type="text" name="video" id="video" value="<?php echo set_value('video', $post->video); ?>" placeholder="eg: http://www.youtube.com/watch?v=" />
					</div>
				</div> -->

				<!-- LINK -->
				<div class="col-md-40 mg-t-md">
					<div class="col-xs-60">
						<label for="link">URL</label>
					</div>
					<div class="col-xs-60">
						<div class="form-control disabled auto-overflow">
							<span class="demo-url txt-responsive-xs"><?php echo base_url().set_value('permalink', $post->permalink); ?></span>
							<input type="hidden" value="<?php echo $post->permalink; ?>" name="permalink" id="permalink" />
						 </div>
					</div>


					<div class="col-xs-60 mg-t-md">
						<label class="col-xs-60" for="meta_description">Meta Description <span style="float: right; font-size: 0.75em">(max 255 characters)</span></label>
					</div>
					<div class="col-xs-60">						
						<textarea name="meta_description" id="meta_description" class="form-control"><?php echo set_value('meta_description', $post->meta_description); ?></textarea>
					</div>


				</div>
				<div class="col-md-20 pd-l-md mg-t-md">


					<div class="col-xs-60">
						<label>Featured Blog Post &nbsp; <input type="checkbox" name="featured" value="1" <?php echo set_checkbox('featured', '1', (bool) $post->featured); ?> /></label>
					</div>

					<div class="col-xs-60 mg-t-sm">
						<label for="prop-excerpt">Categories</label>
					</div>
					<div class="bg-light-grey pd-all-xs col-xs-60 ">
						<ul id="categoy-select">
						<?php if(isset($categories) && is_array($categories)) : ?>
						<?php foreach ($categories as $key => $category): ?>
						<li class="list-none">
							<label class="custom-checkbox txt-responsive-xs" for="category-<?php echo $category->id ?>" >
								<input class="" id="category-<?php echo $category->id ?>" type="checkbox" <?php echo set_checkbox('category[]', $category->id, in_array($category->id, $assigned_categories)); ?> value="<?php echo $category->id ?>" name="category[]">
								<span class="circle"><i></i></span>
								<span class="text"><?php echo $category->name ?></span>
							</label>
						</li>         
						<?php endforeach ?>
						<?php endif ?>  
						</ul>
					</div>
					<div class="col-xs-60 mg-t-sm">New Category</div>
					<div class="col-xs-60 mg-t-sm">
						<div class="col-xs-60 col-sm-60">
							
								<select name="new_category_parent" id="new_category_parent">
									<option value="">-- Select Parent --</option>
									<?php if(isset($categories) && is_array($categories)) : ?>
									<?php foreach ($categories as $key => $category): ?>
										<option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
									<?php endforeach ?>
									<?php endif; ?>
								</select>
							
						</div>
						<div class="col-xs-60 col-sm-40 mg-t-sm">
							<input class="form-control" type="text" name="new_category" id="new_category" placeholder="Name" />
						</div>
						<button class="button txt-white bg-seagreen pd-all-xxs txt-responsive-xs txt-white pull-left block mg-l-xs add_category_btn mg-t-sm" type="button" insert-to = "#categoy-select" data-url="<?php echo base_url('admin/blog/add_new_blog_category'); ?>" data-rel="<?php echo encode($post->id); ?>" id="add-category">
							<span class="icon icon-size-100 outline-light icon-plus">
								<?php echo config_item('icon_image'); ?>
							</span>
							<span class="text pd-r-xxs">
								Add
							</span>
						</button>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div> <!-- / DETAILS TAB -->

		<div class="tab-content" id="hero-gallery"> 
			<div class="row action-buttons">

				<?php echo gallery_message(); ?>
			
				<div class="pull-right">
					<a class="txt-grey start-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left" href="#" style="display: block;">
						<span class="icon icon-size-150 icon-reorder">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Order Images
						</span>
					</a>
					<a href="#" class="txt-grey stop-ordering bg-light-grey pd-all-xs txt-responsive-sm txt-grey block pull-left">
						<span class="icon icon-size-150 icon-reorder">
							<?php echo config_item('icon_image'); ?>
						</span>
						<span class="text">
							Stop Ordering
						</span>
					</a>
				</div>
			</div>

			<div class="row std-pd">
				
				<ul class="manage-list manage-galleries col-xs-60" data-url="<?php echo base_url('admin/blog/update_blog_image_order') ?>">
						
					<?php 
						if ($post_images) :
							foreach($post_images as $key => $image) :
								$this->load->view('admin/_partials/blog_image_partial', array('post_id' => $post->id, 'image' => $image));
        					endforeach;
						endif; 
					?>

					<li class="upload-new-image col-xs-60 col-sm-30 col-md-20 std-pd ordering-hide" data-name="file">
					<div class="row bg-white mg-t-sm pd-all-sm dashed-bordered new-image-box">

						<div class="clearfix pd-t-xxxs"></div>
						
						<div class="img-placeholder image-center byheight row mg-t-xs mg-b-xs">
							<div id="galery-uploader" class="dropzone multi-uploader" data-title="Upload Images" data-name="file" data-action="<?php echo base_url('admin/blog/upload_blog_image/'.encode($post->id)); ?>">
								<div class="fallback">
					                <img src="<?php echo base_url('assets/admin/images/no-image-small.png'); ?>" class="img-responsive">
					            </div>
					            <div class="dz-default dz-message">
					                Drag files here to upload (or click) to upload a new Image
					                
					            </div>
				            </div>
						</div>

						<div class="clearfix pd-t-sm"></div>
						
						<div class="row txt-responsive-xs inherit mg-t-xxs">
							<div class="col-xs-60 col-sm-20 inherit">
								<label for="prop-new_image_title">Title</label>
							</div>
							<div class="col-xs-60 col-sm-40 inherit">
								<input type="text" name="new_image_title" id="prop-new_image_title" value="<?php echo set_value('new_image_title'); ?>" />
							</div>
						</div>
					
						<div class="row txt-responsive-xs inherit mg-t-xs">
							<div class="col-xs-60 col-sm-20 inherit"> 
								<label for="prop-new_image_link">Link</label>
							</div>
							<div class="col-xs-60 col-sm-40 inherit">
								<input type="text" name="new_image_link" id="prop-new_image_link" value="<?php echo set_value('new_image_link'); ?>" />
							</div>
						</div> 

						<div class="clearfix"></div>

					</div>
				</li>
				</ul>
			</div>
		</div> <!-- / HERO GALLERY TAB -->

		<div class="tab-content" id="content-blog">
						<!--- CONTENT -->

				 <!--- EXCERPT -->    
				<div class="row">
					<label class="col-xs-60" for="prop-excerpt">Excerpt</label>
					<div class="col-xs-60">
						<textarea name="excerpt" id="prop-excerpt" class="form-control"><?php echo set_value('excerpt', $post->excerpt); ?></textarea>
					</div>
				</div>    
						
				<div class="row mg-t-md ">
					<label for="content" class="col-xs-60 col-xs-60 col-md-60 col-lg-60">Content</label>
					<div class="full-ck col-xs-60"><?php echo $this->ckeditor->editor("content", set_value('content', $post->content)); ?></div>
				</div>
		</div>

	</div><!-- / TAB CONTAINER -->

</section>

	<input type="hidden" value="" name="author_id" />        
	<input type="hidden" value="<?php echo blog_url(); ?>" id="dummy_parent" name="dummy_parent" />
</form>    

