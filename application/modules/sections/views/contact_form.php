<?php
	$form_name = @$content->form_name;
	$form_id = @$content->id;
	$sent = FALSE;
	$errors = FALSE;

	if (isset($form_result) && $form_result) 
	{
		if (isset($form_result->form_name) && $form_result->form_name == $form_name) 
		{
			$errors = TRUE;
			if (isset($form_result->form_success) && $form_result->form_success == TRUE) 
			{
				$sent = TRUE;
				$errors = FALSE;
			}
		}
	} 
	?>
		<?php if ($sent) : ?>

		<div class="form-thankyou">
			<h1 class="h2">Thank you for your registration of interest.</h1>
		</div>

		<?php else : ?>

		<?php if ($errors && validation_errors()) : ?>
		<div class="form-validation-errors">
			<p>Please fill in the required fields.</p>
		</div>
		<?php endif; ?>

<section class="contact sec">
		<div class="container">
		<h1>Enquire here</h1>
		<p>“Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.“</p>
			<form class="register__form here" method="POST" action="" name="<?php echo $form_name; ?>">
				<input type="hidden" name="ref" value="<?php echo $form_name; ?>" required/>
				<input type="hidden" name="form" value="<?php echo $form_id;?>" required/>
				<input type="hidden" name="db_lead_source" value="Web" required/>
				<div class="contact-row">
					<input type="text" name="db_first_name" placeholder="First name*" value="<?php echo set_value('db_first_name'); ?>" required required/>
					<input type="email" name="db_email" placeholder="Email*" value="<?php echo set_value('db_email'); ?>" required />
				</div>
				<div class="contact-row">
					<input type="text" name="db_phone" placeholder="Phone*" value="<?php echo set_value('db_phone'); ?>" required />
					<div class="custom-select">
						<select name="db_source" required >
							<option selected disabled>Where did you hear about us?*</option>
							<option value="Google Search" <?php echo set_select('db_source', 'Google Search'); ?> >Google Search</option>
							<option value="Realestate.com.au" <?php echo set_select('db_source', 'Realestate.com.au'); ?> >Realestate.com.au</option>
							<option value="Signage" <?php echo set_select('db_source', 'Signage'); ?> >Signage</option>
							<option value="Walk-in" <?php echo set_select('db_source', 'Walk-in'); ?> >Walk-in</option>
							<option value="Word of month" <?php echo set_select('db_source', 'Word of month'); ?> >Word of month</option>
						</select>
					</div>
				</div>
				<textarea placeholder="Enter your enquiry here..." value="<?php echo set_value('db_message'); ?>" required > </textarea>
				<span>*Required fields</span>

				<button type="submit" class="button register__submit">Submit</button>
			</form>
		</div>
</section>

	<?php endif; ?>
