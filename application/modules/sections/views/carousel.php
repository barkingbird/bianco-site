<section class="section carousel-section">
    <div class="container">
        <div class="row std-pd">
            <?php if($items):
                $counter = 1; foreach ($items as $item): ?>
                <div class="col-md-20 col-sm-30 col-xs-60 std-pd">
                    <div class="carousel-section__wrap <?php echo ($item->show_artists_impression === 'yes') ? 'ai' : '';?>">
                        <img class="img-responsive" src="<?php echo base_url($item->image); ?>">
                        <a class="carousel-section__overlay js-fancybox <?php echo ($item->show_artists_impression === 'yes') ? 'fancybox-with-ai' : '';?>" data-fancybox="carousel" href="<?php echo base_url($item->image); ?>">
                            <i class="fa fa-search-plus carousel-section__overlay__icon" aria-hidden="true"></i>
                        </a>
                    </div>
                </div> 
                <?php if ($counter % 3 === 0): ?>
                <div class="clearfix visible-md visible-lg"></div>
                <?php elseif ($counter % 2 === 0): ?>
                <div class="clearfix visible-sm"></div>
                <?php endif;?>
            <?php $counter ++; endforeach; endif;?>
        </div>
    </div>
</section>