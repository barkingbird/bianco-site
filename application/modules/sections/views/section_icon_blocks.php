<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section class="article-section article-section__icon-blocks <?php echo $content->bgcolor; ?>">
	
	<div class="image-text ">
		<div class="text col-sm-60">
			<div class="inner-text">
				<?php if ($content->title) : ?>
				<h1><?php echo $content->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($content->subheading) : ?>
				<div class="subheading"><?php echo $content->subheading; ?></div>
				<?php endif; ?>

				<?php if ($content->description) : ?>
				<div class="description"><?php echo $content->description; ?></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
		<div class="icons flex">
			<?php foreach ($items as $item) : ?>
				<div class="icon">
					<img src="<?php echo display_image($item->icon, 'desktop_'); ?>" alt="<?php echo $item->caption; ?>" class="">
					<p><?php echo $item->caption; ?></p>
				</div>
			<?php endforeach; ?>
		</div>
	
</section>

<?php endif; ?>
