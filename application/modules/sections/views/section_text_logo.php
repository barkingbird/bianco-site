<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section id="<?php echo $content->name; ?>" class="article-section article-section__image-text logo">
	<!-- <?php //foreach ($items as $item) : ?> -->
	<div class="image-text">
		<div class="text col-sm-60">
			<div class="inner-text">
				<?php if ($content->title) : ?>
				<h1 class="white"><?php echo $content->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($content->logo) : ?>
				<img src="<?php echo $content->logo; ?>">
				<?php endif; ?>


				<?php if ($content->subheading) : ?>
				<div class="subheading"><?php echo $content->subheading; ?></div>
				<?php endif; ?>

				<?php if ($content->subheading) : ?>
				<div class="subheading"><?php echo $content->subheading2; ?></div>
				<?php endif; ?>

			</div>
		</div>

		

		<?php if ($content->description) : ?>
		<div class="subheading"><?php echo $content->description; ?></div>
		<?php endif; ?>

		<?php if ($content->author) : ?>
		<div class="subheading"><?php echo $content->author; ?></div>
		<?php endif; ?>

		<?php if ($content->image) : ?>
		<img src="<?php echo display_image($content->image, 'desktop_'); ?>" class="">
		<?php endif; ?>
	</div>
	<!-- <?php //endforeach; ?> -->
</section>

<?php endif; ?>
