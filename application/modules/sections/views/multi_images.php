<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section class="article-section article-section__multi-image <?php echo $content->bgcolor; ?>">
		<div class="col-md-20 col-sm-60 inner-text">
			<?php if ($content->title) : ?>
				<h1><?php echo $content->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($content->subheading) : ?>
				<div class="subheading"><?php echo $content->subheading; ?></div>
				<?php endif; ?>

				<?php if ($content->caption) : ?>
				<div class="description"><?php echo $content->caption; ?></div>
				<?php endif; ?>
		</div>
		<div class="col-md-20 col-sm-60 images-wrapper flex">
		<?php foreach ($items as $item) : ?>
			<img class="images <?php echo $item->size; ?>" src="<?php echo base_url($item->image); ?>">
		<?php endforeach; ?>
		</div>
</section>

<?php endif; ?>