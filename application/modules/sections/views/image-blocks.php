<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section class="article-section article-section__image-blocks full-width-<?php echo $content->full_width; ?> ">
	<div class="container">
		<div class="col-md-20 col-sm-60">
			<h1><?php echo $content->title; ?></h1>
			<p><?php echo $content->description; ?></p>
		</div>
		<?php foreach ($items as $item) : ?>
		<div class="col-md-20 col-sm-60 images-wrapper">
			
			<div class="col-sm-60 image-block" style="background-image: url('<?php echo base_url($item->image); ?>')">
				<a href="<?php echo $item->link; ?>"><?php echo $item->linkname; ?></a>
			</div>
			
		</div>
		<?php endforeach; ?>

	</div>
</section>

<?php endif; ?>