<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>
<?php foreach ($items as $item) : ?>
<section id="<?php echo $content->title; ?>" class="article-section article-section__image-text video" style="background-image: url('<?php echo display_image($item->image, 'desktop_'); ?>')">
	
	<div class="image-text video" >
		<!-- <div class="image <?php echo $item->size; ?> flex col-sm-60">
			<img src="<?php echo display_image($item->image, 'desktop_'); ?>" alt="<?php echo $item->title; ?>" class="">
		</div> -->
		<!-- <div class="bg-video-wrap">
		    <video src="<?php echo base_url('assets/stone-water.mp4'); ?>" loop muted autoplay></video>
		</div> -->
		<div class="text col-sm-60">
			<div class="inner-text flex">
				<?php if ($item->title) : ?>
				<h1><?php echo $item->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($item->subheading) : ?>
				<div class="subheading"><?php echo $item->subheading; ?></div>
				<?php endif; ?>

				<?php if ($item->description) : ?>
				<div class="description"><?php echo $item->description; ?></div>
				<?php endif; ?>
			</div>
		</div>
		
	</div>
	
</section>
<?php endforeach; ?>
<?php endif; ?>
