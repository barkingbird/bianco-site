<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
$image = $items[0];
?>

<section class="article-section article-section--full-image full-width-<?php echo $content->full_width; ?> ">
	<div class="container">

		<?php if (count($items) > 1) : ?>

		<div class="flexslider-wrapper">
				<div class="flexslider hero-gallery">
					<ul class="slides">
						<?php foreach ($items as $image) : ?>
							<li>
								<a class="image-center js-fancybox <?php echo ($image->show_artists_impression == 'yes') ? 'fancybox-with-ai' : ''; ?>" data-fancybox="group-<?php echo $id; ?>" href="<?php echo display_image($image->image, 'full_'); ?>" style="background-image: url('<?php echo display_image($image->image, 'desktop_'); ?>');"> 
									<img src="<?php echo display_image($image->image, 'desktop_'); ?>" alt="<?php echo @$content->title; ?>" class="" />
									<?php if ($image->show_artists_impression == 'yes') : ?>
										<span class="artist-impression">Artist's Impression</span>
									<?php endif; ?>
								</a>
							</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		
		<?php elseif (count($items) == 1) : ?>

		<a class="full-image js-fancybox <?php echo ($image->show_artists_impression == 'yes') ? 'fancybox-with-ai' : ''; ?>" data-fancybox="group-<?php echo $id; ?>" href="<?php echo display_image($image->image, 'full_'); ?>"> 
			<img src="<?php echo display_image($image->image, 'full_'); ?>" alt="<?php echo @$content->title; ?>" class="" />
			<?php if ($image->show_artists_impression == 'yes') : ?>
				<span class="artist-impression">Artist's Impression</span>
			<?php endif; ?>
		</a>

		<?php endif; ?>

	</div>
</section>

<?php endif; ?>
