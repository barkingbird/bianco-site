<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>
<?php foreach ($items as $item) : ?>
<section class="article-section article-section__image-text full" style="background-image: url('<?php echo display_image($item->image, 'desktop_'); ?>')">
	
	<div class="image-text">
		<div class="text col-sm-60">
			<div class="inner-text">
				<?php if ($item->title) : ?>
				<h1><?php echo $item->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($item->subheading) : ?>
				<div class="subheading"><?php echo $item->subheading; ?></div>
				<?php endif; ?>

				<?php if ($item->description) : ?>
				<div class="description"><?php echo $item->description; ?></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
</section>
<?php endforeach; ?>
<?php endif; ?>
