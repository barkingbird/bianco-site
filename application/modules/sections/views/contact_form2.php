
<section class="contact">
	<div class="container">
	<?php 
	if ( (isset($content) && $content)) :
	?>
		<h1><?php echo $content->title; ?></h1>
		<p><?php echo $content->subheading; ?></p>

	<?php endif; ?>
	
		<div class="register__form here">
		<?php
			$form_name = 'contact';
			$form_id = 'NQ';
			$sent = FALSE;
			$errors = FALSE;

			if (isset($form_result) && $form_result) 
			{
				if (isset($form_result->form_name) && $form_result->form_name == $form_name) 
				{
					$errors = TRUE;
					if (isset($form_result->form_success) && $form_result->form_success == TRUE) 
					{
						$sent = TRUE;
						$errors = FALSE;
					}
				}
			} 
			?>
				<?php if ($sent) : ?>

				<div class="form-thankyou">
					<h1 class="h2">Thank you for your enquiry.</h1>
				</div>

				<?php else : ?>

				<?php if ($errors && validation_errors()) : ?>
				<div class="form-validation-errors">
					<p>Please fill in the required fields.</p>
				</div>
				<?php endif; ?>


				<form class="" method="POST" action="" name="<?php echo $form_name; ?>" id="enquiry">
					<input type="hidden" name="ref" value="<?php echo $form_name; ?>" />
					<input type="hidden" name="form" value="<?php echo $form_id;?>" />
					<input type="hidden" name="db_lead_source" value="Web" />

					<input type="text" name="db_first_name" placeholder="Name*" value="<?php echo set_value('db_first_name'); ?>" required />
					<input type="text" name="db_phone" placeholder="Phone*" value="<?php echo set_value('db_phone'); ?>" required />
					<input type="email" name="db_email" placeholder="Email*" value="<?php echo set_value('db_email'); ?>" required />
					<div class="custom-select">
						<select name="db_source" required >
							<option selected disabled>How did you hear about us?*</option>
							<option value="Google Search" <?php echo set_select('db_source', 'Google Search'); ?> >Google Search</option>
							<option value="Realestate.com.au" <?php echo set_select('db_source', 'Realestate.com.au'); ?> >Realestate.com.au</option>
							<option value="Signage" <?php echo set_select('db_source', 'Signage'); ?> >Signage</option>
							<option value="Walk-in" <?php echo set_select('db_source', 'Walk-in'); ?> >Walk-in</option>
							<option value="Word of month" <?php echo set_select('db_source', 'Word of month'); ?> >Word of month</option>
						</select>
					</div>
					<textarea name="db_message" placeholder="Enter your enquiry here..." value="<?php echo set_value('db_message'); ?>" required > </textarea>
					<span>*Required fields</span>
					<button type="submit" class="button button--secondary register__submit">Submit</button>
				</form>
			<?php endif; ?>
		</div>
	</div>
</section>