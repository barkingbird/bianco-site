<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section id="<?php echo $content->name; ?>" class="article-section article-section__image-text form  flex <?php echo $content->bgcolor; ?>">
	<?php foreach ($items as $item) : ?>
	<div class="image-text ">
		<div class="text col-sm-60">
			<div class="inner-text">
				<?php if ($content->title) : ?>
				<h1><?php echo $content->title; ?></h1>
				<?php endif; ?>

				<?php if ($content->caption) : ?>
				<div class="subheading"><?php echo $content->caption; ?></div>
				<?php endif; ?>
			</div>
		</div>
		
	</div>
	<?php endforeach; ?>

	<div class="container">
		<div class="register__form here">
		<?php
			$form_name = 'Register';
			$form_id = 'MQ';
			$sent = FALSE;
			$errors = FALSE;

			if (isset($form_result) && $form_result) 
			{
				if (isset($form_result->form_name) && $form_result->form_name == $form_name) 
				{
					$errors = TRUE;
					if (isset($form_result->form_success) && $form_result->form_success == TRUE) 
					{
						$sent = TRUE;
						$errors = FALSE;
					}
				}
			} 
			?>
				<?php if ($sent) : ?>

				<div class="form-thankyou">
					<h1 class="h2">Thank you for your enquiry.</h1>
				</div>

				<?php else : ?>

				<?php if ($errors && validation_errors()) : ?>
				<div class="form-validation-errors">
					<p>Please fill in the required fields.</p>
					<p><?php echo $errors; ?></p>
				</div>
				<?php endif; ?>


				<form method="POST" action="" name="<?php echo $form_name; ?>" id="enquiry">
					<input type="hidden" name="ref" value="<?php echo $form_name; ?>" />
					<input type="hidden" name="form" value="<?php echo $form_id;?>" />
					<input type="hidden" name="db_lead_source" value="Web" />

					<div class="contact-row">
						<input type="text" name="db_first_name" placeholder="First Name*" value="<?php echo set_value('db_first_name'); ?>" required />
						<input type="text" name="db_first_name" placeholder="Surname*" value="<?php echo set_value('db_first_name'); ?>" required />
						
					</div>
					
					<input type="email" name="db_email" placeholder="Email*" value="<?php echo set_value('db_email'); ?>" required />
					<input type="text" name="db_phone" placeholder="Phone*" value="<?php echo set_value('db_phone'); ?>" required />
					<div class="contact-row">
						<div class="custom-select">
							<select name="db_source" required >
								<option selected disabled>How did you hear about us?*</option>
								<option value="Google Search" <?php echo set_select('db_source', 'Google Search'); ?> >Google Search</option>
								<option value="Realestate.com.au" <?php echo set_select('db_source', 'Realestate.com.au'); ?> >Realestate.com.au</option>
								<option value="Signage" <?php echo set_select('db_source', 'Signage'); ?> >Signage</option>
								<option value="Walk-in" <?php echo set_select('db_source', 'Walk-in'); ?> >Walk-in</option>
								<option value="Word of month" <?php echo set_select('db_source', 'Word of month'); ?> >Word of month</option>
							</select>
						</div>
					</div>
					<div class="contact-row">
						<input type="submit" class="button button--secondary register__submit" value="submit">
					</div>
				</form>
			<?php endif; ?>
		</div>
	</div>

	<?php foreach ($items as $item) : ?>
	<div class="image-text ">
		<div class="text col-sm-60">
			<div class="inner-text">

				<?php if ($item->subheading) : ?>
				<div class="subheading"><?php echo $item->subheading; ?></div>
				<?php endif; ?>  


				<?php if ($item->details) : ?>
				<div class="details"><?php echo $item->details; ?></div>
				<?php endif; ?>

				<?php if ($item->message) : ?>
				<div class="description"><?php echo $item->message; ?></div>
				<?php endif; ?> 
			</div>
		</div>
		
	</div>
	<?php endforeach; ?>
</section>

<?php endif; ?>