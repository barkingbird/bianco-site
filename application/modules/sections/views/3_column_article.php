<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section class="article-section article-section--3-column full-width-<?php echo $content->full_width; ?> ">
	<div class="container">

		<div class="row">

			<?php foreach ($items as $item) : ?>
			<div class="column col-xs-60 col-sm-20">

				<?php if ($item->title) : ?>
				<h1 class="h4 column_heading">
					<img class="image-up" src="<?php echo base_url('assets/images/heading-symbol-up.png'); ?>" aria-hidden="true"/>
					<?php echo $item->title; ?>
					<img class="image-down" src="<?php echo base_url('assets/images/heading-symbol-down.png'); ?>" aria-hidden="true"/>
				</h1>
				<?php endif; ?>
				
				<?php if ($item->description) : ?>
				<div class="wysiwyg-content"><?php echo $item->description; ?></div>
				<?php endif; ?>

				<?php if ($item->image) : ?>
				<div class="column_image">
					<img src="<?php echo display_image($item->image, 'mobile_'); ?>" alt="<?php echo @$item->title; ?>" class="" />
				</div>
				<?php endif; ?>							

			</div>
			<?php endforeach; ?>
				
		</div>
	</div>
</section>

<?php endif; ?>
