 <!-- <?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
$image = $items[0];
?>

<section class="article-image article-row-image--text-<?php echo $content->text_position; ?> ">
		
		<div class="article-image__row">
			<div class="article-image__row-wrap">
				<?php if ($item->title) : ?>
				<h1 class="article-image__title"><?php echo $item->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($item->subheading) : ?>
				<div class="article-image__description"><?php echo $item->subheading; ?></div>
				<?php endif; ?>

				<?php if ($item->description) : ?>
				<div class="article-image__description"><?php echo $item->description; ?></div>
				<?php endif; ?>

			</div>
		</div>
</section>

<?php endif; ?> -->



<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
$image = $items[0];
?>

<section class="article-image article-image--text-<?php echo $content->text_position; ?> ">
	<div class="flex-row">
		<div class="article-image__gallery">
			<div class="flexslider-wrapper">
					<div class="flexslider hero-gallery">
						<ul class="slides">
							<?php foreach ($items as $item) : ?>
								<li>
									<a class="image-center js-fancybox <?php echo ($item->show_artists_impression == 'yes') ? 'ai' : ''; ?>" data-fancybox="group-<?php echo $id; ?>" href="<?php echo display_image($image->image, 'full_'); ?>" style="background-image: url('<?php echo display_image($item->image, 'desktop_'); ?>');"> 
										<img src="<?php echo display_image($item->image, 'desktop_'); ?>" alt="<?php echo @$item->title; ?>" class="" />
										<span class=" fancybox-zoom"><i class="fal fa-search-plus"></i></span>
									</a>
								</li>
							<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="article-image__content flex-row flex-row--center">
			<div class="article-image__content-wrap">
				<?php if ($item->title) : ?>
				<h1 class="article-image__title"><?php echo $item->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($item->subheading) : ?>
				<div class="article-image__description"><?php echo $item->subheading; ?></div>
				<?php endif; ?>

				<?php if ($item->description) : ?>
				<div class="article-image__description"><?php echo $item->description; ?></div>
				<?php endif; ?>
				
				
		</div>
	</div>
</section>

<?php endif; ?>

