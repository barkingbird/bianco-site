<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section class="article-section article-section__image-text">
	<?php foreach ($items as $item) : ?>
	<div class="image-text <?php echo $item->bgcolor; ?>">
		<div class="text col-sm-60">
			<div class="inner-text">
				<?php if ($item->title) : ?>
				<h1><?php echo $item->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($item->subheading) : ?>
				<div class="subheading"><?php echo $item->subheading; ?></div>
				<?php endif; ?>

				<?php if ($item->description) : ?>
				<div class="description"><?php echo $item->description; ?></div>
				<?php endif; ?>
			</div>
		</div>
		<div class="image flex col-sm-60">
			<img src="<?php echo display_image($item->image, 'desktop_'); ?>" alt="<?php echo $item->title; ?>" class="">
		</div>
	</div>
	<?php endforeach; ?>
</section>

<?php endif; ?>
