<?php 
if ( (isset($content) && $content) && (isset($items) && $items) ) :
?>

<section id="<?php echo $content->name; ?>" class="article-section article-section__image-text <?php echo $content->bgcolor; ?>">
	<?php foreach ($items as $item) : ?>
	<div class="image-text ">
		<div class="text col-sm-60">
			<div class="inner-text">

				<?php if ($content->title) : ?>
				<h1><?php echo $content->title; ?></h1>
				<?php endif; ?>


				<?php if ($content->caption) : ?>
				<div class="subheading caption"><?php echo $content->caption; ?></div>
				<?php endif; ?>

				<?php if ($item->title) : ?>
				<h1><?php echo $item->title; ?></h1>
				<?php endif; ?>
				
				<?php if ($item->subheading) : ?>
				<div class="subheading"><?php echo $item->subheading; ?></div>
				<?php endif; ?>

				<?php if ($item->description) : ?>
				<div class="description"><?php echo $item->description; ?></div>
				<?php endif; ?>
			</div>
		</div>
		<div class="image <?php echo $item->size; ?> <?php echo $item->position; ?> flex col-sm-60">
			<img src="<?php echo display_image($item->image, 'desktop_'); ?>" alt="<?php echo $item->title; ?>" class="">
		</div>
	</div>
	<?php endforeach; ?>
</section>

<?php endif; ?>
