<?php  
class Gallery extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all()
    {
        $this->db->order_by('display_order');
        $query = $this->db->get('gallery');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }
	
    function build_all($image_limit = 10)
    {
        $this->db->order_by('display_order');
        $query = $this->db->get('gallery');
		$return = array();
        if ($query->num_rows() > 0){
		$i = 0;
		foreach($query->result() as $row)
		{
		    $return[$i]['gallery'] = $row;
		    $return[$i]['images'] = self::get_images($row->id, $image_limit);
		    $i++;
		}
	}
	return (count($return) > 0)? $return : FALSE;
    }

    function get_gallery($page_id = FALSE)
    {
        $this->db->where('id', $page_id);
        //$this->db->order_by();
        $query = $this->db->get('gallery', 1);

        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }
	
	function by_permalink($permalink = FALSE)
    {
        $this->db->where('permalink', $permalink);
        $query = $this->db->get('gallery', 1);

        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    function get_images($gallery_id = FALSE, $limit = FALSE, $args = FALSE)
    {
        if($gallery_id === false || ($gallery_id = trim($gallery_id)) === '')
            return false;

        if ($args) $this->db->where($args);
        $this->db->where('gallery_id', $gallery_id);
        $this->db->order_by('display_order');
		if($limit) $this->db->limit($limit);
        $query = $this->db->get('gallery_images');

        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    function get_hero($gallery_id = FALSE, $args = FALSE)
    {
        if(!$gallery_id) return false;

        if ($args) $this->db->where($args);
        
        $this->db->where('gallery_id', $gallery_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('gallery_images', 1);

        return ($query->num_rows() > 0)? $query->row() : FALSE;
    }

    
}