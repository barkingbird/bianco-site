<?php if(isset($images) && $images) :?>
	<div class="flexslider-wrapper">
		<div class="flexslider home-hero-gallery js-fade-gallery ">
			<ul class="slides">
				<?php foreach ($images as $image) : ?>
					<li>
						<div class="image-center"  style="background-image: url('<?php echo display_image($image->image, 'full_'); ?>');"> 
							<img src="<?php echo display_image($image->image, 'full_'); ?>" alt="<?php echo @$image->description; ?>" class="" />
						</div>
						<?php if ($image->title || $image->description) : ?>
						<i class="hero-gallery__overlay"></i>
						<?php endif; ?> 
						<?php if (isset($image->mp4)) : ?>
						<video class="video-js vjs-default-skin vjs-big-play-centered cover autoplay"
							controls preload="auto" width="640" height="480"
							poster="<?php echo display_image($image->image, $image_quality); ?>"
						>
							<source src="<?php echo base_url($image->mp4); ?>" type="video/mp4" />
						</video>
						<?php endif; ?>
						<?php if (isset($image->title) || isset($image->description)) : ?>
							<div class="caption caption--<?php echo $image->caption_position?> container">
								<div class="valign-parent"><i class="valign"></i>
									<span class="valign">
										<div class="caption__wrap">
											<?php if ($image->title) : ?>
											<p class="caption-heading"><?php echo $image->title ; ?></p>
											<?php endif; ?>
											<?php if (isset($image->description) && $image->description) : ?>
											<p class="caption-description"><?php echo $image->description; ?></p>
											<?php endif; ?>
											<?php if ($image->link) : ?>
											<a class="button button--secondary button--lg" href="<?php echo base_url($image->link); ?>">
												<?php echo ($image->link_text)? $image->link_text : 'Learn More'; ?>
											</a>
											<?php endif; ?>
										</div>
									</span>
								</div>
							</div>
						<?php endif; ?>
						<?php if (isset($image->subtitle)):?>
						<p class="hero-gallery__subtitle"><?php echo $image->subtitle;?></p>
						<?php endif;?>
						<?php if ($image->artists_impression == 'yes') : ?>
						<span class="artist-impression">
							Artist's Impression
						</span>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
			<div class="hero-gallery__anchor">
				<span class="hero-gallery__anchor__line"></span>
				<a href="#project-detail" class="js-scroll-to hero-gallery__anchor__link">SCROLL</a>
			</div>
		</div>
	</div>
<?php endif; ?>