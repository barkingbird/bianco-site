



<div class="template-projects">

	<?php 
	$project_url = project_url(); 
	$filter = ($this->input->get('service')) ? '?service='.$this->input->get('service') : '';
	?>

	
	<?php 
	$hero_image = FALSE;
	if ($gallery_images) 
	{
		$hero_image  = reset($gallery_images);
		$this->load->view('_partials/details-gallery', array('images' => $gallery_images)); 
	} else {
		$this->load->view('_partials/empty-gallery', array('images' => $gallery_images)); 
	} ?>
			


	<div class="project-details" id="project-detail">
		<div class="container">
			<div class="row" >
				<!-- <div class="project-details__col"><h1 class="project-details__title"><?php echo $project->title;?></h1></div>-->
				<div class="project-details__link pull-right">
					<i class="fal fa-long-arrow-left"></i>&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url( $project_url);?>" class="">Back To Projects</a>
				</div>
				<div class="project-details__col pull-left"><h6 class="project-details__address"><?php echo $project->address;?></h6></div>
				
			</div>
			<div class="flex-row">
				<h6 class="project-details__subtitle"><?php echo $project->subtitle;?></h6>
				<div class="project-details__col">
					<div class="project-details__content visible-sm">
						<div class="wysiwyg-content">
							<?php echo filter_wysiwyg($project->detail); ?>
						</div>
					</div>
				</div>
				<!-- <div class="project-details__col">
					<div class="flex-row">
						<div class="project-details__sm-col">
							<?php if(isset($project->type)):?>
							<label class="project-details__label">Type</label>
							<p class="project-details__detail"><?php echo $project->type;?></p>
							<?php endif;?>
							<?php if(isset($project->budget)):?>
							<label class="project-details__label">budget</label>
							<p class="project-details__detail"><?php echo $project->budget;?></p>
							<?php endif;?>
							<?php if(isset($project->commenced)):?>
							<label class="project-details__label">commenced</label>
							<p class="project-details__detail"><?php echo $project->commenced;?></p>
							<?php endif;?>
							<?php if(isset($project->completed)):?>
							<label class="project-details__label">completed</label>
							<p class="project-details__detail"><?php echo $project->completed;?></p>
							<?php endif;?>
						</div>
						<div class="project-details__sm-col">
							<?php if(isset($project->architect)):?>
							<label class="project-details__label">architect</label>
							<p class="project-details__detail"><?php echo $project->architect;?></p>
							<?php endif;?>
							<?php if(isset($project->interiors)):?>
							<label class="project-details__label">interiors</label>
							<p class="project-details__detail"><?php echo $project->interiors;?></p>
							<?php endif;?>
							<?php if(isset($project->landscaper)):?>
							<label class="project-details__label">landscaper</label>
							<p class="project-details__detail"><?php echo $project->landscaper;?></p>
							<?php endif;?>
						</div>
					</div>
				</div> -->
			</div>
			<div class="project-details__content hidden-sm">
				<div class="wysiwyg-content">
					<?php echo filter_wysiwyg($project->detail); ?>
				</div>
				
			</div>
			
		</div>
	</div>

	<?php if (isset($project->author, $project->testimonial) && $project->author != '' && $project->testimonial != ''):?>
	<!-- TESTIMONIAL -->
	<div class="section testimonial">
		<div class="container">
			<i class="testimonial__quote"><?php echo file_get_contents(base_url('assets/images/logos/quote.svg'));?></i>
			<p class="testimonial__content"><?php echo $project->testimonial;?></p>
			<h5 class="testimonial__author"><?php echo $project->author;?></h5>
		</div>
	</div>
	<!-- END TESTIMONIAL -->
	<?php endif;?>
</div>