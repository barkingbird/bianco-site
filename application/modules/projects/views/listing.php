<div class="header-placeholder"></div>

<?php if (@$row->sub_heading || @$row->detail) : ?>
<div class="page-content">
	<div class="container">
		
		<?php if ($row->sub_heading) : ?>
		<h1 class="heading animateMe" data-animation="fadeIn" data-animation-delay="1s"><?php echo $row->sub_heading; ?></h1>
		<?php endif; ?>

		<?php if ($row->detail) : ?>
		<div class="wysiwyg-content">
			<?php echo filter_wysiwyg($row->detail); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
<?php endif; ?>

<?php if ($projects) : ?>

<?php 
$project_url = project_url();
$filter = $this->input->get('service');
$filter_valid = FALSE;
?>
<div class="projects">
<h1>projects</h1>
	<?php if (!$filter_valid) { $filter = FALSE; } ?>

	<?php if (@$projects) : ?>

	<div class="container">
		<div class="project">

		<?php foreach ($projects as $project) : ?>
				<?php if($project->detail) : ?>
					<a class="js-project-listing projects__project" href="<?php echo base_url($project_url.$project->safe_name); ?>" data-category-id="<?php echo 'category-'.encode($project->category_id);?>">
					<?php else : ?>
					<span class="js-project-listing projects__project"  data-category-id="<?php echo 'category-'.encode($project->category_id);?>">
				<?php endif; ?>
				<figure class="image-center" style="background-image: url('<?php echo display_image($project->hero_image, 'full_'); ?>');"></figure>
				<div class="projects__project-overlay-wrap">
					<div class="projects__project-overlay">
						<h1><?php echo $project->title; ?></h1>
	
						<div class="projects__project-details">
							<?php if (@$project->intro): ?>
							<div class="projects__project-intro"><?php echo $project->intro;?></div>
							<?php endif;?>
							<div class="text-center"></div>
								<!-- <i class="fal fa-long-arrow-right"></i> -->
						</div>
					</div>
				</div>
				<?php if($project->detail) : ?>
					</a>
					<?php else : ?>
						</span>
				<?php endif; ?>
		<?php endforeach; ?>

	</div>
	<?php else: ?>
	<p>Currently no projects listing</p>
	<?php endif; ?>
	
</div>
<?php endif; ?>

<?php 
if (isset($page_sections) && $page_sections) : 
	foreach ($page_sections as $section) : 
		echo $section->html; 
	endforeach;
endif;
?>
