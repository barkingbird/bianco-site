
<hr class="header-hr" />

<?php if ($row->sub_heading || $row->detail) : ?>
<div class="page-content">
	<div class="container">
		
		<?php if ($row->sub_heading) : ?>
		<h1 class="heading"><?php echo $row->sub_heading; ?></h1>
		<?php endif; ?>

		<?php if ($row->detail) : ?>
		<div class="wysiwyg-content">
			<?php echo filter_wysiwyg($row->detail); ?>
		</div>
		<?php endif; ?>

	</div>
</div>
<?php endif; ?>

<?php 
	$blog_url = base_url(blog_url());  

	if ($images) 
	{
		$hero_image =  $images[0];
		unset($images[0]);
	} 
?>

<div class="blog-detail">
	
	<div class="container">

		<?php if ($post) : ?>
		<div class="row dbl-pd">

			<div class="col-xs-60 col-sm-15 dbl-pd">		
				<aside class="blog-categories">
					<h1>Categories</h1>
					<ul>
						<li><a href="<?php echo $blog_url; ?>" class="parent-category">All</a></li>
						<?php
						if (isset($categories) && $categories) :
							foreach ($categories as $key => $next_category) : 
								$category_url = $blog_url.'/'.$next_category->permalink;
						?>
						<li class="<?php echo ($this->uri->segment(3) == trim($next_category->permalink, '/')) ? 'active open' : ''; ?>">
							<?php $sub_categories = $this->posts->categories($next_category->id); ?>
							<a href="<?php echo $category_url; ?>" class="parent-category"><?php echo $next_category->name; ?> <?php if ($sub_categories): ?><i class="dropdown-arrow active fa fa-chevron-down" data-toggle="#cat_<?php echo $key; ?>" aria-hidden="true"></i><?php endif;?></a>
							<?php if ($sub_categories): ?>
							<ul class="open" id="cat_<?php echo $key; ?>">
								<?php foreach ($sub_categories as $key => $sub_category): ?>
								<li class="<?php echo ($this->uri->segment(4) == $sub_category->permalink) ? 'active' : ''; ?>">
									<?php $sub_sub_categories = $this->posts->categories($sub_category->id); ?>
									<a href="<?php echo $blog_url.'/'.$sub_category->permalink; ?>"><?php echo $sub_category->name; ?></a>
									
									<?php if ($sub_sub_categories): ?>
									<ul class="open">
										<?php foreach ($sub_sub_categories as $key => $sub_sub_category): ?>
										<li class="<?php echo ($this->uri->segment(4) == $sub_sub_category->permalink) ? 'active' : ''; ?>">
											<a href="<?php echo $category_url.$sub_sub_category->permalink; ?>"><?php echo $sub_sub_category->name; ?></a>
										</li>
										<?php endforeach ?>									
									</ul>
									<?php endif ?>

								</li>
								<?php endforeach ?>									
							</ul>
							<?php endif ?>								
						</li>
						<?php endforeach; ?>
						<?php endif; ?>		
					</ul>
				</aside>
			</div>

			<div class="col-xs-60 col-sm-45 dbl-pd">	
				<div class="blog-article">

					<?php if ($hero_image): ?>
					<figure class="row">
						<img class="img-responsive" src="<?php echo display_image($hero_image->image, 'desktop_'); ?>" alt="<?php echo $post->title; ?>">
					</figure>
					<?php endif; ?>

					
					
					
					<h1><?php echo $post->title; ?></h1>

					<time><?php echo formate_date("d F Y ", strtotime($post->date_added)); ?></time>
					<div class="clearfix"></div>
					<div class="wysiwyg-content">
						<?php echo $post->content; ?>
						<div class="clearfix"></div>
						<?php 
							if ($images):
								foreach ($images as $key => $image):
						?>
								<?php if ($image->link) : ?>
									
										<a href="<?php echo display_image($image->link); ?>" class="post-image" target="_blank">
											<img class="img-responsive" src="<?php echo display_image($image->image, 'desktop_'); ?>" alt="<?php echo $image->title; ?>">
										</a>
									
								<?php else: ?>
									
										<figure class="post-image">
											<img class="img-responsive" src="<?php echo display_image($image->image, 'desktop_'); ?>" alt="<?php echo $image->title; ?>">
										</figure>
									
								<?php endif; ?>
						<?php 
								endforeach;
							endif; 
						?>
						<div class="clearfix"></div>
					</div>
					<div class="row last">
						<div class="addthis_toolbox addthis_custom_sharing addthis_post addthis_32x32_style " addthis:image="<?php echo display_image($hero_image->image, 'mobile_'); ?>" addthis:title="<?php echo $post->title; ?>" addthis:description="<?php echo strip_tags($post->excerpt); ?>" addthis:url="<?php echo base_url($post->permalink); ?>">
								
							<span class="blog__addthis__share">share</span>
							<a href="#" class="js-share-all "><i class="fal fa-share-alt"></i></a>
							<a href="#" class="addthis_button_email"><i class="fas fa-envelope"></i></a>
							<a href="#" class="addthis_button_facebook"><i class="fab fa-facebook-f"></i></a>
							<a href="#" class="addthis_button_linkedin"><i class="fab fa-linkedin-in"></i></a>
							<a href="#" class="addthis_button_pinterest"><i class="fab fa-pinterest-p"></i></a>
							<a href="#" class="addthis_button_twitter"><i class="fab fa-twitter"></i></a>

						</div>
						<a class="back-to-news" href="<?php echo $blog_url; ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> back to news</a>
					</div>
				</div>
			</div>

		</div>
		<?php else : ?>
		<p>There are no articles at this time.</p>
		<?php endif; ?>

	</div>

</div>
<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ID_FROM_ADDTHIS"></script>
<div class="addthis_inline_share_toolbox"></div>
<!-- END Go to www.addthis.com/dashboard to customize your tools --> 