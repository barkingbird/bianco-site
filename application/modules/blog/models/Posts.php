<?php 
class Posts extends CI_Model {


    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_all($limit = FALSE, $count = FALSE)
    {
        if(!$limit)
        {
            $limit[0] = 0;
            $limit[1] = 10;
        }

        $this->db->order_by('date_added', 'DESC');
        $this->db->where('published !=', 0);
        $query = $this->db->get('blog_posts', $limit[1], $limit[0]);
        
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function post_hero($post_id, $all = FALSE)
    {
        if(!$post_id) return FALSE;

        $this->db->order_by('display_order');
        $this->db->where("post_id", $post_id);
        if(!$all) $this->db->limit(1);
        $query = $this->db->get('blog_images');
        
        return ($query->num_rows() > 0)? (($all)? $query->result() : $query->row()) : FALSE;
    }

    public function get_tags()
    {
        $query = $this->db->query("SELECT service_gallery_images.title FROM service_gallery_images WHERE title != '' GROUP BY title");
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function total($category = FALSE)
    {
        if ($category) 
        {
            $category = $this->category($category);
            if(!$category) return FALSE;

            $select = $this->db->query("SELECT COUNT(id) as counter FROM blog_posts WHERE blog_posts.id IN (SELECT post_id FROM blog_posts_to_categories WHERE category_id = ?)", array($category->id));
            $query = ($select->num_rows() > 0)? $select->row()->counter : FALSE;
        }
        else
        {
            $select = $this->db->query('SELECT COUNT(id) as counter FROM blog_posts WHERE blog_posts.id IN (SELECT post_id FROM blog_posts_to_categories )');
            $query = ($select->num_rows() > 0)? $select->row()->counter : FALSE; 
        }
        

        
        return (int)$query;
    }

    public function count_all_posts()
    {
        //$this->db->where("DATE(date_published)", "CURDATE()");
        //$this->db->order_by('date_published');
        $this->db->order_by('date_added', 'DESC');
        $query = $this->db->count_all_results('blog_posts');
        
        return (int)$query;
    }

    public function by_category($permalink = FALSE, $limit = FALSE, $count = FALSE)
    {
        if(!$limit)
        {
            $limit[0] = 0;
            $limit[1] = 10;
        }

        if ($permalink) 
        {
            $category = $this->category($permalink);
            if(!$category) return FALSE;

            $query = $this->db->query("SELECT * FROM blog_posts WHERE blog_posts.id IN (SELECT post_id FROM blog_posts_to_categories WHERE (category_id = ?)) ORDER BY date_added DESC LIMIT $limit[0], $limit[1]", array($category->id));
        }
        else
        {
            
            //$this->db->order_by('date_published');
            $this->db->order_by('date_added', 'DESC');
            $query = $this->db->get('blog_posts', $limit[1], $limit[0]);
        }
        
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function all_articles($limit = array(), $return_count = FALSE)
    {

        $calc = '';
        if($return_count) { $calc = 'SQL_CALC_FOUND_ROWS '; }

        //$this->db->where('published !=', 0);

        if ($limit) 
        {
            $query = $this->db->query('SELECT '.$calc.' * FROM blog_posts WHERE published != 0 LIMIT ?,?', array($limit[0], $limit[1]));
        }
        else
        {
            $query = $this->db->query('SELECT '.$calc.' * FROM blog_posts WHERE published != 0');
        }

        if($return_count)
        {
            $query = $this->db->query('SELECT FOUND_ROWS() AS record_count');
            return $query->row()->record_count;
        }
        
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function articles($category_id = FALSE, $limit = array(), $return_count = FALSE)
    {
        //if (!$category_id) return FALSE;

        $calc = '';
        if($return_count) { $calc = 'SQL_CALC_FOUND_ROWS '; }

        if($category_id)
        {
            if ($limit) 
            {
                $query = $this->db->query('SELECT '.$calc.' blog_posts.* FROM blog_posts_to_categories LEFT JOIN blog_posts ON blog_posts_to_categories.post_id = blog_posts.id  WHERE blog_posts_to_categories.category_id = ? AND blog_posts.published != 0 ORDER BY date_added DESC LIMIT ?,?', array($category_id, $limit[0], $limit[1]));
            }
            else
            {
                $query = $this->db->query('SELECT '.$calc.' blog_posts.* FROM blog_posts_to_categories LEFT JOIN blog_posts ON blog_posts_to_categories.post_id = blog_posts.id  WHERE blog_posts_to_categories.category_id = ? AND blog_posts.published != 0 ORDER BY date_added DESC', array($category_id));
            }
        }
        else
        {
            if ($limit) 
            {
                $query = $this->db->query('SELECT '.$calc.' blog_posts.* FROM blog_posts WHERE  blog_posts.published != 0 ORDER BY date_added DESC LIMIT ?,?', array($limit[0], $limit[1]));
            }
            else
            {
                $query = $this->db->query('SELECT '.$calc.' blog_posts.* FROM blog_posts WHERE blog_posts.published != 0 ORDER BY date_added DESC');
            }
        }
        

        if($return_count)
        {
            $query = $this->db->query('SELECT FOUND_ROWS() AS record_count');
            return $query->row()->record_count;
        }
        
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function categories($parent_id = FALSE)
    {
        if ($parent_id !== FALSE) 
        {
            $query = $this->db->query('SELECT * FROM blog_categories WHERE blog_categories.id IN (SELECT category_id FROM blog_posts_to_categories ) AND parent_id = ?  ORDER BY display_order', array($parent_id));    
        }
        else
        {
            $query = $this->db->query('SELECT * FROM blog_categories WHERE blog_categories.id IN (SELECT category_id FROM blog_posts_to_categories ) AND parent_id = FALSE ORDER BY display_order');
        }
        
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function post_categories($post_id = FALSE)
    {
        if(!$post_id) return FALSE;
        $query = $this->db->query('SELECT * FROM blog_categories WHERE blog_categories.id IN (SELECT category_id FROM blog_posts_to_categories WHERE post_id = ? ) ORDER BY display_order', array($post_id));       
        
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function post_category_names($post_id = FALSE)
    {
        if(!$post_id) return FALSE;
        $query = $this->db->query('
            SELECT GROUP_CONCAT(name) as name 
            FROM blog_categories 
            WHERE blog_categories.id IN (
                SELECT category_id 
                FROM blog_posts_to_categories 
                WHERE post_id = ? 
            ) 
            ORDER BY display_order', 
            array($post_id)
        );       
        
        return ($query->num_rows() > 0)? $query->row() : FALSE;
    }

    public function category($permalink = FALSE)
    {
        if(!$permalink) return FALSE;

        $this->db->where('permalink', trim($permalink, '/').'/');
        $query = $this->db->get('blog_categories', 1);

        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    public function get_page($page_id = FALSE)
    {
        $this->db->where('id', $page_id);
        $query = $this->db->get('blog_posts', 1);

        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    public function valid_premalink($page_id = FALSE, $uri = FALSE)
    {
        if($page_id)
        $this->db->where('id <>', $page_id);
    
        $this->db->where('permalink', $uri);
        $query = $this->db->get('blog_posts', 1);

        return ($query->num_rows() == 1)? FALSE : TRUE;
    }

    public function premalink($uri = FALSE)
    {
        //view(blog_url());
        if(!$uri) return FALSE;
        //$this->db->where('permalink', trim(str_replace(blog_url(), '', $uri), '/').'/');
        $this->db->where('permalink', $uri);
        $query = $this->db->get('blog_posts', 1);
        //view($this->db->last_query());
        return ($query->num_rows() == 1)? $query->row() : FALSE;
    }

    public function build_list($parent_id = 0) {
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('display_order');
        $query = $this->db->get('blog_posts');
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function featured_posts($limit = 2)
    {
        $this->db->where('published !=', 0);
        $this->db->where('featured !=', 0);
        $this->db->order_by('date_added', 'DESC');
        $query = $this->db->get('blog_posts', $limit);
        
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }

    public function sitemap()
    {
        $return = array(config_item('blog_url') => base_url(config_item('blog_url')));

        $this->db->order_by('date_published');
        $this->db->where('published', '1');
        $query = $this->db->get('blog_posts');
        
        if ($query->num_rows() == 0) return $return;

        foreach ($query->result() as $row) 
            $return['blog_post'.$row->id] = base_url($row->permalink); 

        return $return;
    }

    function search($search_string = FALSE)
    {
        if(!$search_string) return FALSE;
        
        $this->db->select('blog_posts.*');
        //$this->db->like('blog_posts.title', $search_string);
        //$this->db->or_like('blog_posts.content', $search_string);

        $this->db->where('(`blog_posts`.`title` LIKE "%'.$search_string.'%" OR `blog_posts`.`content` LIKE "%'.$search_string.'%")');
        $this->db->where('published', 1);

        $query = $this->db->get('blog_posts');
        //view($query->row());
        //view($this->db->last_query());
        return ($query->num_rows() > 0)? $query->result() : FALSE;
    }
}