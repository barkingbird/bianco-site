<?php

/**
* 
*/
class MY_Form_validation extends CI_Form_validation
{
	public function __construct($rules = array())
	{
		parent::__construct($rules);
		//$this->CI->lang->load('MY_form_validation');
	}

	function input_validations()
	{
		$html = FALSE;
		$fields = $this->_error_array;

		if($fields)
		{
			// foreach ($fields as $key => $value) {
				
			// }
			$html = '<script type="text/javascript"> $(document).ready(function() { scroll_to("enquireform"); });</script>';
		}
		return $html;
		//view($this->_field_data);
	}

	function show_validation_errors()
	{
		$html = FALSE;
		$fields = $this->_error_array;
		//view($fields);
		if($fields)
		{
			foreach ($fields as $key => $value) {
				$html .= '<script type="text/javascript"> $(document).ready(function() { $("*[name='.$key.']").addClass("validation-error"); });</script>';
			}
		}
		return $html;
	}

	function form_errors_array()
	{
		return $this->_error_array;
	}

	function phone_number($str_in)
	{
		if (! preg_match("/^([-0-9_ ])+$/i", $str_in))
	    {
	        $this->set_message('phone_number', 'The %s field may only contain numeric values and spaces.');
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}

	public function clear_field_data()
	{
		$this->_field_data = array();
	}

	//form validation callback function
    function check_lolly($field, $lolly)
    {
        if ($field != $lolly)
        {
            $this->form_validation->set_message('check_lolly', 'The Validation field does not match');
            return FALSE;
        }
        else
        {
            return TRUE;
        }

	}
	function verify_recaptcha($response)
	{ 
        // if no response received 
        if(empty($response))
        {
            $this->set_message('verify_recaptcha', 'Please confirm that you are not a robot.');
            return false;               
        }
        else {

        	$ci = &get_instance();
            // check if response code verified
            $result = $ci->recaptcha->verifyResponse($response);
            if(!$result['success']){
                $this->set_message('verify_recaptcha', 'Verifying reCAPTCHA is unsuccessful. Error: '.$result['error-codes']);
                return false;               
            }
            if ($result['score'])
            {
                $_POST['score'] = $result['score'];
                if ( $result['score'] < 0.5) 
                {
                    $this->set_message('verify_recaptcha', 'Your message could not be sent due to failing our SPAM check. Please call us for further assistance.');
                    return false; 
                } 
            }
        }   
        return true;
	}

}