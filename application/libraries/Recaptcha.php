<?php
/**
 * CodeIgniter Recaptcha library
 *
 * This is a PHP library that handles calling reCAPTCHA.
 *    - Documentation and latest version
 *          https://developers.google.com/recaptcha/docs/php
 *    - Get a reCAPTCHA API Key
 *          https://www.google.com/recaptcha/admin/create
 *    - Discussion group
 *          http://groups.google.com/group/recaptcha
 *
 * 
 * ****************** README FROM NINJA MIKE ***********************
 *  1. Register recaptcha keys and store in config/recaptcha.php
 *  2. On template.php call add_reCAPTCHA() with all form IDs as parameter.
 *  3. (*Not necessary for V3)In each form add a <div> for g-recaptcha's placeholder and use "{current_form_id}-recaptcha"
 *     as its ID. For example general_enquiry form element ID is "general-enquiry" and the recaptcha placeholder's 
 *     ID is "general-enquiry-recaptcha".
 *  4. Toggle self::IS_INVISIBLE, self::IS_V3 to decide whether or not use invisible recaptcha.
 *  5. Make sure in form validation g-recaptcha-response is required
 * ****************************************************************************************
 * 
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category    Libraries
 * @author      Prashant Pareek <prashantpareek1988@gmail.com>
 * @copyright   Copyright (c) 2014, Google Inc.
 * @link        http://www.google.com/recaptcha
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ReCaptcha Class
 */
class Recaptcha {
    /**
     * ci instance object
     *
     */
    private $_ci;

    /**
     * reCAPTCHA site up, verify and api url.
     *
     */
    const sign_up_url = 'https://www.google.com/recaptcha/admin';
    const site_verify_url = 'https://www.google.com/recaptcha/api/siteverify';
    const api_url = 'https://www.google.com/recaptcha/api.js';


     /**
     * Toggle invisible reCAPTCHA and normal reCAPTCHA and V3
     * (V3 and V2 can be used together so keep those two variable seperated for now)
     */
    const IS_INVISIBLE = FALSE;
    const IS_V3 = TRUE;


    /**
     * constructor     
     */
    public function __construct()
    {
        $this->_ci = & get_instance();
        $this->_ci->load->config('recaptcha'); 
            
        if (self::IS_V3)
        {
            $this->_siteKey = $this->_ci->config->item('recaptcha_v3_site_key');
            $this->_secretKey = $this->_ci->config->item('recaptcha_v3_secret_key');
        }
        else 
        {
            if (self::IS_INVISIBLE)
            {
                $this->_siteKey = $this->_ci->config->item('recaptcha_invisible_site_key');
                $this->_secretKey = $this->_ci->config->item('recaptcha_invisible_secret_key');
            }
            else
            {
                $this->_siteKey = $this->_ci->config->item('recaptcha_site_key');
                $this->_secretKey = $this->_ci->config->item('recaptcha_secret_key');
            }
        }
        
        $this->_language = $this->_ci->config->item('recaptcha_lang');

        if (empty($this->_siteKey) or empty($this->_secretKey)) {
            die("To use reCAPTCHA you must get an API key from <a href='"
                .self::sign_up_url."'>".self::sign_up_url."</a>");
        }         
    }   

    /**
     * Submits an HTTP GET to a reCAPTCHA server.
     *     
     * @param array  $data array of parameters to be sent.
     *
     * @return array response
     */
    private function _submitHTTPGet($data)
    {
        $url = self::site_verify_url.'?'.http_build_query($data);

        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($curl_handle, CURLOPT_USERAGENT, 'BB CMS');
        $response = curl_exec($curl_handle);
        curl_close($curl_handle);

        //$response = file_get_contents($url);

        return $response;
    }

    /**
     * Calls the reCAPTCHA siteverify API to verify whether the user passes
     * CAPTCHA test.
     *
     * @param string $remoteIp   IP address of end user.
     * @param string $response   response string from recaptcha verification.
     *
     * @return ReCaptchaResponse
     */
    public function verifyResponse($response, $remoteIp = null)
    {
        $remoteIp = (!empty($remoteIp)) ? $remoteIp : $this->_ci->input->ip_address();
        
        // Discard empty solution submissions
        if (empty($response)) {
            return array(
                'success' => false,
                'error-codes' => 'missing-input',
            );
        }

        $getResponse = $this->_submitHttpGet(
            array(
                'secret' => $this->_secretKey,
                'remoteip' => $remoteIp,   
                'response' => $response
            )
        );

        // get reCAPTCHA server response
        $responses = json_decode($getResponse, true);

        if (isset($responses['success']) and $responses['success'] == true) {
            $status = true;
            if (isset($responses['score']))
            {
                $score = $responses['score'];
            }
        } else {
            $status = false;
            $error = (isset($responses['error-codes'])) ? $responses['error-codes']
                : 'invalid-input-response';
        }

        return array(
            'success' => $status,
            'error-codes' => (isset($error)) ? $error : null,
            'score' => (isset($score)) ? $score : FALSE
        );
    }

    /**
     * Render Script Tag
     *
     * onload: Optional.
     * render: [explicit|onload] Optional.
     * hl: Optional.
     * see: https://developers.google.com/recaptcha/docs/display
     *
     * @param array parameters.
     *
     * @return scripts
     */
    public function getScriptTag(array $parameters = array())
    {
        $default = array(
            'render' => 'onload',
            'hl' => $this->_language,
        );

        $result = array_merge($default, $parameters);

        $scripts = sprintf('<script type="text/javascript" src="%s?%s" async defer></script>',
            self::api_url, http_build_query($result));

        return $scripts;
    }

    /**
     * render the reCAPTCHA widget
     *
     * data-theme: dark|light
     * data-type: audio|image
     *
     * @param array parameters.
     *
     * @return scripts
     */
    public function getWidget(array $parameters = array())      
    {
        $default = array(
            'data-sitekey' => $this->_siteKey,
            'data-theme' => 'light',
            'data-type' => 'image',
            'data-size' => 'normal',
        );

        $result = array_merge($default, $parameters);

        $html = '';
        foreach ($result as $key => $value) {
            $html .= sprintf('%s="%s" ', $key, $value);
        }

        return '<div class="g-recaptcha" '.$html.'></div>';
    }

    /**
     *  extending the reCAPTCHA widget to implement multiple forms on a single page
     *  
     *  @param array form_ids all form element IDs which has a Captcha 
     *
     *  @return scripts for multiple recaptcha elements
     * 
      */
    public function add_reCAPTCHA(array $form_ids = array())
    {        
        if (self::IS_V3)
        {
            $display_text = $this->getScriptTag(array('render' => $this->_siteKey, 'onload'=> 'recaptcha_display', 'hl' => $this->_language));
        }
        else
        {
            $display_text = $this->getScriptTag(array('render' => 'explicit', 'onload'=> 'recaptcha_display', 'hl' => $this->_language));
        }

        $display_text .= '<script>';

        $size = self::IS_INVISIBLE ? 'invisible' : 'normal';


        // captcha callback function, submit after captcha verified
        $display_text .= ' ;function gCaptchaCallback(formID) {

            $("#" + formID).attr("data-g-recaptcha-verified", true);
            $("#" + formID).off("submit");
            $("#" + formID).submit(); 
        };
        ';
        $display_text .= ' ;function recaptcha_display () { ';

        if (self::IS_V3)
        {   
            $this->_ci->load->helper('url');
            // page onload first
            $display_text .= 'grecaptcha.ready(function() {
                grecaptcha.execute("'.$this->_siteKey.'", {action: "page_'.str_replace('-', '_', uri_string()).'"});
            });';

            // 
            $display_text .= 'if ($("form").length) { 
                $.each($("form"), function(index, form) {';
                    // when on form submit, if not g-recaptcha verified then execute recaptcha, otherwise submit form
                    $display_text .= '$(form).on("submit", function(e) {
                        if ($(this).data("g-recaptcha-verified") === true) {
                            return true;
                        } else {
                            e.preventDefault();
                            $(this).find("[type=submit]").attr("disabled", "disabled");
                            var id = $(this).attr(\'id\');
                            grecaptcha.execute("'.$this->_siteKey.'", {action: "submit_" + id.replace(/-/g, \'_\')}).then(function(token){
                                $("#" + id).append("<input type=\'hidden\' name=\'g-recaptcha-response\' value=\'"+ token +"\'/>");
                                gCaptchaCallback(id);
                            });;
                        }
                    });
                });
            };';
        }

        else if (self::IS_INVISIBLE)
        {   
            foreach ($form_ids as $key => $id) {
                $display_text .= 'if (document.getElementById(\''.$id.'-recaptcha\') != null) { var widgetID = "";';
                $display_text .= ' widgetID = grecaptcha.render("'.$id.'-recaptcha", {
                        "sitekey" : "'.$this->_siteKey.'",
                        "size" : "'.$size.'",
                        "theme" : "light",
                        "badge" : "inline",
                        "callback": function() { gCaptchaCallback(\''.$id.'\') }
                    });';
                    $display_text .= '$("#'.$id.'").attr("data-g-recaptcha-widget-id", widgetID);';   

                    // when on form submit, if not g-recaptcha verified then execute recaptcha, otherwise submit form
                    $display_text .= '; $("#'.$id.'").on("submit", function(e) {
                        if ($(this).data("g-recaptcha-verified") === true) {
                            return true;
                        } else {
                            e.preventDefault();
                            grecaptcha.execute($(this).data("g-recaptcha-widget-id"));
                            $(this).find("#submitButton").attr("disabled", "disabled");
                            return;
                        }
                    })
            };
            ';
            }
        }
        else
        {
            foreach ($form_ids as $key => $id) {
                $display_text .= 'if (document.getElementById(\''.$id.'-recaptcha\') != null) { var widgetID = "";';
                $display_text .= ' widgetID = grecaptcha.render("'.$id.'-recaptcha", {
                        "sitekey" : "'.$this->_siteKey.'",
                        "size" : "'.$size.'",
                        "theme" : "light",

                    });';
                
                $display_text .= '}';    
            }
        }

        $display_text .= '    }
        </script>';
        return $display_text;
    }
}

/* End of file Recaptcha.php */
/* Location: ./application/libraries/Recaptcha.php */   