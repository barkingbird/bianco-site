<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leads_submission
{

	var $ci;
	var $data;
	var $form;
	var $field_prefix;
	var $email_config;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->data = FALSE;
		$this->form = FALSE;
		$this->field_prefix = '';

		$this->email_config = array(
			// 'smtp_port'		=> 25,
			// 'smtp_host'    	=> "103.208.188.248",
			// 'smtp_user' 	=> '',
			// 'smtp_pass' 	=> '',
			// 'protocol'		=> 'smtp',
			'protocol'      => 'mail',
			'mailtype' 		=> 'html',
			'useragent'		=> 'Barking Bird',
			'priority'		=> 3,
			'wordwrap'		=> TRUE,
			'validate'		=> FALSE,
			'crlf'          => "\r\n",
            'newline'       => "\r\n",
		);
	}

	function form_id_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

	function form_id_decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}

	public function add_lead($data = FALSE, $prefix = FALSE, $is_spam = FALSE)
	{
		if (!$data || !isset($data['form']) || !$data['form']) return FALSE;

		$this->data = $data;
		unset($this->data['form']);
		if ($is_spam)
		{
			// add spam label
			$this->data[$prefix.'is_spam'] = 'Yes';
		}

		$this->retrieve_form_info($this->form_id_decode($data['form']));
		// view(array($data['form'], $this->form_id_decode($data['form']), $this->data, $this->form));
		if ($prefix)
		{
			$this->field_prefix = $prefix;
		}
		if ($this->form && $this->create_lead())
		{
			if (!$is_spam) 
			{
				// dont sent notification if it's spam, only save it in database.
				$this->notification_email();
				$this->autoresponder_email();
			}
			return TRUE;
		}
		return FALSE;
	}

	private function retrieve_form_info($form_id = FALSE)
	{
		if (!$form_id) return FALSE;

		$this->ci->db->where('published', 1);
		$this->ci->db->where('id', $form_id);
		$query = $this->ci->db->get('forms', 1);

		$this->form = ($query->num_rows() == 1) ? $query->row() : FALSE;
	}

	private function create_lead()
	{
		$fpre = $this->field_prefix;

		$post_data = array();
		$post_data['form_id'] = $this->form->id;
		$post_data['content'] = array();
	
		foreach ($this->data as $key => $value)
		{
			if ($fpre && $value)
			{
				// if key begins with the prefix, then include
				if (strpos($key, $fpre) === 0)
				{
					$key = substr($key, strlen($fpre));
					$post_data['content'][$key] = htmlspecialchars(strip_tags($value));
				}
			}
			elseif ($value)
			{
				$post_data['content'][$key] = htmlspecialchars(strip_tags($value));
			}
		}
		
		if ($post_data['content'])
		{

			$post_data['email'] = (isset($post_data['content']['email'])) ? $post_data['content']['email'] : '';

			if (isset($post_data['content']['first_name']))
			{
				$post_data['name'] = $post_data['content']['first_name'];
			}
			if (isset($post_data['content']['last_name']))
			{
				$post_data['name'] .= ' ' . $post_data['content']['last_name'];
			}
			if (isset($post_data['content']['name']))
			{
				$post_data['name'] = $post_data['content']['name'];
			}
			if (isset($post_data['content']['is_spam']) && $post_data['content']['is_spam'] == 'Yes') 
			{
				$post_data['is_spam'] = 1; // show both in DB column and ['content'] json data column
			}
			$post_data['date_created'] = date('Y-m-d H:i:s');

			// update data for use by other functions
			$this->data = $post_data;

			$post_data['content'] = json_encode($post_data['content']);

			$this->ci->db->insert('form_leads', $post_data);

			if ($this->ci->db->insert_id())
			{
				$this->data['lead_id'] = $this->ci->db->insert_id();
				return TRUE;
			}

		}

		return FALSE;	
	}

	private function notification_email()
	{
		if ($this->form->notification_active)
		{
			$this->ci->load->library('email');
			$config = $this->email_config;

			$to_email 		= explode(',', $this->form->notification_recipients);
			$from_email 	= $this->form->notification_from_email;
			$from_name 		= $this->form->notification_from_name;
			$subject 		= $this->parse_replacements($this->form->notification_subject, 'notification');
			$message		= $this->parse_replacements($this->form->notification_message, 'notification');
			$message 		= $this->ci->load->view('admin/leads/email_template', array('message' => $message), TRUE);
			
			$this->ci->email->initialize($config);
			$this->ci->email->to($to_email);
			$this->ci->email->from($from_email, $from_name);
			$this->ci->email->subject($subject);
			$this->ci->email->message($message);

			if(@$this->form->attachments)
			{
				$attachments = json_decode($this->form->attachments, true);
				foreach ($attachments as $key => $attachment)
				{
					$this->ci->email->attach(ROOT.$attachment['file']);
				}
			}
			
			//view($this->ci->email);
			
			if($this->ci->email->send())
			{
				// update sent time
				$update_data = array(
					'notification_sent' => date('Y-m-d H:i:s'),
				);
				$this->ci->db->where('id', $this->data['lead_id']);
				$this->ci->db->update('form_leads', $update_data);

				return TRUE;
			}
			else
			{
				//return FALSE;
				$this->ci->email->print_debugger();
				die;
			}

		}
		return FALSE;
	}

	private function autoresponder_email()
	{
		if ($this->form->responder_active)
		{
			$this->ci->load->library('email');
			$config = $this->email_config;

			$to_email 		= $this->data['email'];
			$from_email 	= $this->form->responder_from_email;
			$from_name 		= $this->form->responder_from_name;
			$subject 		= $this->parse_replacements($this->form->responder_subject, 'autoresponder');
			$message		= $this->parse_replacements($this->form->responder_message, 'autoresponder');
			$message 		= $this->ci->load->view('admin/leads/email_template', array('message' => $message), TRUE);
			
			$this->ci->email->initialize($config);
			$this->ci->email->to($to_email);
			$this->ci->email->from($from_email, $from_name);
			$this->ci->email->subject($subject);
			$this->ci->email->message($message);
			
			if(@$this->form->attachments)
			{
				$attachments = json_decode($this->form->attachments, true);
				foreach ($attachments as $key => $attachment)
				{
					$this->ci->email->attach(ROOT.$attachment['file']);
				}
			}
			// view($this->ci->email);

			if($this->ci->email->send())
			{
				// update sent time
				$update_data = array(
					'responder_sent' => date('Y-m-d H:i:s'),
				);
				$this->ci->db->where('id', $this->data['lead_id']);
				$this->ci->db->update('form_leads', $update_data);

				return TRUE;
			}
			else
			{
				//return FALSE;
				$this->ci->email->print_debugger();
				die;
			}

		}
		return FALSE;
	}

	private function parse_replacements($string = FALSE, $type = FALSE)
	{
		if (strpos($string, '{') !== FALSE)
		{
			foreach ($this->data['content'] as $key => $value) 
			{
				$string = str_replace('{'.$key.'}', $value, $string);
			}

			$string = str_replace('{lead_date}', $this->data['date_created'], $string);

			if ($type == 'notification')
			{
				$string = str_replace('{subject}', $this->form->notification_subject, $string);
			}

			if ($type == 'autoresponder')
			{
				$string = str_replace('{subject}', $this->form->responder_subject, $string);
			}
		}

		if (strpos($string, '{all}') !== FALSE)
		{
			$html = 'Received on '. date('l j F Y, g:i:sa', strtotime($this->data['date_created']));
			$html .= '<br><br>';

			foreach ($this->data['content'] as $key => $value) 
			{
				$html .= '<strong>' . str_replace('_', ' ', ucwords($key)) . '</strong>';
				$html .= '<br>';
				$html .= $value;
				$html .= '<br><br>';
			}

			$string = str_replace('{all}', $html, $string);
		}

		return $string;
	}


}