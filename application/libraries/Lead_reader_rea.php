<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Lead_reader_rea extends Lead_reader
{
	var $message;
	var $order;
	function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->ci->load->model('admin/lead_readers');
	}

	function process_lead()
	{
		$this->message 							= str_replace(array(PHP_EOL, "\r", "\t"), '', $this->ci->lead_reader->message);
		$this->lead 							= array();
		
		$this->lead['form_id'] = 1001;
		$this->lead['email'] = $this->find_email();
		$name = $this->find_name();
		$surname = $this->find_surname();
		$this->lead['name'] = $name.' '.$surname;
		$phone = $this->find_phone();
		$about_me = $this->find_about();
		$comments = $this->find_comments();
		$this->lead['content'] = json_encode(array(
			"first_name" => $name,
			"last_name" => $surname,
			"email" => $this->lead['email'],
			"phone" => $phone,
			"about_me" => $about_me,
			"comments" => $comments
		));
		$this->lead['published'] = 1;
		$this->lead['date_created'] = date('Y-m-d H:i:s');
		$this->lead['deleted'] = 0;
		$this->lead['lead_origin'] = 'REA';

		return $this->lead;
	}

	private function find_email()
	{
		$startDelimiter = '<p><strong>Email:</strong> ';
		$endDelimiter 	= '</p>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
			$text 	= str_replace(' ', '', $text);
		}
		else
		{
			$text = '';
		}
		
		return $text;
	}

	private function find_name()
	{
		$startDelimiter = '<p><strong>Name:</strong> ';
		$endDelimiter 	= '</p>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
			$text 	= str_replace(' ', '', $text);
		}
		else
		{
			$text = '';
		}
		

		return $text;
	}

	private function find_surname()
	{
		$startDelimiter = '<p><strong>Surname:</strong> ';
		$endDelimiter 	= '</p>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
			$text 	= str_replace(' ', '', $text);
		}
		else
		{
			$text = '';
		}

		return $text;
	}

	private function find_phone()
	{
		$startDelimiter = '<p><strong>Phone:</strong> ';
		$endDelimiter 	= '</p>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
			$text 	= str_replace(' ', '', $text);
		}
		else
		{
			$text = '';
		}
		
		return $text;
	}

	private function find_about()
	{
		$startDelimiter = '<p><strong>About me:</strong> ';
		$endDelimiter 	= '</p>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
		}
		else
		{
			$text = '';
		}
		
		return $text;
	}

	private function find_comments()
	{
		$startDelimiter = '<p><strong>Comments:</strong><br/> ';
		$endDelimiter 	= '</p>';

		if(stripos($this->message, $startDelimiter))
		{
			$text = between_text($this->message, $startDelimiter, $endDelimiter);
		}
		else
		{
			$text = '';
		}

		return $text;
	}
}