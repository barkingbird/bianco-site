# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.26)
# Database: wdb
# Generation Time: 2020-05-07 23:49:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_groups`;

CREATE TABLE `admin_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0' COMMENT 'owner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL COMMENT '0=inactive, 1=active',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `admin_groups` WRITE;
/*!40000 ALTER TABLE `admin_groups` DISABLE KEYS */;

INSERT INTO `admin_groups` (`id`, `parent_id`, `name`, `status`, `date_created`)
VALUES
	(1,0,'Master Admin',1,'0000-00-00 00:00:00'),
	(2,0,'User',1,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `admin_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '2',
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `username` varchar(255) NOT NULL DEFAULT '',
  `contact_email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `notes` text NOT NULL,
  `bio` text,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT '9999',
  `hash` varchar(255) DEFAULT NULL,
  `hash_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;

INSERT INTO `admin_users` (`id`, `group_id`, `owner_id`, `name`, `pass`, `date_added`, `username`, `contact_email`, `phone`, `notes`, `bio`, `title`, `image`, `display_order`, `hash`, `hash_date`)
VALUES
	(1,1,0,'tech@barkingbird.com.au','1452af4a3651a77ef660ce0a168b179c475d672e','2018-12-03 12:12:30','tech@barkingbird.com.au','tech@barkingbird.com.au','9514 3112','','','',NULL,9999,'','0000-00-00 00:00:00'),
	(2,1,0,'Support','73f11ff31ba5c144e0c06ce607ac907f388d7152','2017-10-24 12:35:35','support@barkingbird.com.au','support@barkingbird.com.au',NULL,'',NULL,NULL,NULL,9999,NULL,NULL),
	(3,2,0,'Suissa','f71adffb9d25db7569a84ac831560692fdecfe68','2020-04-23 09:56:09','slo','slo@urbanangles.com',NULL,'',NULL,NULL,NULL,9999,NULL,NULL);

/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table blog_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_categories`;

CREATE TABLE `blog_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `permalink` varchar(255) NOT NULL DEFAULT '' COMMENT 'If left blank, the permalink will automatically be created for you.',
  `published` int(1) NOT NULL DEFAULT '1',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `display_order` int(2) DEFAULT '99',
  `safe_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permalink` (`permalink`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_images`;

CREATE TABLE `blog_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `main` varchar(255) NOT NULL DEFAULT '',
  `impressions` int(11) DEFAULT '0',
  `clicks` int(11) DEFAULT '0',
  `display_order` int(4) DEFAULT '9999',
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_posts`;

CREATE TABLE `blog_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `safe_name` varchar(255) DEFAULT NULL,
  `permalink` varchar(255) NOT NULL DEFAULT '' COMMENT 'This is the last part of the url string. If left blank, the permalink will automatically be created for you.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `excerpt` text NOT NULL COMMENT 'A condensed version of the content',
  `hero` varchar(255) NOT NULL DEFAULT '',
  `author_id` int(10) unsigned NOT NULL COMMENT 'If left blank, you will assumed be the author.',
  `allow_comments` int(1) unsigned DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `published` int(1) unsigned NOT NULL DEFAULT '0',
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `date_published` timestamp NULL DEFAULT NULL,
  `video` varchar(255) NOT NULL,
  `featured` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permalink` (`permalink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_posts_to_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_posts_to_categories`;

CREATE TABLE `blog_posts_to_categories` (
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `ip_address` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `user_agent` varchar(120) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table footer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `footer`;

CREATE TABLE `footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `columns` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `footer` WRITE;
/*!40000 ALTER TABLE `footer` DISABLE KEYS */;

INSERT INTO `footer` (`id`, `columns`)
VALUES
	(1,5);

/*!40000 ALTER TABLE `footer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table footer_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `footer_links`;

CREATE TABLE `footer_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display_order` int(2) NOT NULL,
  `column` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`,`column`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table form_leads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `form_leads`;

CREATE TABLE `form_leads` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) unsigned NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NULL DEFAULT NULL,
  `notification_sent` timestamp NULL DEFAULT NULL,
  `responder_sent` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `is_spam` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `form_leads` WRITE;
/*!40000 ALTER TABLE `form_leads` DISABLE KEYS */;

INSERT INTO `form_leads` (`id`, `form_id`, `email`, `name`, `content`, `published`, `date_created`, `notification_sent`, `responder_sent`, `deleted`, `is_spam`)
VALUES
	(5,5,'slo@urbanangles.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Walk-in\",\"message\":\"test only\"}',1,'2020-05-07 10:34:40',NULL,NULL,1,0),
	(3,5,'slo@urbanangles.com','Suissa','{\"lead_source\":\"Web\",\"first_name\":\"Suissa\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Walk-in\",\"message\":\"test\"}',1,'2020-05-07 10:30:35',NULL,NULL,1,0),
	(4,5,'slo@urbanangles.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Walk-in\",\"message\":\"test only\"}',1,'2020-05-07 10:31:17',NULL,NULL,1,0),
	(6,5,'slo@urbanangles.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Walk-in\",\"message\":\"test only\"}',1,'2020-05-07 10:34:52',NULL,NULL,1,0),
	(7,5,'slo@urbanangles.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Walk-in\",\"message\":\"test only\"}',1,'2020-05-07 10:35:57',NULL,NULL,1,0),
	(8,5,'slo@urbanangles.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Walk-in\",\"message\":\"test only\"}',1,'2020-05-07 11:35:31',NULL,NULL,1,0),
	(9,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 11:36:26',NULL,NULL,0,0),
	(10,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 11:37:03',NULL,NULL,0,0),
	(11,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 11:38:33',NULL,NULL,0,0),
	(12,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 11:38:58',NULL,NULL,0,0),
	(13,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 11:48:46',NULL,NULL,0,0),
	(14,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 11:49:52',NULL,NULL,0,0),
	(15,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 12:16:33',NULL,NULL,0,0),
	(16,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 12:17:25',NULL,NULL,0,0),
	(17,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 12:18:36',NULL,NULL,0,0),
	(18,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 12:24:30',NULL,NULL,0,0),
	(19,5,'suissa@startsmartsourcing.com','Suissa 2','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 2\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"test test\"}',1,'2020-05-07 12:25:18',NULL,NULL,0,0),
	(20,5,'suissa@startsmartsourcing.com','Suissa 3','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 3\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"text text\"}',1,'2020-05-07 12:27:08',NULL,NULL,0,0),
	(21,5,'suissa@startsmartsourcing.com','Suissa 3','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 3\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"text text\"}',1,'2020-05-07 12:42:25',NULL,NULL,0,0),
	(22,5,'slo@urbanangles.com','Suissa 3','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 3\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Word of month\",\"message\":\"dlsakndsaf\"}',1,'2020-05-07 16:11:47','2020-05-07 16:11:47',NULL,0,0),
	(23,5,'slo@urbanangles.com','Suissa 3','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 3\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Word of month\",\"message\":\"dlsakndsaf\"}',1,'2020-05-07 16:14:05','2020-05-07 16:14:05',NULL,0,0),
	(24,5,'slo@urbanangles.com','Suissa 3','{\"lead_source\":\"Web\",\"first_name\":\"Suissa 3\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Word of month\",\"message\":\"dlsakndsaf\"}',1,'2020-05-07 16:14:12','2020-05-07 16:14:12',NULL,0,0),
	(25,5,'suissa@startsmartsourcing.com','cczxc','{\"lead_source\":\"Web\",\"first_name\":\"cczxc\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Signage\",\"message\":\"sanocnasdc\"}',1,'2020-05-07 16:14:30','2020-05-07 16:14:30',NULL,0,0),
	(26,5,'suissa@startsmartsourcing.com','cczxc','{\"lead_source\":\"Web\",\"first_name\":\"cczxc\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Signage\",\"message\":\"sanocnasdc\"}',1,'2020-05-07 16:16:13','2020-05-07 16:16:13',NULL,0,0),
	(27,5,'suissa@startsmartsourcing.com','cczxc','{\"lead_source\":\"Web\",\"first_name\":\"cczxc\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"sadascfasf\",\"is_spam\":\"Yes\"}',1,'2020-05-07 16:16:25',NULL,NULL,0,1),
	(28,5,'suissa@startsmartsourcing.com','cczxc','{\"lead_source\":\"Web\",\"first_name\":\"cczxc\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Walk-in\",\"message\":\"sddasdas\",\"is_spam\":\"Yes\"}',1,'2020-05-07 16:17:53',NULL,NULL,0,1),
	(29,5,'suissa@startsmartsourcing.com','tttttt','{\"lead_source\":\"Web\",\"first_name\":\"tttttt\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Signage\",\"message\":\"sdasdasd34343434\",\"is_spam\":\"Yes\"}',1,'2020-05-07 16:19:18',NULL,NULL,0,1),
	(30,5,'suissa@startsmartsourcing.com','tttttt','{\"lead_source\":\"Web\",\"first_name\":\"tttttt\",\"phone\":\"09984297271\",\"email\":\"suissa@startsmartsourcing.com\",\"source\":\"Signage\",\"message\":\"sdasdasd34343434\",\"is_spam\":\"Yes\"}',1,'2020-05-07 16:21:14',NULL,NULL,0,1),
	(31,5,'slo@urbanangles.com','rrrrr','{\"lead_source\":\"Web\",\"first_name\":\"rrrrr\",\"phone\":\"09984297271\",\"email\":\"slo@urbanangles.com\",\"source\":\"Signage\",\"message\":\"dsadsadsadsadsa\",\"is_spam\":\"Yes\"}',1,'2020-05-07 16:21:32',NULL,NULL,0,1);

/*!40000 ALTER TABLE `form_leads` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `responder_active` tinyint(1) DEFAULT '0',
  `responder_from_name` varchar(255) DEFAULT NULL,
  `responder_from_email` varchar(255) DEFAULT NULL,
  `responder_subject` varchar(255) DEFAULT NULL,
  `responder_message` text,
  `notification_active` tinyint(1) DEFAULT '0',
  `notification_from_name` varchar(255) DEFAULT NULL,
  `notification_from_email` varchar(255) DEFAULT NULL,
  `notification_recipients` text,
  `notification_subject` varchar(255) DEFAULT NULL,
  `notification_message` text,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `forms` WRITE;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;

INSERT INTO `forms` (`id`, `name`, `responder_active`, `responder_from_name`, `responder_from_email`, `responder_subject`, `responder_message`, `notification_active`, `notification_from_name`, `notification_from_email`, `notification_recipients`, `notification_subject`, `notification_message`, `published`, `date_created`, `date_modified`)
VALUES
	(5,'contact',0,'','','','',1,'Sample','no-reply@urbanangles.com','slo@urbanangles.com','Online Enquiry','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\r\n<style type=\"text/css\">table { margin: auto; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }\r\n	table tr { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }\r\n	table td { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }\r\n	img { border:none; outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }\r\n	#outlook a { padding: 0; }\r\n	html { margin: 0px; padding: 0px; } \r\n	body { \r\n		width: 100% !important; -webkit-text-size-adjust: none; -ms-text-size-adjust: 100%; margin: 0; padding: 0;\r\n		-webkit-font-smoothing: antialiased;\r\n		-moz-font-smoothing: antialiased;\r\n		-moz-osx-font-smoothing: grayscale;\r\n		font-smoothing: antialiased; \r\n	} \r\n	.ExternalClass { width:100%; }\r\n	/* stop iphone resizing fonts */\r\n	div, p, a, li, td { -webkit-text-size-adjust: none; }\r\n</style>\r\n<!-- BODY TABLE -->\r\n<table bgcolor=\"#e5e5e5\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"backgroundTable\" style=\"background:#e5e5e5 none; border-collapse:collapse; color:#222222; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td height=\"20\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td align=\"center\" valign=\"top\"><!-- BODY TABLE --><!-- CONTENT TABLE -->\r\n			<table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-collapse:collapse; color:#222222; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:left; width:600px\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"center\" style=\"font-size: 0px; line-height: 0px; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\" valign=\"top\"><!-- CONTENT TABLE --><!-- HEADING -->\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; color:#222222; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:24px; font-weight:bold; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:left; width:560px\">\r\n							<tbody>\r\n								<tr>\r\n									<td height=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td>\r\n									<h1 style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: bold; font-size: 24px; line-height: 1.25; color: #222222; text-align: left; margin: 0px; padding: 0px;\">You have received a new email enquiry from your {subject}</h1>\r\n									</td>\r\n								</tr>\r\n								<tr>\r\n									<td height=\"20\">&nbsp;</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- HEADING -->\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; color:#222222; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:left; width:560px\">\r\n							<tbody>\r\n								<tr>\r\n									<td height=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\"left\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.25; color: #222222; vertical-align: top; text-align: left;\" valign=\"top\">{all}</td>\r\n								</tr>\r\n								<tr>\r\n									<td height=\"40\">&nbsp;</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- FOOTER -->\r\n\r\n						<table bgcolor=\"#333333\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#333333; border-collapse:collapse; color:#ffffff; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:12px; font-weight:normal; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:left; width:600px\">\r\n							<tbody>\r\n								<tr>\r\n									<td colspan=\"3\" height=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n									<td><a href=\"http://www.barkingbird.com.au\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: bold; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; border: 0; outline: 0;\" target=\"_blank\"><img alt=\"Barking Bird\" border=\"0\" src=\"http://assets.barkingbird.com.au/media/autoresponders/barkingbird/leads/barking-bird-logo-white.png\" style=\"height:20px\" /> </a></td>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td colspan=\"3\" height=\"10\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n									<td><strong>Powered by Barking Bird</strong><br />\r\n									Ground Floor, 10 Grattan St, Prahran, VIC 3181, Australia</td>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td colspan=\"3\" height=\"10\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n									<td><strong>P</strong>&nbsp; <a href=\"tel:1300660177\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; \">1300 660 177</a><br />\r\n									<strong>E</strong>&nbsp;&nbsp; <a href=\"mailto:peck@barkingbird.com.au\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; \">peck@barkingbird.com.au</a><br />\r\n									<strong>W</strong>&nbsp; <a href=\"http://www.barkingbird.com.au\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; \" target=\"_blank\">www.barkingbird.com.au</a></td>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td colspan=\"3\" height=\"20\">&nbsp;</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- FOOTER --><!-- CONTENT TABLE --></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<!-- CONTENT TABLE --><!-- BODY TABLE --></td>\r\n		</tr>\r\n		<tr>\r\n			<td height=\"20\">&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<!-- BODY TABLE -->',1,'2020-05-07 08:26:39','2020-05-07 14:18:47');

/*!40000 ALTER TABLE `forms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `display_order` int(4) DEFAULT '9999',
  `arrows` tinyint(1) DEFAULT NULL,
  `thumbs` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(4) DEFAULT '1',
  `hover` tinyint(1) DEFAULT NULL,
  `bg_colour` varchar(7) DEFAULT NULL,
  `navigation` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `meta_keywords` (`meta_keywords`,`meta_description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;

INSERT INTO `gallery` (`id`, `title`, `content`, `display_order`, `arrows`, `thumbs`, `status`, `hover`, `bg_colour`, `navigation`, `meta_keywords`, `meta_description`)
VALUES
	(1,'Sample Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(2,'Home Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(3,'Projects Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(4,'Services Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(5,'About Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(6,'Contact Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(7,'Hilcrest Drive Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(8,'RIVERDALE VILLAGE Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(9,'Coming soon Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL),
	(10,'Deanside Village Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery_images`;

CREATE TABLE `gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `width` varchar(100) DEFAULT NULL,
  `height` varchar(100) DEFAULT NULL,
  `image` text,
  `thumb` varchar(255) DEFAULT '',
  `main` varchar(255) DEFAULT '',
  `impressions` int(11) DEFAULT '0',
  `clicks` int(11) DEFAULT '0',
  `caption` text,
  `caption_position` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT '',
  `display_order` int(4) DEFAULT '9999',
  `image_hover` varchar(255) DEFAULT NULL,
  `webm` varchar(255) DEFAULT NULL,
  `mp4` varchar(255) DEFAULT NULL,
  `link_text` varchar(255) DEFAULT NULL,
  `artists_impression` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `gallery_images` WRITE;
/*!40000 ALTER TABLE `gallery_images` DISABLE KEYS */;

INSERT INTO `gallery_images` (`id`, `gallery_id`, `title`, `description`, `width`, `height`, `image`, `thumb`, `main`, `impressions`, `clicks`, `caption`, `caption_position`, `link`, `display_order`, `image_hover`, `webm`, `mp4`, `link_text`, `artists_impression`)
VALUES
	(2,2,'Building Quality<br>Australian Homes','',NULL,NULL,'assets/media/gallery/home/elora-hero.jpg','assets/media/gallery/home/elora-hero.jpg','assets/media/gallery/home/elora-hero_main.jpg',0,0,NULL,'middle-left','/about',9999,NULL,NULL,NULL,'Learn more about WDB','no'),
	(3,4,'Services','',NULL,NULL,'assets/media/gallery/services/cg-002-southrd-010.jpg','assets/media/gallery/services/cg-002-southrd-010.jpg','assets/media/gallery/services/cg-002-southrd-010_main.jpg',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no'),
	(4,6,'Contact','',NULL,NULL,'assets/media/gallery/contact/contact.jpg','assets/media/gallery/contact/cg-002-southrd-010@2x.png','assets/media/gallery/contact/cg-002-southrd-010@2x_main.png',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no'),
	(5,5,'About','',NULL,NULL,'assets/media/gallery/about/about.jpg','assets/media/gallery/about/about.jpg','assets/media/gallery/about/about_main.jpg',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no'),
	(6,3,'','',NULL,NULL,'assets/media/gallery/projects/31434966252_8e1bc946da_b_(1).jpg','assets/media/gallery/projects/31434966252_8e1bc946da_b_(1).jpg','assets/media/gallery/projects/31434966252_8e1bc946da_b_(1)_main.jpg',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no'),
	(7,2,'Building Quality<br>Australian Homes','',NULL,NULL,'assets/media/gallery/home/cg-002-southrd-010-copy-3@2x.jpg','assets/media/gallery/home/cg-002-southrd-010-copy-3@2x.jpg','assets/media/gallery/home/cg-002-southrd-010-copy-3@2x_main.jpg',0,0,NULL,'middle-left','/about',9999,NULL,NULL,NULL,'Learn more about WDB','no'),
	(8,2,'Building Quality<br>Australian Homes','',NULL,NULL,'assets/media/gallery/home/cg-002-southrd-010-copy@2x.jpg','assets/media/gallery/home/cg-002-southrd-010-copy@2x.jpg','assets/media/gallery/home/cg-002-southrd-010-copy@2x_main.jpg',0,0,NULL,'middle-left','/about',9999,NULL,NULL,NULL,'Learn more about WDB','no'),
	(9,7,'','',NULL,NULL,'assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy@2x.jpg','assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy@2x.jpg','assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy@2x_main.jpg',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no'),
	(10,7,'','',NULL,NULL,'assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy-3@2x.jpg','assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy-3@2x.jpg','assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy-3@2x_main.jpg',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no'),
	(11,7,'','',NULL,NULL,'assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy-3@2x1.jpg','assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy-3@2x1.jpg','assets/media/gallery/hilcrest-drive/cg-002-southrd-010-copy-3@2x1_main.jpg',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no'),
	(12,8,'','',NULL,NULL,'assets/media/gallery/riverdale-village/cg-002-southrd-010-copy@2x.jpg','assets/media/gallery/riverdale-village/cg-002-southrd-010-copy@2x.jpg','assets/media/gallery/riverdale-village/cg-002-southrd-010-copy@2x_main.jpg',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no'),
	(13,10,NULL,NULL,NULL,NULL,'assets/media/gallery/deanside-village/cg-002-southrd-010-copy@2x.jpg','assets/media/gallery/deanside-village/cg-002-southrd-010-copy@2x.jpg','assets/media/gallery/deanside-village/cg-002-southrd-010-copy@2x_main.jpg',0,0,NULL,NULL,'',9999,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `gallery_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table geocoding
# ------------------------------------------------------------

DROP TABLE IF EXISTS `geocoding`;

CREATE TABLE `geocoding` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL,
  `display` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(100) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0',
  `theme` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub_heading` varchar(255) NOT NULL DEFAULT '',
  `detail` text,
  `sidebar` text,
  `permalink` varchar(255) DEFAULT NULL,
  `navigation` varchar(225) DEFAULT '0',
  `template` varchar(255) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `display_order` int(4) DEFAULT '9999',
  `image` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `nav_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nav_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nav_position` int(11) NOT NULL,
  `footer` int(1) DEFAULT NULL,
  `enable_link` int(1) DEFAULT '1',
  `contact_form` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'the url of the contact form to use',
  `contact_embed` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`),
  KEY `enable_link` (`enable_link`),
  FULLTEXT KEY `detail` (`detail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `parent_id`, `display`, `timestamp`, `user`, `status`, `theme`, `title`, `sub_heading`, `detail`, `sidebar`, `permalink`, `navigation`, `template`, `meta_title`, `meta_keywords`, `meta_description`, `display_order`, `image`, `gallery_id`, `nav_icon`, `nav_title`, `nav_position`, `footer`, `enable_link`, `contact_form`, `contact_embed`)
VALUES
	(1,-1,0,'2020-04-23 09:49:48',NULL,'1',NULL,'Sample','Sample only','',NULL,'sample/','0','default','',NULL,'',1,NULL,1,NULL,'',0,0,1,'',0),
	(2,0,0,'2020-04-23 09:56:50',NULL,'1',NULL,'Home','','<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>',NULL,'home/','0','home','',NULL,'',2,NULL,2,NULL,'',0,0,1,'',0),
	(3,0,0,'2020-04-23 09:57:35',NULL,'1',NULL,'Projects','','',NULL,'projects/','0','projects','',NULL,'',3,NULL,3,NULL,'',0,0,1,'',0),
	(4,0,0,'2020-04-23 09:57:43',NULL,'1',NULL,'Services','','',NULL,'services/','0','default','',NULL,'',4,NULL,4,NULL,'',0,0,1,'',0),
	(5,0,0,'2020-04-23 09:57:52',NULL,'1',NULL,'About','This is the H1 Headline','<p>sdadafdasfasf f ff</p>',NULL,'about/','0','default','',NULL,'',5,NULL,5,NULL,'',0,0,1,'',0),
	(6,0,0,'2020-04-23 09:57:58',NULL,'1',NULL,'Contact','Enquire here','<p><strong>&ldquo;Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.&ldquo;</strong></p>',NULL,'contact/','0','contact','',NULL,'',6,NULL,6,NULL,'',0,0,1,'',0);

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_categories`;

CREATE TABLE `project_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `safe_name` varchar(255) DEFAULT NULL,
  `display_order` int(11) NOT NULL DEFAULT '999',
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table project_categories_relation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_categories_relation`;

CREATE TABLE `project_categories_relation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `display` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `detail` text,
  `permalink` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `display_order` int(4) DEFAULT '9999',
  `hero_image` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '1',
  `address` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `budget` varchar(255) DEFAULT NULL,
  `commenced` varchar(255) DEFAULT NULL,
  `completed` varchar(255) DEFAULT NULL,
  `architect` varchar(255) DEFAULT NULL,
  `interiors` varchar(255) DEFAULT NULL,
  `landscaper` varchar(255) DEFAULT NULL,
  `safe_name` varchar(255) DEFAULT NULL,
  `intro` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `testimonial` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`),
  FULLTEXT KEY `detail` (`detail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`id`, `display`, `timestamp`, `status`, `title`, `subtitle`, `detail`, `permalink`, `meta_title`, `meta_keywords`, `meta_description`, `display_order`, `hero_image`, `gallery_id`, `width`, `address`, `type`, `budget`, `commenced`, `completed`, `architect`, `interiors`, `landscaper`, `safe_name`, `intro`, `author`, `testimonial`)
VALUES
	(1,0,'2020-04-28 07:12:15','1','Hilcrest Drive','This is H1 Headline','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</p>','hilcrest-drive/','',NULL,'',1,'assets/media/hilcrest-drive/cg-002-southrd-010-copy@2x.jpg',7,1,'11 & 15 Hillcrest Drive, West Meadows, VIC 3049','Single detached','$1500','12/10/2019','3/10/2020','Lucio Tan','Jessie Jems','Ambre Lim','hilcrest-drive','													West Meadows','','															'),
	(2,0,'2020-04-29 08:33:11','1','RIVERDALE VILLAGE','','','riverdale-village/','',NULL,'',2,'assets/media/riverdale-village/cg-002-southrd-010-copy@2x.jpg',8,1,'','','','','','','','','riverdale-village','					TArneit										','','															'),
	(3,0,'2020-04-29 09:48:17','1','Coming soon','COMING SOON','<p><span style=\"background-color:rgb(139, 137, 131); color:rgb(255, 255, 255); font-family:din-2014,sans-serif; font-size:19.2px\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit.&nbsp;</span></p>','coming-soon/','',NULL,'',3,NULL,9,1,'11 & 15 July STREET VIC 3049','','','','','','','','coming-soon','															TARNEIT','','															'),
	(4,0,'2020-04-29 09:49:18','1','Deanside Village','','','deanside-village/','',NULL,'',4,'assets/media/deanside-village/metricon-chelsea-33-ellendale-externals-0011@2x.jpg',10,1,'','Single detached','$1500','12/10/2019','3/10/2020','Lucio Tan','Jessie Jems','Ambre Lim','deanside-village','															Rockbank','','															');

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table redirects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redirects`;

CREATE TABLE `redirects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `header` varchar(10) DEFAULT '301',
  PRIMARY KEY (`id`),
  UNIQUE KEY `from` (`from`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table section_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `section_items`;

CREATE TABLE `section_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(11) unsigned NOT NULL,
  `content` text,
  `display_order` int(4) unsigned NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `section_items` WRITE;
/*!40000 ALTER TABLE `section_items` DISABLE KEYS */;

INSERT INTO `section_items` (`id`, `section_id`, `content`, `display_order`)
VALUES
	(18,5,'{\"image\":\"assets\\/media\\/sections\\/NQ\\/640x360.png\",\"link\":\"\\/projects\",\"linkname\":\"See Project\"}',9999),
	(19,5,'{\"image\":\"assets\\/media\\/sections\\/NQ\\/640x3601.png\",\"link\":\"\\/projects\",\"linkname\":\"See Project\"}',9999),
	(37,18,'{\"title\":\"NEW HOME CONSTRUCTION\",\"subheading\":\"\\u201cMorbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.\\u201c\",\"description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"image\":\"assets\\/media\\/sections\\/MTg\\/cg-002-southrd-010-copy@2x.jpg\"}',9999),
	(38,18,'{\"title\":\"KNOCK-DOWN REBUILD\",\"subheading\":\"\\u201cMorbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.\\u201c\",\"description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"image\":\"assets\\/media\\/sections\\/MTg\\/metricon-chelsea-33-ellendale-externals-0011@2x.jpg\"}',9999),
	(39,18,'{\"title\":\"COMPLETE COMMUNITIES\",\"subheading\":\"\\u201cMorbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.\\u201c\",\"description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"image\":\"assets\\/media\\/sections\\/MTg\\/cg-002-southrd-010-copy-3@2x.jpg\"}',9999),
	(40,19,'{\"title\":\"ABOUT WBD\",\"subheading\":\"Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"image\":\"assets\\/media\\/sections\\/MTk\\/cg-002-southrd-010-copy@2x.jpg\"}',9999),
	(41,19,'{\"title\":\"Our PEOPLE\",\"subheading\":\"Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"image\":\"assets\\/media\\/sections\\/MTk\\/cg-002-southrd-010-copy-2@2x.jpg\"}',9999),
	(42,19,'{\"title\":\"Our quality guarantee\",\"subheading\":\"Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \",\"image\":\"assets\\/media\\/sections\\/MTk\\/cg-002-southrd-010-copy-3@2x.jpg\"}',9999);

/*!40000 ALTER TABLE `section_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `content` text,
  `display_order` int(4) unsigned NOT NULL DEFAULT '9999',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;

INSERT INTO `sections` (`id`, `page_id`, `type`, `content`, `display_order`, `published`)
VALUES
	(5,2,'image-blocks','{\"full_width\":\"yes\",\"title\":\"This is the H1<br> Headline\",\"description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.\"}',9999,1),
	(18,4,'image_text','{\"full_width\":\"yes\",\"caption\":\"\\u201cMorbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus vulputate diam eu pretium. Lorem ipsum dolor sit amet.\\u201c\"}',9999,1),
	(19,5,'image_text','{\"full_width\":\"yes\",\"caption\":\"Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean lacinia bibendum nulla sed consecteturLorem ipsum dolor sit amet, consectetur adipiscing elit. \"}',9999,1);

/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table site_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `site_settings`;

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pritty name',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'text',
  PRIMARY KEY (`id`),
  KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `site_settings` WRITE;
/*!40000 ALTER TABLE `site_settings` DISABLE KEYS */;

INSERT INTO `site_settings` (`id`, `key`, `value`, `title`, `type`)
VALUES
	(1,'logo','assets/media/documents/bb.png','Admin Logo','file'),
	(2,'address_1','XX Address St Suburb VIC T ','Address Line 1','text'),
	(3,'address_2','','Address Line 2','text'),
	(4,'address_3','','Address Line 3','text'),
	(5,'lat_long','','Address Latitude / Longitude (-37.848400, 144.991135)','text'),
	(6,'contact_phone','+61 3 0000 0000','Contact Phone','text'),
	(7,'contact_email','slo@urbanangles.com','Contact Email','text'),
	(8,'front_end_logo','assets/media/documents/wbd-logo-reversed.svg','Front End Logo','file'),
	(9,'front_end_logo_scroll',NULL,'Front End Logo on Scroll','file');

/*!40000 ALTER TABLE `site_settings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
