# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.26)
# Database: bianco
# Generation Time: 2020-07-14 22:10:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_groups`;

CREATE TABLE `admin_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0' COMMENT 'owner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL COMMENT '0=inactive, 1=active',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `admin_groups` WRITE;
/*!40000 ALTER TABLE `admin_groups` DISABLE KEYS */;

INSERT INTO `admin_groups` (`id`, `parent_id`, `name`, `status`, `date_created`)
VALUES
	(1,0,'Master Admin',1,'0000-00-00 00:00:00'),
	(2,0,'User',1,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `admin_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '2',
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `username` varchar(255) NOT NULL DEFAULT '',
  `contact_email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `notes` text NOT NULL,
  `bio` text,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT '9999',
  `hash` varchar(255) DEFAULT NULL,
  `hash_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;

INSERT INTO `admin_users` (`id`, `group_id`, `owner_id`, `name`, `pass`, `date_added`, `username`, `contact_email`, `phone`, `notes`, `bio`, `title`, `image`, `display_order`, `hash`, `hash_date`)
VALUES
	(1,1,0,'tech@barkingbird.com.au','1452af4a3651a77ef660ce0a168b179c475d672e','2018-12-03 12:12:30','tech@barkingbird.com.au','tech@barkingbird.com.au','9514 3112','','','',NULL,9999,'','0000-00-00 00:00:00'),
	(2,1,0,'Support','73f11ff31ba5c144e0c06ce607ac907f388d7152','2017-10-24 12:35:35','support@barkingbird.com.au','support@barkingbird.com.au',NULL,'',NULL,NULL,NULL,9999,NULL,NULL);

/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table blog_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_categories`;

CREATE TABLE `blog_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `permalink` varchar(255) NOT NULL DEFAULT '' COMMENT 'If left blank, the permalink will automatically be created for you.',
  `published` int(1) NOT NULL DEFAULT '1',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `display_order` int(2) DEFAULT '99',
  `safe_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permalink` (`permalink`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_images`;

CREATE TABLE `blog_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `main` varchar(255) NOT NULL DEFAULT '',
  `impressions` int(11) DEFAULT '0',
  `clicks` int(11) DEFAULT '0',
  `display_order` int(4) DEFAULT '9999',
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_posts`;

CREATE TABLE `blog_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `safe_name` varchar(255) DEFAULT NULL,
  `permalink` varchar(255) NOT NULL DEFAULT '' COMMENT 'This is the last part of the url string. If left blank, the permalink will automatically be created for you.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `excerpt` text NOT NULL COMMENT 'A condensed version of the content',
  `hero` varchar(255) NOT NULL DEFAULT '',
  `author_id` int(10) unsigned NOT NULL COMMENT 'If left blank, you will assumed be the author.',
  `allow_comments` int(1) unsigned DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `published` int(1) unsigned NOT NULL DEFAULT '0',
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `date_published` timestamp NULL DEFAULT NULL,
  `video` varchar(255) NOT NULL,
  `featured` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permalink` (`permalink`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table blog_posts_to_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blog_posts_to_categories`;

CREATE TABLE `blog_posts_to_categories` (
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ci_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `ip_address` varchar(45) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `user_agent` varchar(120) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table footer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `footer`;

CREATE TABLE `footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `columns` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `footer` WRITE;
/*!40000 ALTER TABLE `footer` DISABLE KEYS */;

INSERT INTO `footer` (`id`, `columns`)
VALUES
	(1,5);

/*!40000 ALTER TABLE `footer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table footer_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `footer_links`;

CREATE TABLE `footer_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `display_order` int(2) NOT NULL,
  `column` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`,`column`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table form_leads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `form_leads`;

CREATE TABLE `form_leads` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) unsigned NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NULL DEFAULT NULL,
  `notification_sent` timestamp NULL DEFAULT NULL,
  `responder_sent` timestamp NULL DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `is_spam` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `responder_active` tinyint(1) DEFAULT '0',
  `responder_from_name` varchar(255) DEFAULT NULL,
  `responder_from_email` varchar(255) DEFAULT NULL,
  `responder_subject` varchar(255) DEFAULT NULL,
  `responder_message` text,
  `notification_active` tinyint(1) DEFAULT '0',
  `notification_from_name` varchar(255) DEFAULT NULL,
  `notification_from_email` varchar(255) DEFAULT NULL,
  `notification_recipients` text,
  `notification_subject` varchar(255) DEFAULT NULL,
  `notification_message` text,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `forms` WRITE;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;

INSERT INTO `forms` (`id`, `name`, `responder_active`, `responder_from_name`, `responder_from_email`, `responder_subject`, `responder_message`, `notification_active`, `notification_from_name`, `notification_from_email`, `notification_recipients`, `notification_subject`, `notification_message`, `published`, `date_created`, `date_modified`)
VALUES
	(1,'Register',1,'','','','',0,'','','','','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\r\n<style type=\"text/css\">table { margin: auto; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }\r\n	table tr { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }\r\n	table td { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }\r\n	img { border:none; outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }\r\n	#outlook a { padding: 0; }\r\n	html { margin: 0px; padding: 0px; } \r\n	body { \r\n		width: 100% !important; -webkit-text-size-adjust: none; -ms-text-size-adjust: 100%; margin: 0; padding: 0;\r\n		-webkit-font-smoothing: antialiased;\r\n		-moz-font-smoothing: antialiased;\r\n		-moz-osx-font-smoothing: grayscale;\r\n		font-smoothing: antialiased; \r\n	} \r\n	.ExternalClass { width:100%; }\r\n	/* stop iphone resizing fonts */\r\n	div, p, a, li, td { -webkit-text-size-adjust: none; }\r\n</style>\r\n<!-- BODY TABLE -->\r\n<table bgcolor=\"#e5e5e5\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"backgroundTable\" style=\"background:#e5e5e5 none; border-collapse:collapse; color:#222222; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td height=\"20\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td align=\"center\" valign=\"top\"><!-- BODY TABLE --><!-- CONTENT TABLE -->\r\n			<table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-collapse:collapse; color:#222222; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:left; width:600px\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"center\" style=\"font-size: 0px; line-height: 0px; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\" valign=\"top\"><!-- CONTENT TABLE --><!-- HEADING -->\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; color:#222222; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:24px; font-weight:bold; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:left; width:560px\">\r\n							<tbody>\r\n								<tr>\r\n									<td height=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td>\r\n									<h1 style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: bold; font-size: 24px; line-height: 1.25; color: #222222; text-align: left; margin: 0px; padding: 0px;\">You have received a new email enquiry from your {subject}</h1>\r\n									</td>\r\n								</tr>\r\n								<tr>\r\n									<td height=\"20\">&nbsp;</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- HEADING -->\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; color:#222222; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:left; width:560px\">\r\n							<tbody>\r\n								<tr>\r\n									<td height=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td align=\"left\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.25; color: #222222; vertical-align: top; text-align: left;\" valign=\"top\">{all}</td>\r\n								</tr>\r\n								<tr>\r\n									<td height=\"40\">&nbsp;</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- FOOTER -->\r\n\r\n						<table bgcolor=\"#333333\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#333333; border-collapse:collapse; color:#ffffff; font-family:helvetica neue,helvetica,arial,sans-serif; font-size:12px; font-weight:normal; line-height:1.25; margin:auto; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:left; width:600px\">\r\n							<tbody>\r\n								<tr>\r\n									<td colspan=\"3\" height=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n									<td><a href=\"http://www.barkingbird.com.au\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: bold; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; border: 0; outline: 0;\" target=\"_blank\"><img alt=\"Barking Bird\" border=\"0\" src=\"http://assets.barkingbird.com.au/media/autoresponders/barkingbird/leads/barking-bird-logo-white.png\" style=\"height:20px\" /> </a></td>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td colspan=\"3\" height=\"10\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n									<td><strong>Powered by Barking Bird</strong><br />\r\n									Ground Floor, 10 Grattan St, Prahran, VIC 3181, Australia</td>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td colspan=\"3\" height=\"10\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n									<td><strong>P</strong>&nbsp; <a href=\"tel:1300660177\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; \">1300 660 177</a><br />\r\n									<strong>E</strong>&nbsp;&nbsp; <a href=\"mailto:peck@barkingbird.com.au\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; \">peck@barkingbird.com.au</a><br />\r\n									<strong>W</strong>&nbsp; <a href=\"http://www.barkingbird.com.au\" style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 1.25; color: #ffffff !important; text-decoration: none !important; text-align: left; \" target=\"_blank\">www.barkingbird.com.au</a></td>\r\n									<td style=\"width: 20px;\" width=\"20\">&nbsp;</td>\r\n								</tr>\r\n								<tr>\r\n									<td colspan=\"3\" height=\"20\">&nbsp;</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						<!-- FOOTER --><!-- CONTENT TABLE --></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			<!-- CONTENT TABLE --><!-- BODY TABLE --></td>\r\n		</tr>\r\n		<tr>\r\n			<td height=\"20\">&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<!-- BODY TABLE -->',1,'2020-07-14 21:09:42','2020-07-14 21:43:41');

/*!40000 ALTER TABLE `forms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `display_order` int(4) DEFAULT '9999',
  `arrows` tinyint(1) DEFAULT NULL,
  `thumbs` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(4) DEFAULT '1',
  `hover` tinyint(1) DEFAULT NULL,
  `bg_colour` varchar(7) DEFAULT NULL,
  `navigation` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `meta_keywords` (`meta_keywords`,`meta_description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;

INSERT INTO `gallery` (`id`, `title`, `content`, `display_order`, `arrows`, `thumbs`, `status`, `hover`, `bg_colour`, `navigation`, `meta_keywords`, `meta_description`)
VALUES
	(1,'Home Gallery','',9999,NULL,1,1,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery_images`;

CREATE TABLE `gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `width` varchar(100) DEFAULT NULL,
  `height` varchar(100) DEFAULT NULL,
  `image` text,
  `thumb` varchar(255) DEFAULT '',
  `main` varchar(255) DEFAULT '',
  `impressions` int(11) DEFAULT '0',
  `clicks` int(11) DEFAULT '0',
  `caption` text,
  `caption_position` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT '',
  `display_order` int(4) DEFAULT '9999',
  `image_hover` varchar(255) DEFAULT NULL,
  `webm` varchar(255) DEFAULT NULL,
  `mp4` varchar(255) DEFAULT NULL,
  `link_text` varchar(255) DEFAULT NULL,
  `artists_impression` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `gallery_images` WRITE;
/*!40000 ALTER TABLE `gallery_images` DISABLE KEYS */;

INSERT INTO `gallery_images` (`id`, `gallery_id`, `title`, `description`, `width`, `height`, `image`, `thumb`, `main`, `impressions`, `clicks`, `caption`, `caption_position`, `link`, `display_order`, `image_hover`, `webm`, `mp4`, `link_text`, `artists_impression`, `video_url`)
VALUES
	(2,1,'The best of urban  coastal living.','A Boutique Collection of  1, 2 & 3 Bedroom Apartments.',NULL,NULL,'assets/media/gallery/home/Group_3@3x.jpg','assets/media/gallery/home/Group_3@3x.jpg','assets/media/gallery/home/Group_3@3x_main.jpg',0,0,NULL,'middle-center','',9999,NULL,NULL,NULL,'','no','0vrdgDdPApQ');

/*!40000 ALTER TABLE `gallery_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table geocoding
# ------------------------------------------------------------

DROP TABLE IF EXISTS `geocoding`;

CREATE TABLE `geocoding` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL,
  `display` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(100) DEFAULT NULL,
  `status` varchar(255) DEFAULT '0',
  `theme` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub_heading` varchar(255) NOT NULL DEFAULT '',
  `detail` text,
  `sidebar` text,
  `permalink` varchar(255) DEFAULT NULL,
  `navigation` varchar(225) DEFAULT '0',
  `template` varchar(255) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `display_order` int(4) DEFAULT '9999',
  `image` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `nav_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nav_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nav_position` int(11) NOT NULL,
  `footer` int(1) DEFAULT NULL,
  `enable_link` int(1) DEFAULT '1',
  `contact_form` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'the url of the contact form to use',
  `contact_embed` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`),
  KEY `enable_link` (`enable_link`),
  FULLTEXT KEY `detail` (`detail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `parent_id`, `display`, `timestamp`, `user`, `status`, `theme`, `title`, `sub_heading`, `detail`, `sidebar`, `permalink`, `navigation`, `template`, `meta_title`, `meta_keywords`, `meta_description`, `display_order`, `image`, `gallery_id`, `nav_icon`, `nav_title`, `nav_position`, `footer`, `enable_link`, `contact_form`, `contact_embed`)
VALUES
	(1,0,0,'2020-07-09 11:52:55',NULL,'1',NULL,'Home','The best of urban coastal living','<div><span style=\"color:rgb(67, 60, 68); font-family:helvetica neue; font-size:13px\">A Boutique Collection of</span><span style=\"color:rgb(67, 60, 68); font-family:helvetica neue; font-size:13px\">&nbsp;</span></div>\r\n\r\n<div>\r\n<p class=\"p2\" style=\"margin: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 13px; line-height: normal; font-family: &quot;Helvetica Neue&quot;; color: rgb(67, 60, 68);\">1, 2 &amp; 3 Bedroom Apartments.</p>\r\n</div>',NULL,'home/','0','default','',NULL,'',1,NULL,1,NULL,'',0,0,1,'',0);

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_categories`;

CREATE TABLE `project_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `safe_name` varchar(255) DEFAULT NULL,
  `display_order` int(11) NOT NULL DEFAULT '999',
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table project_categories_relation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_categories_relation`;

CREATE TABLE `project_categories_relation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `display` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) NOT NULL DEFAULT '',
  `detail` text,
  `permalink` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `display_order` int(4) DEFAULT '9999',
  `hero_image` varchar(255) DEFAULT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `width` int(11) NOT NULL DEFAULT '1',
  `address` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `budget` varchar(255) DEFAULT NULL,
  `commenced` varchar(255) DEFAULT NULL,
  `completed` varchar(255) DEFAULT NULL,
  `architect` varchar(255) DEFAULT NULL,
  `interiors` varchar(255) DEFAULT NULL,
  `landscaper` varchar(255) DEFAULT NULL,
  `safe_name` varchar(255) DEFAULT NULL,
  `intro` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `testimonial` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `display_order` (`display_order`),
  FULLTEXT KEY `detail` (`detail`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table redirects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `redirects`;

CREATE TABLE `redirects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) DEFAULT NULL,
  `to` varchar(255) DEFAULT NULL,
  `header` varchar(10) DEFAULT '301',
  PRIMARY KEY (`id`),
  UNIQUE KEY `from` (`from`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table section_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `section_items`;

CREATE TABLE `section_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(11) unsigned NOT NULL,
  `content` text,
  `display_order` int(4) unsigned NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `section_items` WRITE;
/*!40000 ALTER TABLE `section_items` DISABLE KEYS */;

INSERT INTO `section_items` (`id`, `section_id`, `content`, `display_order`)
VALUES
	(2,2,'{\"subheading\":\"\",\"description\":\"\",\"image\":\"assets\\/media\\/sections\\/Mg\\/Screen_Shot_2020-07-03_at_12_25_43_PM.png\",\"signature\":\"assets\\/media\\/sections\\/Mg\\/Screen_Shot_2020-07-03_at_1_00_07_PM.png\"}',9999),
	(9,5,'{\"bgcolor\":\"\",\"title\":\"The Apartments\",\"subheading\":\"Light-filled residences with private terraces \\r\\non one of Melbourne\\u2019s most iconic streets. \",\"description\":\"Responding to its location on one of Melbourne\\u2019s most iconic streets, Bianco\\u2019s light-filled apartments and private terraces are designed to merge contemporary form with an awareness of Bay Street\\u2019s vibrant cultural milieu.\\r\\n\\r\\n\",\"image\":\"assets\\/media\\/sections\\/NQ\\/Screen_Shot_2020-06-28_at_11_21_34_PM.png\"}',9999),
	(12,1,'{\"title\":\"A DRAMATIC GALLERY ENTRANCE\",\"subheading\":\"The ebb & flow creates a conscious invitation, \\r\\nwith organic contours and natural finishes. \",\"description\":\"Colums of natural light cascade through the two lightwells luminating the lobby, \\r\\nwhile ambiant lighting accentuate the terrazzo flooring and the curved timber \\r\\npanelling; all complimented by the void of lush greenery.    \",\"image\":\"assets\\/media\\/sections\\/MQ\\/Screen_Shot_2020-07-01_at_7_18_20_AM1.png\",\"position\":\"center\",\"size\":\"full\"}',9999),
	(13,1,'{\"title\":\"Sophisticated kitchen & dining\",\"subheading\":\"Inspired by the contours of coastal elements\",\"description\":\"Functionality takes centre stage in spaces designed to prepare a casual\\r\\nmeal or host a memorable party.\\r\\nLarge island bench with brass bronze accents and nickel tapware \\r\\ncomplements plentiful storage as well as an integrated fridge and pantry. \\r\\n\",\"image\":\"assets\\/media\\/sections\\/MQ\\/Screen_Shot_2020-07-01_at_7_26_35_AM1.png\",\"position\":\"center\",\"size\":\"full\"}',9999),
	(14,1,'{\"title\":\"large open plan living\",\"subheading\":\"Generous living spills out to terrace gardens\",\"description\":\"Inspired by the contrasting harmony of natural and built environments, the fine \\r\\nselection of raw materials include sandy-blonde, engineered timber floors and \\r\\nbluestone tiles on terrace balconies. Quality materials are deftly handled \\r\\nby expert craftspeople for longevity.\",\"image\":\"assets\\/media\\/sections\\/MQ\\/Screen_Shot_2020-07-01_at_7_26_45_AM1.png\",\"position\":\"center\",\"size\":\"full\"}',9999),
	(15,1,'{\"title\":\"REFINED ENSUITES & BATHROOMS\",\"subheading\":\"The ideal environment to cleanse and invigorate\",\"description\":\"A fine selection of quality fixtures and finishes ensure longevity and ease of maintenance in spaces designed to enhance daily rituals.\",\"image\":\"assets\\/media\\/sections\\/MQ\\/Screen_Shot_2020-07-01_at_7_26_53_AM1.png\",\"position\":\"center\",\"size\":\"half\"}',9999),
	(16,1,'{\"title\":\"OVERSIZED MASTER SUITES\",\"subheading\":\"\",\"description\":\"\",\"image\":\"assets\\/media\\/sections\\/MQ\\/Screen_Shot_2020-07-01_at_7_27_11_AM1.png\",\"position\":\"center\",\"size\":\"full\"}',9999),
	(17,1,'{\"title\":\"Outdoor living\",\"subheading\":\"Relax and entertain alfresco\",\"description\":\"Every apartment includes generous sunlit outdoor living spaces. \",\"image\":\"assets\\/media\\/sections\\/MQ\\/Screen_Shot_2020-07-01_at_7_27_21_AM1.png\",\"position\":\"center\",\"size\":\"full\"}',9999),
	(19,3,'{\"title\":\"\",\"subheading\":\"\",\"description\":\"\",\"image\":\"assets\\/media\\/sections\\/Mw\\/429_Bay_Drone_Location_30mm-072-Pano.png\",\"position\":\"center\",\"size\":\"full\"}',9999),
	(20,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s1.png\",\"caption\":\"Rainwater Collection\"}',9999),
	(21,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s2.png\",\"caption\":\"Efficient Heating\"}',9999),
	(22,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s3.png\",\"caption\":\"Efficient Lighting\"}',9999),
	(23,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s4.png\",\"caption\":\"20% Recycled Insulation\"}',9999),
	(24,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s61.png\",\"caption\":\"No Toxins\"}',9999),
	(25,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s71.png\",\"caption\":\"Double Glazing\"}',9999),
	(26,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s8.png\",\"caption\":\"Natural Ventilation\"}',9999),
	(27,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s9.png\",\"caption\":\"Efficient Air-conditioning\"}',9999),
	(28,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s10.png\",\"caption\":\"Renewable Energy Costs\"}',9999),
	(29,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s11.png\",\"caption\":\"Efficient Fixtures\"}',9999),
	(30,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s12.png\",\"caption\":\"Improved Air Quality\"}',9999),
	(31,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s13.png\",\"caption\":\"13 Bycicle Spaces\"}',9999),
	(32,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s141.png\",\"caption\":\"Great Walk Score\"}',9999),
	(33,6,'{\"icon\":\"assets\\/media\\/sections\\/Ng\\/s151.png\",\"caption\":\"Excellent transport\"}',9999),
	(34,7,'{\"image\":\"assets\\/media\\/sections\\/Nw\\/Screen_Shot_2020-07-03_at_11_40_31_AM.png\",\"size\":\"fourth\"}',9999),
	(35,7,'{\"image\":\"assets\\/media\\/sections\\/Nw\\/Screen_Shot_2020-07-03_at_11_40_37_AM.png\",\"size\":\"fourth\"}',9999),
	(36,7,'{\"image\":\"assets\\/media\\/sections\\/Nw\\/Screen_Shot_2020-07-03_at_11_40_42_AM.png\",\"size\":\"fourth\"}',9999),
	(37,7,'{\"image\":\"assets\\/media\\/sections\\/Nw\\/Screen_Shot_2020-07-03_at_11_40_50_AM.png\",\"size\":\"fourth\"}',9999),
	(38,8,'{\"title\":\"429 bay st brighton\",\"subheading\":\"Bianco reflects the purity of salt and sand with \\r\\nnatural surfaces designed as the perfect complement \\r\\nto affordable cosmopolitan living.\",\"description\":\"\",\"videourl\":\"0vrdgDdPApQ\",\"size\":\"full\"}',9999),
	(41,12,'{\"title\":\"PREMIUM FINISHES\",\"subheading\":\"Warmth and light reflected \\r\\nfrom a clean palette of natural finishes. \\r\\n\",\"description\":\"\",\"image\":\"assets\\/media\\/sections\\/MTI\\/bbir-bay-st-flat-lays-032-2-extended-bg@2x.jpg\"}',9999),
	(43,15,'{\"image\":\"assets\\/media\\/sections\\/MTU\\/bay-st-lifestyle-photography-012@2x.jpg\",\"size\":\"fourth\"}',1),
	(44,15,'{\"image\":\"assets\\/media\\/sections\\/MTU\\/bay-st-lifestyle-photography-093@2x.jpg\",\"size\":\"half\"}',2),
	(45,15,'{\"image\":\"assets\\/media\\/sections\\/MTU\\/bay-st-lifestyle-photography-012.jpg\",\"size\":\"fourth\"}',0),
	(46,16,'{\"image\":\"assets\\/media\\/sections\\/MTY\\/bay-st-lifestyle-photography-093@2x.jpg\",\"size\":\"half\"}',9999),
	(47,16,'{\"image\":\"assets\\/media\\/sections\\/MTY\\/bay-st-lifestyle-photography-012@2x.jpg\",\"size\":\"fourth\"}',9999),
	(48,16,'{\"image\":\"assets\\/media\\/sections\\/MTY\\/bay-st-lifestyle-photography-012@2x1.jpg\",\"size\":\"fourth\"}',9999),
	(49,17,'{\"image\":\"assets\\/media\\/sections\\/MTc\\/bay-st-lifestyle-photography-012@3x.jpg\",\"size\":\"fourth\"}',1),
	(50,17,'{\"image\":\"assets\\/media\\/sections\\/MTc\\/bay-st-lifestyle-photography-093.jpg\",\"size\":\"half\"}',2),
	(51,17,'{\"image\":\"assets\\/media\\/sections\\/MTc\\/bay-st-lifestyle-photography-012@2x.jpg\",\"size\":\"fourth\"}',0),
	(53,19,'{\"title\":\"\",\"subheading\":\"\",\"description\":\"\",\"image\":\"assets\\/media\\/sections\\/MTk\\/bianco_Map@3x.png\"}',9999),
	(54,23,'{\"title\":\"\",\"subheading\":\"We are a design focused architecture and \\r\\ninterior design studio with an intent on creating \\r\\nmeaningful relationships through our work.\",\"description\":\"A design studio, renowned for exceptional, highly resolved, contextual based design responses, Cera Stribley, is a bespoke practice based in Melbourne. Established in 2014, the studio is collection of almost 40 architects, designers and spatial planners.  \\r\\nFounded by Domenic Cerantonio and Chris Stribley, who joined forces after having studied together and following their careers at some of Melbourne\\u2019s most renown firms, Cera Stribley is a truly unique studio, designing unparalleled design outcomes. \\r\\n<br><br>\\r\\nStudio 5\\/249 Chapel St, Prahran, VIC 3181\\r\\ncs-a.com.au\",\"image\":\"assets\\/media\\/sections\\/MjM\\/cera-stribley-logo-secondary-left-cmyk-90-mm.png\",\"position\":\"center\",\"size\":\"fifth\"}',9999),
	(55,24,'{\"title\":\"\",\"subheading\":\"Damgar Property Group stands at the \\r\\nforefront of boutique residential development \\r\\nin inner Melbourne. We have complete focus \\r\\nfrom beginning to end.\",\"description\":\"During their 30 years in property development, Damgar Property Group has fostered strong relationships with key industry partners including Architects, Engineers and Planning Consultants. All projects are funded in conjunction with Australian Banks, whom they have long-standing relationships with.\\r\\nAs builders in their own right, Damgar have a keen eye for quality and \\r\\nliveability. They know what it takes to get the job done properly.\\r\\n<br><br>\\r\\n79B Asling St, Brighton, VIC 3186\\r\\ndamgargroup.com.au\",\"image\":\"assets\\/media\\/sections\\/MjQ\\/damgar-logo-black@2x.png\",\"position\":\"center\",\"size\":\"fourth\"}',9999),
	(58,25,'{\"subheading\":\"Michael Lang 0407 766 771\\r\\n\",\"details\":\"CALL TO BOOK AN APPOINTMENT \\r\\nAT OUR  DISPLAY SUITE*<br>\\r\\n425 BAY STREET, BRIGHTON \",\"message\":\"*We are continuing to closely monitor the ongoing COVID-19 situation, with the health and well-being of our customers and staff as a top priority. To maximise precautions, and in the interest of your safety all inspections have been moved to private appointments. Should you visit there will be some screening questions we\\u2019ll need to ask you in addition to practicing safe social distancing guidelines including observing a distance of 1.5m between people and limiting numbers to 3 people per appointment. Enquire now to book your private inspection.\"}',9999),
	(59,30,'{\"title\":\"ALSO AT 429 BAY STREET\",\"subheading\":\"Five Exceptional Commercial Spaces \\r\\nfor Sale or Lease   \\r\\n\",\"description\":\"Commercial Agents details:\\r\\n<br><br>\\r\\nAston Commercial\\r\\n<br><br>\\r\\n<big>Jeremy Gruzewski 0422 211 021<\\/big>\\r\\n<br><br>\\r\\nJeremy@astoncommercial.com.au\\r\\n<br><br>\\r\\nDBRE Property\\r\\n<br><br>\\r\\n<big>James Davie 0412 209 <\\/big>\\r\\n<br><br>\\r\\nJames@dbreproperty.com.au\\r\\n\",\"image\":\"assets\\/media\\/sections\\/MzA\\/BaySt_LivingBalcony_05_Copy_2.jpg\",\"position\":\"center\",\"size\":\"full\"}',9999),
	(60,29,'{\"title\":\"429 bay st brighton\",\"subheading\":\"Bianco reflects the purity of salt and sand with \\r\\nnatural surfaces designed as the perfect complement \\r\\nto affordable cosmopolitan living.\",\"description\":\"\",\"image\":\"assets\\/media\\/sections\\/Mjk\\/BBIR0592_BayStLobby_ViewA@2x.jpg\",\"videourl\":\"\",\"size\":\"full\"}',9999);

/*!40000 ALTER TABLE `section_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `content` text,
  `display_order` int(4) unsigned NOT NULL DEFAULT '9999',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;

INSERT INTO `sections` (`id`, `page_id`, `type`, `content`, `display_order`, `published`)
VALUES
	(1,1,'section_image_text','{\"full_width\":\"yes\",\"name\":\"interiors\",\"bgcolor\":\"white\",\"title\":\"interiors\",\"caption\":\"\"}',4,1),
	(2,1,'image_text_below','{\"full_width\":\"yes\",\"bgcolor\":\"beige\",\"name\":\"architecture\",\"title\":\"429 BAY ST, BRIGHTON\",\"logo\":\"assets\\/media\\/sections\\/Mg\\/Screen_Shot_2020-07-03_at_12_26_03_PM_copy.png\",\"subheading\":\"Underneath its rational, clean cut form, Bianco is really an expression of architectural classicism.\",\"description\":\"The building\\u2019s fa\\u00e7ade articulation is an exercise in proportion, without excess adornment Bianco appears pure and contemporary. \",\"author\":\"Dom Cerantonio, Managing Principal  Cera Stribley Architects\",\"image\":\"assets\\/media\\/sections\\/Mg\\/Screen_Shot_2020-07-03_at_12_25_43_PM1.png\",\"signature\":\"assets\\/media\\/sections\\/Mg\\/Screen_Shot_2020-07-03_at_1_00_07_PM1.png\"}',2,1),
	(3,1,'section_image_text','{\"full_width\":\"yes\",\"name\":\"lifestyle\",\"bgcolor\":\"white\",\"title\":\"lifestyle\",\"logo\":null,\"caption\":\"\"}',9,1),
	(4,1,'section_image_text','{\"full_width\":\"\",\"title\":\"\",\"bgcolor\":\"\",\"caption\":\"\"}',10,1),
	(5,1,'image_text','{\"full_width\":\"yes\",\"title\":\"The Apartments\"}',3,1),
	(6,1,'section_icon_blocks','{\"full_width\":\"yes\",\"name\":\"\",\"bgcolor\":\"white\",\"title\":\"Sustainability\",\"subheading\":\"Bianco Brighton features a range of innovative \\r\\nand cutting-edge sustainability measures, to deliver \\r\\na more energy efficient and environmentally \\r\\nfriendly home to each resident.\",\"description\":\"\"}',5,1),
	(7,1,'multi_images','{\"full_width\":\"yes\",\"bgcolor\":\"white\",\"title\":\"It\'s in the detail\",\"subheading\":\"An attention to fine details and meticulous interior design are apparent at every turn.\",\"caption\":\"Luxury is reflected in bronze brass accents and quality brushed nickel fixtures. The use of natural materials is expertly blended to reveal a serene template for sophisticated living.  \"}',6,1),
	(8,1,'section_video_text','{\"full_width\":\"yes\",\"name\":\"\",\"bgcolor\":\"white\",\"title\":\"\",\"caption\":\"\"}',0,0),
	(12,1,'full_image_with_text','{\"full_width\":\"yes\"}',8,1),
	(15,1,'multi_images','{\"full_width\":\"yes\",\"bgcolor\":\"white\",\"title\":\"BRIGHTON\\u2019S FINEST\",\"subheading\":\"Village lifestyle on Bay Street\",\"caption\":\"Blending the quintessential expression of cosmopolitan Melbourne and Australia\\u2019s reputation for relaxed outdoor living, Bay Street fosters a friendly atmosphere where the needs of everyday living are on your doorstep. \\r\\nCaf\\u00e9s from all points on the culinary map are represented, whilst restaurants and bars offer plenty of choice for event dining, celebration and casual catch ups.  \\r\\nFind your tribe at yoga, or catch a movie at the local cinema. Enjoy a quick trip into the city by train or shop locally for homewares, gifts and groceries at one of the world\\u2019s most accessible locales. \"}',11,1),
	(16,1,'multi_images','{\"full_width\":\"yes\",\"bgcolor\":\"white\",\"title\":\"BOUTIQUE SHOPPING AT  FASHIONABLE FAVOURITES\",\"subheading\":\"As Melbourne\\u2019s most desirable  suburb, Brighton has it all\",\"caption\":\"With so many entertainment choices, there\\u2019s always somewhere new \\r\\nto discover strolling tree-lined streets to the Brighton Baths and iconic \\r\\nMiddle Brighton beach.\\r\\n\"}',12,1),
	(17,1,'multi_images','{\"full_width\":\"yes\",\"bgcolor\":\"white\",\"title\":\"FROM PARKSIDE TO BAYSIDE\",\"subheading\":\"Live by iconic Brighton Beach\",\"caption\":\"More than 5km of coastline supports walking and cycling trails all the way through Elwood to St Kilda and inner-city attractions, whilst the bay hosts a range of both competitive and leisure sports for all ages. \\r\\n\"}',13,1),
	(19,1,'full_image_with_text','{\"full_width\":\"yes\"}',14,1),
	(22,1,'section_text_logo','{\"full_width\":\"yes\",\"title\":\"team\",\"name\":\"THE Architects & Interior DESIGNERs\",\"logo\":\"\",\"subheading\":\"Cera Stribley\",\"subheading2\":\"We are a design focused architecture and \\r\\ninterior design studio with an intent on creating \\r\\nmeaningful relationships through our work.\",\"description\":\"A design studio, renowned for exceptional, highly resolved, contextual based design responses, Cera Stribley, is a bespoke practice based in Melbourne. Established in 2014, the studio is collection of almost 40 architects, designers and spatial planners.  \\r\\nFounded by Domenic Cerantonio and Chris Stribley, who joined forces after having studied together and following their careers at some of Melbourne\\u2019s most renown firms, Cera Stribley is a truly unique studio, designing unparalleled design outcomes. \",\"author\":\"Studio 5\\/249 Chapel St, Prahran, VIC 3181 cs-a.com.au\",\"image\":\"assets\\/media\\/sections\\/MjI\\/cera-stribley-logo-secondary-left-cmyk-90-mm.png\"}',15,1),
	(23,1,'section_image_text','{\"full_width\":\"yes\",\"name\":\"team\",\"bgcolor\":\"lightbeige\",\"title\":\"THE Architects & Interior DESIGNERs\",\"logo\":null,\"caption\":\"Cera Stribley\"}',16,1),
	(24,1,'section_image_text','{\"full_width\":\"yes\",\"name\":\"\",\"bgcolor\":\"white\",\"title\":\"The developer\",\"caption\":\"Damgar Property Group\"}',17,1),
	(25,1,'section_with_form','{\"full_width\":\"yes\",\"bgcolor\":\"beige\",\"name\":\"register\",\"title\":\"\",\"caption\":\"Register <br>\\r\\nfor more information\\r\\n\"}',18,1),
	(28,1,'section_text_logo','{\"full_width\":\"yes\",\"title\":\"429 bay st brighton\",\"name\":\"429 bay st brighton\",\"logo\":\"assets\\/media\\/sections\\/Mjg\\/Screen_Shot_2020-07-03_at_12_26_03_PM_copy.png\",\"subheading\":\"Finishes strike a balance between \\r\\nneutrality and warmth. \\r\\n\",\"subheading2\":\"\",\"description\":\"The interiors have been designed cognizant of and  complimentary to the architectural language. Linear details feature throughout interrupted with organic interventions emulating the ebb and flow of the ocean. The interiors natural materials and textures are synonymous \\r\\nwith those found on sandy shores.\\r\\nHues of grey, blonde timbers and terrazzo evoke an  earthy elegance with fine bronze detailing adding a touch of glamour to custom joinery.  Finishes create welcoming and soothing spaces, a blank canvas for  residents to layer with their own personality.\\r\\n\\r\\n\",\"author\":\"Jessica Coulter, Senior Interior Designer  Cera Stribley Architects\",\"image\":\"\"}',7,1),
	(29,1,'section_video_text','{\"full_width\":\"yes\",\"name\":\"sample video\",\"bgcolor\":\"white\",\"title\":\"video\",\"caption\":\"\"}',1,1),
	(30,1,'section_image_text','{\"full_width\":\"yes\",\"name\":\"ALSO AT 429 BAY STREET\",\"bgcolor\":\"white\",\"title\":\"\",\"logo\":\"\",\"caption\":\"\"}',9999,1),
	(31,1,'section_text_logo','{\"full_width\":\"yes\",\"title\":\"429 bay st brighton\",\"name\":\"\",\"logo\":\"assets\\/media\\/sections\\/MzE\\/Screen_Shot_2020-07-03_at_12_26_03_PM_copy.png\",\"subheading\":\"Finishes strike a balance between \\r\\nneutrality and warmth. \\r\\n\",\"subheading2\":\"\",\"description\":\"The interiors have been designed cognizant of and  complimentary to the architectural language. Linear details feature throughout interrupted with organic interventions emulating the ebb and flow of the ocean. The interiors natural materials and textures are synonymous \\r\\nwith those found on sandy shores.\\r\\nHues of grey, blonde timbers and terrazzo evoke an  earthy elegance with fine bronze detailing adding a touch of glamour to custom joinery.  Finishes create welcoming and soothing spaces, a blank canvas for  residents to layer with their own personality.\\r\\n\",\"author\":\"Jessica Coulter, Senior Interior Designer  Cera Stribley Architects\",\"image\":\"\"}',9999,1);

/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table site_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `site_settings`;

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pritty name',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'text',
  PRIMARY KEY (`id`),
  KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `site_settings` WRITE;
/*!40000 ALTER TABLE `site_settings` DISABLE KEYS */;

INSERT INTO `site_settings` (`id`, `key`, `value`, `title`, `type`)
VALUES
	(1,'logo','assets/media/documents/bb.png','Admin Logo','file'),
	(2,'address_1','','Address Line 1','text'),
	(3,'address_2','','Address Line 2','text'),
	(4,'address_3','','Address Line 3','text'),
	(5,'lat_long','','Address Latitude / Longitude (-37.848400, 144.991135)','text'),
	(6,'contact_phone','','Contact Phone','text'),
	(7,'contact_email','','Contact Email','text'),
	(8,'front_end_logo','assets/media/documents/bianco-logo-cmyk-white@3x.png','Front End Logo','file'),
	(9,'front_end_logo_scroll',NULL,'Front End Logo on Scroll','file');

/*!40000 ALTER TABLE `site_settings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
